package com.javabooter.core.exception;

import com.javabooter.core.info.BaseInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息异常
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MessageException extends RuntimeException {

    private static final long serialVersionUID = 8247610319171014183L;

    /**
     * 错误码
     */
    protected int code;
    /**
     * 错误信息
     */
    protected String message;

    public MessageException(BaseInfo info) {
        super(info.getMessage());
        this.code = info.getCode();
        this.message = info.getMessage();
    }

    public MessageException(BaseInfo info, String message) {
        super(info.getMessage());
        this.code = info.getCode();
        this.message = message;
    }

    public MessageException(Throwable cause) {
        super(cause.getMessage(), cause);
    }

    public MessageException(BaseInfo info, Throwable cause) {
        super(info.getMessage(), cause);
        this.code = info.getCode();
        this.message = info.getMessage();
    }

    public MessageException(String message) {
        super(message);
        this.message = message;
    }

    public MessageException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public MessageException(int code) {
        super(String.valueOf(code));
        this.code = code;
    }

    public MessageException(int code, Throwable cause) {
        super(String.valueOf(code), cause);
        this.code = code;
    }

    public MessageException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public MessageException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
