package com.javabooter.core.configuration;

import com.javabooter.core.graceful.GracefulShutdownListener;
import com.javabooter.core.graceful.GracefulStartupListener;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.Validator;
import com.javabooter.core.validate.handler.ValidateDigitHandler;
import com.javabooter.core.validate.handler.ValidateIntHandler;
import com.javabooter.core.validate.handler.ValidateLengthHandler;
import com.javabooter.core.validate.handler.ValidateLongHandler;
import com.javabooter.core.validate.handler.ValidateNotEmptyHandler;
import com.javabooter.core.validate.handler.ValidateNotNullHandler;
import com.javabooter.core.validate.handler.ValidatePatternHandler;
import com.javabooter.core.validate.handler.ValidateStringInHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
@Import(cn.hutool.extra.spring.SpringUtil.class)
@ComponentScan(basePackages = {"com.javabooter.core.doc.controller"})
public class CoreConfiguration {

    @Bean
    public Validator validator() {
        Validator validator = new Validator();
        Map<String, Handler> handlerMap = new HashMap<>();
        handlerMap.put("ValidateDigit", new ValidateDigitHandler());
        handlerMap.put("ValidateInt", new ValidateIntHandler());
        handlerMap.put("ValidateLong", new ValidateLongHandler());
        handlerMap.put("ValidateNotEmpty", new ValidateNotEmptyHandler());
        handlerMap.put("ValidateNotNull", new ValidateNotNullHandler());
        handlerMap.put("ValidatePattern", new ValidatePatternHandler());
        handlerMap.put("ValidateLength", new ValidateLengthHandler());
        handlerMap.put("ValidateStringIn", new ValidateStringInHandler());
        validator.setHandlerMap(handlerMap);
        return validator;
    }

    /**
     * 优雅上线
     *
     * @return
     */
    @Bean
    public GracefulStartupListener gracefulStartupListener() {
        return new GracefulStartupListener();
    }

    /**
     * 优雅下线
     *
     * @return
     */
    @Bean
    public GracefulShutdownListener gracefulShutdownListener() {
        return new GracefulShutdownListener();
    }
}
