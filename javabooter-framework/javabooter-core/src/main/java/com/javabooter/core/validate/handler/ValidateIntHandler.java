package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Validate the @int annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateIntHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateInt) {
            checkInt(annotation, value);
        }
    }

    /**
     * validate the int type
     *
     * @param annotation
     *            validated annotation
     * @param value
     *            validated value
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkInt(Annotation annotation, Object value) {
        ValidateInt validateInt = (ValidateInt)annotation;
        int min = validateInt.min();
        int max = validateInt.max();
        String message = validateInt.message();

        Integer destValue;
        try {
            destValue = (Integer) value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if(destValue == null) {
            return; //S_NULL value is allowed.
        }

        if (destValue < min) {
            log.error("Validate fail. Error message: validate value is: {},min value is:{}", destValue, min);
            throw new ValidateException(message);
        }

        if (destValue > max) {
            log.error("Validate fail. Error message: validate value is: {},max value is: {}", destValue, max);
            throw new ValidateException(message);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateInt.class)) {
            checkInt(validatedObj, field);
        }
    }

    /**
     * validate the int type
     *
     * @param filter
     *            validated object
     * @param field
     *            validated field or property
     * @throws ValidateException
     */
    private void checkInt(AnnotationValidable filter, Field field) {
        ValidateInt annotation = field.getAnnotation(ValidateInt.class);
        int min = annotation.min();
        int max = annotation.max();
        String message = annotation.message();

        Integer destValue;
        try {
            destValue = (Integer) GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if(destValue == null) {
            return; //S_NULL value is allowed.
        }

        int value = destValue;

        if (value < min) {
            log.error("Validate fail. Error message: validate value is: {},min value is: {}", value, min);
            throw new ValidateException(message);
        }

        if (value > max) {
            log.error("Validate fail. Error message: validate value is: {},max value is: {}", value, max);
            throw new ValidateException(message);
        }
    }

}
