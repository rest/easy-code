package com.javabooter.core.domain;

import com.javabooter.core.mapper.BaseMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * BaseDO
 *
 * @param <T> DTO
 * @param <P> PO
 * @param <M> Mapper
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public class BaseDO<T, P, M extends BaseMapper<T, P>> {

    @Autowired
    protected M mapper;
}
