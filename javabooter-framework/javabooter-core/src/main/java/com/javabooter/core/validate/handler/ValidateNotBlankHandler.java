package com.javabooter.core.validate.handler;

import com.javabooter.core.Func;
import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Validate the @ValidateNotBlank annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateNotBlankHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateNotBlank) {
            checkNotBlank(annotation, value);
        }
    }

    private void checkNotBlank(Annotation annotation, Object value) {
        String message = ((ValidateNotBlank) annotation).message();
        Object dest;

        try {
            dest = value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if (Func.isEmpty(dest)) {
            log.error("Validate fail. Error message: validate value is : blank");
            throw new ValidateException(message);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateNotBlank.class)) {
            checkNotBlank(validatedObj, field);
        }
    }

    private void checkNotBlank(AnnotationValidable filter, Field field) {
        String message = field.getAnnotation(ValidateNotBlank.class).message();
        Object dest;

        try {
            dest = GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if (Func.isEmpty(dest)) {
            log.error("Validate fail. Error message: validate value is : blank");
            throw new ValidateException(message);
        }
    }
}
