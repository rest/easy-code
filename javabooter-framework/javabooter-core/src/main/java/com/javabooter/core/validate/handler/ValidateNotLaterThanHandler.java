package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;

/**
 * Validate the @ValidateBeforeThan annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateNotLaterThanHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        // TODO
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateNotLaterThan.class)) {
            checkTime(validatedObj, field);
        }
    }

    /**
     * validate if the start time is early than the end time
     *
     * @param filter
     *            validated object
     * @param field
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkTime(AnnotationValidable filter, Field field) {
        ValidateNotLaterThan annotation = field.getAnnotation(ValidateNotLaterThan.class);
        String sTime = field.getName();
        String eTime = annotation.laterTime();
        String message = annotation.message();
        Date startTime;
        Date endTime;
        try {
            startTime = (Date) GetFiledValue.getField(filter, sTime);
            endTime = (Date) GetFiledValue.getField(filter, eTime);
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if (startTime != null && endTime != null) {
            if (startTime.after(endTime)) {
                log.error("Validate fail. Error message: startTime is: " + startTime + ", endTime is:" + endTime);
                throw new ValidateException(message + "The startTime is: " + startTime + ",the endTime is:" + endTime);
            }
        }else{
            log.error("Validate fail. Error message: startTime or endTime is null,startTime is: " + startTime + ", endTime is:" + endTime);
            throw new ValidateException(message + "StartTime or endTime is null.The startTime is: " + startTime + ",the endTime is:" + endTime);
        }
    }
}
