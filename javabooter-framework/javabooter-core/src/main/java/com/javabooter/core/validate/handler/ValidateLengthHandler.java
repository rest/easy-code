package com.javabooter.core.validate.handler;

import com.javabooter.core.str.StrConstants;
import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Validate the @ValidateLength annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateLengthHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateLength) {
            checkSize(annotation, value);
        }
    }

    /**
     * validate the String length
     *
     * @param annotation
     *            filter validated object
     * @param value
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkSize(Annotation annotation, Object value){
        ValidateLength validateLength = (ValidateLength) annotation;
        int min = validateLength.minLength();
        int max = validateLength.maxLength();
        String message = validateLength.message();
        String destValue;
        try {
            destValue = (String) value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        int size = 0;
        if (!StrConstants.S_EMPTY.equals(value) && value != null) {
            size = destValue.length();
        }
        if (size < min || size > max) {
            log.error("Validate fail. Error message: validate size is:" + size + ",minSize value is:" + min + ",maxSize value is:" + max);
            throw new ValidateException(message);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateLength.class)) {
            checkSize(validatedObj, field);
        }
    }

    /**
     * validate the String length
     *
     * @param filter
     *            filter validated object
     * @param field
     *            validated field or property
     * @throws ValidateException
     */
    private void checkSize(AnnotationValidable filter, Field field) {
        ValidateLength annotation = field.getAnnotation(ValidateLength.class);
        int min = annotation.minLength();
        int max = annotation.maxLength();
        String message = annotation.message();
        String value;
        try {
            value = (String) GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        int size = 0;
        if (!StrConstants.S_EMPTY.equals(value) && value != null) {
            size = value.length();
        }
        if (size < min || size > max) {
            log.error("Validate fail. Error message: validate size is:" + size + ",minSize value is:" + min + ",maxSize value is:" + max);
            throw new ValidateException(message);
        }
    }
}
