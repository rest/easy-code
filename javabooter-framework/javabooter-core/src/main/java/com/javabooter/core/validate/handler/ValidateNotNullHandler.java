package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Validate the @ValidateNotNull annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateNotNullHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateNotNull) {
            checkNotNull(annotation, value);
        }
    }

    private void checkNotNull(Annotation annotation, Object value){
        String message = ((ValidateNotNull)annotation).message();
        Object dest;

        try {
            dest = value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if (dest == null) {
            log.error("Validate fail. Error message: validate value is : null");
            throw new ValidateException(message);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateNotNull.class)) {
            checkNotNull(validatedObj, field);
        }
    }

    private void checkNotNull(AnnotationValidable filter, Field field) {
        String message = field.getAnnotation(ValidateNotNull.class).message();
        Object dest;

        try {
            dest = GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if (dest == null) {
            log.error("Validate fail. Error message: validate value is: null");
            throw new ValidateException(message);
        }
    }
}
