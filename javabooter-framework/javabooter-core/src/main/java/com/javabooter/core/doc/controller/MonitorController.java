package com.javabooter.core.doc.controller;

import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Maps;
import com.javabooter.core.doc.monitor.MonitorUtils;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.web.controller.ControllerSupport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Slf4j
@RestController
@RequestMapping("/monitor")
public class MonitorController extends ControllerSupport {

    private static final String CONTROLLER = "controller";

    private static final String SERVICE = "service";

    private static final String DAO = "dao";

    @RequestMapping(value = "index")
    public String index() {
        return "monitor_index";
    }

    @RequestMapping(value = "report")
    public void report(@RequestParam("type") String type) {
        Map<String, Object> result = Maps.newHashMap();
        List<Map<String, String>> items = new ArrayList<>();

        ConcurrentMap<String, AtomicLong> timeMap = Maps.newConcurrentMap();
        ConcurrentMap<String, AtomicLong> countMap = Maps.newConcurrentMap();

        if (StrUtil.equalsIgnoreCase(CONTROLLER, type)) {
            timeMap = MonitorUtils.controllerTimeMap;
            countMap = MonitorUtils.controllerCountMap;
        } else if (StrUtil.equalsIgnoreCase(SERVICE, type)) {
            timeMap = MonitorUtils.serviceTimeMap;
            countMap = MonitorUtils.serviceCountMap;
        } else if (StrUtil.equalsIgnoreCase(DAO, type)) {
            timeMap = MonitorUtils.daoTimeMap;
            countMap = MonitorUtils.daoCountMap;
        }

        for (Map.Entry<String, AtomicLong> entry : timeMap.entrySet()) {
            String key = entry.getKey();
            Map<String, String> item = Maps.newHashMap();
            item.put("method", key);
            item.put("totalCount", String.valueOf(countMap.get(key).longValue()));
            item.put("totalTime", String.valueOf(timeMap.get(key).longValue()));
            item.put("avgTime", String.valueOf(timeMap.get(key).longValue() / countMap.get(key).longValue()));
            item.put("exceptionCount", String.valueOf(MonitorUtils.exceptionCountMap.get(key) == null ? 0 : MonitorUtils.exceptionCountMap.get(key).longValue()));
            items.add(item);
        }

        result.put("total", items.size());
        result.put("rows", items);
        responseItem(CommonInfoEnum.SUCCESS, result);
    }
}
