package com.javabooter.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/05/18 21:25
 */
@Data
@ConfigurationProperties(prefix = "redis")
public class RedisProperties {

    public static final String STANDALONE = "STANDALONE";

    public static final String SENTINEL = "SENTINEL";

    public static final String CLUSTER = "CLUSTER";

    private String mode;

    private String ip;

    private Integer port;

    private String password;

    private Integer timeout = 5000;

    private Integer maxIdle = 10;

    private Integer minIdle = 1;

    private Integer maxTotal = 30;

    private Integer maxWaitMillis = 5000;

    private Boolean testOnBorrow = true;

    public Boolean isStandaloneMode() {
       return STANDALONE.equalsIgnoreCase(this.mode);
    }

    public Boolean isSentinelMode() {
        return SENTINEL.equalsIgnoreCase(this.mode);
    }

    public Boolean isClusterMode() {
        return CLUSTER.equalsIgnoreCase(this.mode);
    }
}
