package com.javabooter.core.date;

import lombok.experimental.UtilityClass;

/**
 * 日期工具类
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@UtilityClass
public class DateConstants {

    /**
     * 毫秒
     */
    public static final long MS = 1;
    /**
     * 每秒钟的毫秒数
     */
    public static final long SECOND_MS = MS * 1000;
    /**
     * 每分钟的毫秒数
     */
    public static final long MINUTE_MS = SECOND_MS * 60;
    /**
     * 每小时的毫秒数
     */
    public static final long HOUR_MS = MINUTE_MS * 60;
    /**
     * 每天的毫秒数
     */
    public static final long DAY_MS = HOUR_MS * 24;

    public static final String NORM_YEAR_PATTERN = "yyyy";

    public static final String NORM_YYYYMMDD_PATTERN = "yyyyMMdd";

    public static final String NORM_YYYYMMDDHHMMSS_PATTERN = "yyyyMMddHHmmss";

    /**
     * 标准日期格式
     */
    public static final String NORM_DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 标准时间格式
     */
    public static final String NORM_TIME_PATTERN = "HH:mm:ss";
    /**
     * 标准日期时间格式，精确到分
     */
    public static final String NORM_DATETIME_MINUTE_PATTERN = "yyyy-MM-dd HH:mm";
    /**
     * 标准日期时间格式，精确到秒
     */
    public static final String NORM_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 标准日期时间格式，精确到毫秒
     */
    public static final String NORM_DATETIME_MS_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    /**
     * HTTP头中日期时间格式
     */
    public static final String HTTP_DATETIME_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z";
    public static final char Y_CHAR = 'y';
    public static final char D_CHAR = 'd';
    public static final char H_CHAR = 'h';
    public static final char M_CHAR = 'm';
    public static final char S_CHAR = 's';
    public static final int NUM_2 = 2;
}
