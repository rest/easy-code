package com.javabooter.core.exception;

import com.javabooter.core.info.BaseInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Service层 异常
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 8247610319171014183L;

    /**
     * 错误码
     */
    protected int code;
    /**
     * 错误信息
     */
    protected String message;

    public ServiceException(BaseInfo info) {
        super(info.getMessage());
        this.code = info.getCode();
        this.message = info.getMessage();
    }

    public ServiceException(BaseInfo info, String message) {
        super(info.getMessage());
        this.code = info.getCode();
        this.message = message;
    }

    public ServiceException(Throwable cause) {
        super(cause.getMessage(), cause);
    }

    public ServiceException(BaseInfo info, Throwable cause) {
        super(info.getMessage(), cause);
        this.code = info.getCode();
        this.message = info.getMessage();
    }

    public ServiceException(String message) {
        super(message);
        this.message = message;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public ServiceException(int code) {
        super(String.valueOf(code));
        this.code = code;
    }

    public ServiceException(int code, Throwable cause) {
        super(String.valueOf(code), cause);
        this.code = code;
    }

    public ServiceException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public ServiceException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
