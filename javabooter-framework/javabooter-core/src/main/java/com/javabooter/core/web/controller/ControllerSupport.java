package com.javabooter.core.web.controller;

import cn.hutool.core.util.CharsetUtil;
import com.javabooter.core.info.BaseInfo;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.web.util.ResponseUtils;
import com.javabooter.core.exception.ServiceException;
import com.javabooter.core.web.dto.ModelResult;
import com.javabooter.core.web.dto.ResultPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HttpSupport
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Slf4j
public class ControllerSupport {

    protected static final String RESULT_KEY = "result";

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    /**
     * 处理响应信息
     *
     * @param code
     * @param message
     * @return
     */
    protected final void responseMessage(final int code, final String message) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);

        request.setAttribute(RESULT_KEY, modelResult);
    }

    protected final void responseMessage(final BaseInfo baseInfo) {
        responseMessage(baseInfo.getCode(), baseInfo.getMessage());
    }

    protected final void responseMessage(final BaseInfo baseInfo, final String message) {
        responseMessage(baseInfo.getCode(), message);
    }

    /**
     * 处理响应单个实体
     *
     * @param code
     * @param message
     * @param item
     * @return
     */
    protected final void responseItem(final int code, final String message, final Object item) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);
        modelResult.setItem(item);

        request.setAttribute(RESULT_KEY, modelResult);
    }

    protected final void responseItem(final BaseInfo baseInfo, final Object item) {
        responseItem(baseInfo.getCode(), baseInfo.getMessage(), item);
    }

    protected final void responseItem(final BaseInfo baseInfo, final String message, final Object item) {
        responseItem(baseInfo.getCode(), message, item);
    }

    /**
     * 处理响应list
     *
     * @param code
     * @param message
     * @param items
     * @return
     */
    protected final void responseItems(final int code, final String message, final Collection items) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);
        modelResult.setItems(items);

        request.setAttribute(RESULT_KEY, modelResult);
    }

    protected final void responseItems(final BaseInfo baseInfo, final Collection items) {
        responseItems(baseInfo.getCode(), baseInfo.getMessage(), items);
    }

    protected final void responseItems(final BaseInfo baseInfo, final String message, final Collection items) {
        responseItems(baseInfo.getCode(), message, items);
    }

    /**
     * 处理响应page
     *
     * @param code
     * @param message
     * @param totalCount
     * @param items
     * @return
     */
    protected final void responsePage(final int code, final String message, final Long totalCount,
        final Collection items,
        final Integer pageSize, final Integer pageNo) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);
        modelResult.setResultPage(new ResultPage(totalCount, pageSize, pageNo, items));

        request.setAttribute(RESULT_KEY, modelResult);
    }

    protected final void responsePage(final BaseInfo baseInfo, final Long totalCount, final Collection items,
        final Integer pageSize, final Integer pageNo) {
        responsePage(baseInfo.getCode(), baseInfo.getMessage(), totalCount, items, pageSize, pageNo);
    }

    protected final void responsePage(final BaseInfo baseInfo, final String message, final Long totalCount, final Collection items,
        final Integer pageSize, final Integer pageNo) {
        responsePage(baseInfo.getCode(), message, totalCount, items, pageSize, pageNo);
    }

    protected final String responseRedirect(final String url) {
        return "redirect:" + url;
    }

    protected final void responseExcelByte(final byte[] bytes, final String fileName) {
        ResponseUtils.renderExcelFile(response, bytes, fileName);
    }

    protected final void responseFile(final String content, final String fileName) {
        try {
            byte[] bytes = content.getBytes(CharsetUtil.defaultCharsetName());
            ResponseUtils.renderFile(response, bytes, fileName);
        } catch (UnsupportedEncodingException e) {
            throw new ServiceException(CommonInfoEnum.INTERNAL_ERROR, e);
        }
    }
}
