package com.javabooter.core.validate.handler;

import com.javabooter.core.str.StrConstants;
import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.regex.Pattern;

/**
 * Validate the @ValidateDigit annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateDigitHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateDigit) {
            checkDigit(annotation, value);
        }
    }

    /**
     * validate the String is digit only
     *
     * @param annotation
     *            filter validated annotation
     * @param value
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkDigit(Annotation annotation, Object value) {
        ValidateDigit validateDigit = (ValidateDigit) annotation;
        String message = validateDigit.message();
        String destValue;
        try {
            destValue = (String) value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        String patternStr = "\\d*";
        Pattern pattern = Pattern.compile(patternStr);
        if (value != null && !StrConstants.S_EMPTY.equals(value)) {
            if (!pattern.matcher(destValue).matches()) {
                log.error("Validate fail. Error message: validate value is {}", value);
                throw new ValidateException(message);
            }
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateDigit.class)) {
            checkDigit(validatedObj, field);
        }
    }

    /**
     * validate the String is digit only
     *
     * @param filter
     *            filter validated object
     * @param field
     *            validated field or property
     * @throws ValidateException
     */
    private void checkDigit(AnnotationValidable filter, Field field) {
        ValidateDigit annotation = field.getAnnotation(ValidateDigit.class);
        String message = annotation.message();
        String value;
        try {
            value = (String) GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        String patternStr = "\\d*";
        Pattern pattern = Pattern.compile(patternStr);
        if (value != null && !StrConstants.S_EMPTY.equals(value)) {
            if (!pattern.matcher(value).matches()) {
                log.error("Validate fail. Error message: validate value is: {}", value);
                throw new ValidateException(message);
            }
        }
    }
}
