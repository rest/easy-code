package com.javabooter.core.info;

import cn.hutool.http.HttpStatus;

/**
 * 通用信息定义枚举
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public enum CommonInfoEnum implements BaseInfo {

    SUCCESS(HttpStatus.HTTP_OK, "success"),
    BAT_REQUEST(HttpStatus.HTTP_BAD_REQUEST, "fail"),
    UNAUTHORIZED(HttpStatus.HTTP_UNAUTHORIZED, "未经授权, 请联系管理员"),
    INTERNAL_ERROR(HttpStatus.HTTP_INTERNAL_ERROR, "fail"),
    NOT_ACCEPTABLE(HttpStatus.HTTP_NOT_ACCEPTABLE, "fail");

    /**
     * 信息码
     */
    private final int code;

    /**
     * 信息描述
     */
    private final String message;

    CommonInfoEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
