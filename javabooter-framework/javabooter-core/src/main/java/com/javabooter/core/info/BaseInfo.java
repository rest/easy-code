package com.javabooter.core.info;

/**
 * Base 信息
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public interface BaseInfo {

    /**
     * 信息码
     */
    int getCode();

    /**
     * 信息描述
     */
    String getMessage();
}
