package com.javabooter.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javabooter.core.dao.po.BasePO;

import java.util.List;

/**
 * BaseDAO
 *
 * @param <P> PO
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public interface BaseDAO<P extends BasePO> extends BaseMapper<P> {

    /**
     * 批量插入(所有字段)
     *
     * @param poList
     * @return
     */
    Integer insertBatchAllColumn(List<P> poList);
}
