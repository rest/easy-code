package com.javabooter.core.doc.annotation;

import cn.hutool.core.util.StrUtil;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MethodDoc {

    String name() default StrUtil.EMPTY;

    String[] desc() default {};

    String input() default StrUtil.EMPTY;

    StatusEnum status() default StatusEnum.TODO;

    String author();

    String finishTime() default StrUtil.EMPTY;

    // 响应单个实体
    Class item() default Object.class;

    // 响应集合
    Class items() default Object.class;

    // 响应分页
    Class pages() default Object.class;
}
