package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.regex.Pattern;

/**
 * Validate the @ValidatePattern annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidatePatternHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidatePattern) {
            checkString(annotation, value);
        }
    }

    /**
     * validate the String format
     *
     * @param annotation
     *            validated object
     * @param value
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkString(Annotation annotation, Object value) {
        ValidatePattern validatePattern = (ValidatePattern) annotation;
        String patternStr = validatePattern.pattern();
        String message = validatePattern.message();

        checkStringFormat(patternStr, value, message);
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidatePattern.class)) {
            checkString(validatedObj, field);
        }
    }

    /**
     * validate the String format
     *
     * @param filter
     *            validated object
     * @param field
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkString(AnnotationValidable filter, Field field) {
        ValidatePattern annotation = field.getAnnotation(ValidatePattern.class);
        String patternStr = annotation.pattern();
        String message = annotation.message();
        Object value;

        try {
            value = GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        checkStringFormat(patternStr, value, message);
    }

    /**
     * validate the String format
     *
     * @param patternStr
     *            validate regex pattern
     * @param value
     *            validated value
     * @param message
     *            exception message
     * @throws ValidateException
     */
    private void checkStringFormat(String patternStr, Object value, String message) {
        String checkStr = (String) value;
        Pattern pattern = Pattern.compile(patternStr);
        if (checkStr != null) {
            String[] str = checkStr.split(",");
            for (int i = 0; i < str.length; i++) {
                if (!pattern.matcher(str[i]).matches()) {
                    log.error("Validate fail. Error message: validate value is: {}", str[i]);
                    throw new ValidateException(message);
                }
            }
        }
    }
}
