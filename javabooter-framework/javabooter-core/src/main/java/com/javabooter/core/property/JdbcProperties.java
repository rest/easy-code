package com.javabooter.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/05/18 21:25
 */
@Data
@ConfigurationProperties(prefix = "jdbc")
public class JdbcProperties {

    private String dsMainM0Driverclass;
    private String dsMainM0Url;
    private String dsMainM0Username;
    private String dsMainM0Password;

    private String dsMainM0S0Driverclass;
    private String dsMainM0S0Url;
    private String dsMainM0S0Username;
    private String dsMainM0S0Password;

    private String dsXxxM0Driverclass;
    private String dsXxxM0Url;
    private String dsXxxM0Username;
    private String dsXxxM0Password;
}
