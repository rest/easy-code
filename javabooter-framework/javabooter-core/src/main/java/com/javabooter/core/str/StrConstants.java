package com.javabooter.core.str;

import lombok.experimental.UtilityClass;

/**
 * 字符串工具类
 *
 * @author xiaoleilu
 */
@UtilityClass
public class StrConstants {

    public static final char C_SPACE = ' ';
    public static final char C_TAB = '\t';
    public static final char C_DOT = '.';
    public static final char C_SLASH = '/';
    public static final char C_BACKSLASH = '\\';
    public static final char C_CR = '\r';
    public static final char C_LF = '\n';
    public static final char C_UNDERLINE = '_';
    public static final char C_COMMA = ',';
    public static final char C_DELIM_START = '{';
    public static final char C_DELIM_END = '}';
    public static final char C_PARENTHESES_START = '(';
    public static final char C_PARENTHESES_END = ')';
    public static final char C_DOUBLE_QUOTE = '"';
    public static final char C_FORWARD_QUOTE = '\'';
    public static final char C_BACK_QUOTE = '`';
    public static final char C_POUND = '#';
    public static final char C_COLON = ':';
    public static final char C_DOLLAR = '$';
    public static final char C_MIDDLELINE = '-';
    public static final char C_QUESTION = '?';
    public static final char C_ASTERISK = '*';
    public static final char C_SEMICOLON = ';';
    public static final char C_EQUAL = '=';

    public static final String S_SPACE = " ";
    public static final String S_TAB = "\t";
    public static final String S_DOT = ".";
    public static final String S_SLASH = "/";
    public static final String S_BACKSLASH = "\\";
    public static final String S_EMPTY = "";
    public static final String S_CR = "\r";
    public static final String S_LF = "\n";
    public static final String S_CRLF = "\r\n";
    public static final String S_UNDERLINE = "_";
    public static final String S_COMMA = ",";
    public static final String S_DOUBLE_QUOTE = "\"";
    public static final String S_FORWARD_QUOTE = "'";
    public static final String S_BACK_QUOTE = "`";
    public static final String S_DELIM_START = "{";
    public static final String S_DELIM_END = "}";
    public static final String S_PARENTHESES_START = "(";
    public static final String S_PARENTHESES_END = ")";
    public static final String S_POUND = "#";
    public static final String S_COLON = ":";
    public static final String S_DOLLAR = "$";
    public static final String S_MIDDLELINE = "-";
    public static final String S_QUESTION = "?";
    public static final String S_ASTERISK = "*";
    public static final String S_SEMICOLON = ";";
    public static final String S_SINGLE_AND = "&";
    public static final String S_HTML_NBSP = "&nbsp;";
    public static final String S_HTML_AMP = "&amp";
    public static final String S_HTML_QUOTE = "&quot;";
    public static final String S_HTML_LT = "&lt;";
    public static final String S_HTML_GT = "&gt;";
    public static final String S_EQUAL = "=";

    public static final String S_EMPTY_JSON = "{}";
    public static final String S_HTTP_PREFIX = "http://";
    public static final String S_HTTPS_PREFIX = "https://";
    public static final String S_NULL = "null";
    public static final String S_UNDEFINED = "undefined";
    public static final String S_GET = "get";
    public static final String S_SET = "set";
    public static final int TWO_SIZE = 2;
    public static final int SIXTEEN = 16;
}
