package com.javabooter.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/05/18 21:25
 */
@Data
@ConfigurationProperties(prefix = "cache")
public class CacheProperties {

    private String prefix;
}
