package com.javabooter.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/05/18 21:25
 */
@Data
@ConfigurationProperties(prefix = "zookeeper")
public class ZooKeeperProperties {

    /**
     * 连接地址
     */
    private String url;

    /**
     * 超时时间(毫秒)，默认1000
     */
    private int timeout = 1000;

    /**
     * 重试次数，默认3
     */
    private int retry = 3;
}
