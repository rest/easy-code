package com.javabooter.core.web.dto;

import com.google.common.collect.Maps;
import com.javabooter.core.info.BaseInfo;
import lombok.Data;

import java.util.Collection;
import java.util.Map;

/**
 * ModelResult
 *
 * @author taobr
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
public final class ModelResult<T> {

    public static final String TOTAL_COUNT = "totalCount";
    public static final String TOTAL_PAGE = "totalPage";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PAGE_NO = "pageNo";
    public static final String RECORD_COUNT = "recordCount";
    public static final String ITEMS = "items";
    public static final String ITEM = "item";
    public static final String MESSAGE = "message";

    private int code;

    private final Map<String, Object> data = Maps.newHashMap();

    public ModelResult(final int code) {
        this.code = code;
    }

    public ModelResult(final BaseInfo baseInfo) {
        this.code = baseInfo.getCode();
        this.setMessage(baseInfo.getMessage());
    }

    public ModelResult setResultPage(final ResultPage resultPage) {
        data.put(TOTAL_COUNT, resultPage.getTotalCount());
        data.put(TOTAL_PAGE, resultPage.getTotalPage());
        data.put(PAGE_SIZE, resultPage.getPageSize());
        data.put(ITEMS, resultPage.getItems());

        data.put(PAGE_NO, resultPage.getPageNo());
        data.put(RECORD_COUNT, resultPage.getItems() == null ? 0 : resultPage.getItems().size());
        return this;
    }

    public int getTotalCount() {
        if (!data.containsKey(TOTAL_COUNT)) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(data.get(TOTAL_COUNT)));
    }

    public int getTotalPage() {
        if (!data.containsKey(TOTAL_PAGE)) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(data.get(TOTAL_PAGE)));
    }

    public int getPageSize() {
        if (!data.containsKey(PAGE_SIZE)) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(data.get(PAGE_SIZE)));
    }

    public int getPageNo() {
        if (!data.containsKey(PAGE_NO)) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(data.get(PAGE_NO)));
    }

    public int getRecordCount() {
        if (!data.containsKey(RECORD_COUNT)) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(data.get(RECORD_COUNT)));
    }

    public void setBaseInfo(final BaseInfo baseInfo) {
        this.code = baseInfo.getCode();
        this.setMessage(baseInfo.getMessage());
    }

    public void setMessage(final String message) {
        data.put(MESSAGE, message);
    }

    public String getMessage() {
        return String.valueOf(data.get(MESSAGE));
    }

    public void setItems(final Collection<T> items) {
        data.put(ITEMS, items);
    }

    public Collection<T> getItems() {
        return (Collection<T>) data.get(ITEMS);
    }

    public void setItem(T item) {
        data.put(ITEM, item);
    }

    public T getItem() {
        return (T) data.get(ITEM);
    }
}
