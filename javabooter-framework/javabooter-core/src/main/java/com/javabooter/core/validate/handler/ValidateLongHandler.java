package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Validate the @long annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateLongHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateLong) {
            checkDigit(annotation, value);
        }
    }

    /**
     * validate the long type
     *
     * @param annotation
     *            validated annotation
     * @param value
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkDigit(Annotation annotation, Object value) {
        ValidateLong validateLong = (ValidateLong) annotation;
        long min = validateLong.min();
        long max = validateLong.max();
        String message = validateLong.message();

        Long destValue;
        try {
            destValue = (Long) value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if(destValue == null) {
            return; //S_NULL value is allowed.
        }

        if (destValue < min) {
            log.error("Validate fail. Error message: validate value is: {},min value is: {}", destValue, min);
            throw new ValidateException(message);
        }

        if (destValue > max) {
            log.error("Validate fail. Error message: validate value is: {},max value is: {}", destValue, max);
            throw new ValidateException(message);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateLong.class)) {
            checkLong(validatedObj, field);
        }
    }

    /**
     * validate the long type
     *
     * @param filter
     *            validated object
     * @param field
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkLong(AnnotationValidable filter, Field field) {
        ValidateLong annotation = field.getAnnotation(ValidateLong.class);
        long min = annotation.min();
        long max = annotation.max();
        String message = annotation.message();

        Long destValue;
        try {
            destValue = (Long) GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }

        if(destValue == null) {
            return; //S_NULL value is allowed.
        }

        long value = destValue;

        if (value < min) {
            log.error("Validate fail. Error message: validate value is: {},min value is: {}", value, min);
            throw new ValidateException(message);
        }

        if (value > max) {
            log.error("Validate fail. Error message: validate value is: {},max value is: {}", value, max);
            throw new ValidateException(message);
        }
    }
}
