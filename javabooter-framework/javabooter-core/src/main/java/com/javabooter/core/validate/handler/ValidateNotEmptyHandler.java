package com.javabooter.core.validate.handler;

import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Validate the @ValidateNotNull annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateNotEmptyHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateNotEmpty) {
            checkNotEmpty(annotation, value);
        }
    }

    @SuppressWarnings("unchecked")
    private void checkNotEmpty(Annotation annotation, Object value) {
        String message = ((ValidateNotEmpty)annotation).message();
        try {
            if (value.getClass().isArray()) {
                Object[] dest = (Object[]) value;
                if (dest.length == 0) {
                    log.error("The collection is empty.");
                    throw new ValidateException(message);
                }
            } else {
                Collection dest = (Collection) value;
                if (dest.size() == 0) {
                    log.error("The collection is empty.");
                    throw new ValidateException(message);
                }
            }
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateNotEmpty.class)) {
            checkNotEmpty(validatedObj, field);
        }
    }

    @SuppressWarnings("unchecked")
    private void checkNotEmpty(AnnotationValidable filter, Field field) {
        String message = field.getAnnotation(ValidateNotEmpty.class).message();
        try {
            if (field.getType().isArray()) {
                Object [] dest = (Object []) GetFiledValue.getField(filter, field.getName());
                if (dest == null || dest.length == 0) {
                    log.error("The collection "+ field.getName() +" is empty.");
                    throw new ValidateException(message);
                }
            } else {
                Collection dest = (Collection) GetFiledValue.getField(filter, field.getName());
                if (dest == null || dest.size() == 0) {
                    log.error("The collection "+ field.getName() +" is empty.");
                    throw new ValidateException(message);
                }
            }
        } catch (Exception ex) {
            log.info("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
    }
}
