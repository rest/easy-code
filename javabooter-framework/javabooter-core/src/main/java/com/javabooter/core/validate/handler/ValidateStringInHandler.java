package com.javabooter.core.validate.handler;

import com.javabooter.core.str.StrConstants;
import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.Handler;
import com.javabooter.core.validate.ValidateException;
import com.javabooter.core.validate.GetFiledValue;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * validate the @ValidateStringIn annotation
 *
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class ValidateStringInHandler implements Handler {

    @Override
    public void validate(Annotation annotation, Object value) {
        if (annotation instanceof ValidateStringIn) {
            checkStringIn(annotation, value);
        }
    }

    /**
     * validate the String value
     *
     * @param annotation
     *            filter validated object
     * @param value
     *            validated field or property
     * @throws com.javabooter.core.validate.ValidateException
     */
    private void checkStringIn(Annotation annotation, Object value) {
        ValidateStringIn validateStringIn = (ValidateStringIn) annotation ;
        String exceptValue = validateStringIn.value();
        String message = validateStringIn.message();
        String destValue;
        try {
            destValue = (String) value;
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        if (!StrConstants.S_EMPTY.equals(destValue) && destValue != null) {
            String[] validateValues = exceptValue.split(",");
            List<String> list = Arrays.asList(validateValues);
            if (!list.contains(destValue)) {
                log.error("Validate fail. Error message: validate value is: {}", destValue);
                throw new ValidateException(message);
            }
        }
    }

    @Override
    public void validate(AnnotationValidable validatedObj, Field field) {
        if (field.isAnnotationPresent(ValidateStringIn.class)) {
            checkStringIn(validatedObj, field);
        }
    }

    /**
     * validate the String value
     *
     * @param filter
     *            filter validated object
     * @param field
     *            validated field or property
     * @throws ValidateException
     */
    private void checkStringIn(AnnotationValidable filter, Field field) {
        ValidateStringIn annotation = field.getAnnotation(ValidateStringIn.class);
        String exceptValue = annotation.value();
        String message = annotation.message();
        String value;
        try {
            value = (String) GetFiledValue.getField(filter, field.getName());
        } catch (Exception ex) {
            log.error("Get field value or cast value error. Error message: {}", ex.getMessage(), ex);
            throw new ValidateException(ex.getMessage(), ex);
        }
        if (!StrConstants.S_EMPTY.equals(value) && value != null) {
            String[] validateValues = exceptValue.split(",");
            List<String> list = Arrays.asList(validateValues);
            if (!list.contains(value)) {
                log.error("Validate fail. Error message: validate value is: {}", value);
                throw new ValidateException(message);
            }
        }
    }
}
