package com.javabooter.common.configuration;

import com.javabooter.core.cache.StringKeyGenerator;
import com.javabooter.core.property.CacheProperties;
import com.javabooter.core.property.RedisProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.io.Serializable;
import java.time.Duration;
import java.util.Map;

@Slf4j
@EnableCaching
@Configuration
@EnableConfigurationProperties({RedisProperties.class, CacheProperties.class})
public class RedisConfiguration extends CachingConfigurerSupport {

    @Bean
    public LettuceClientConfiguration lettuceClientConfiguration(RedisProperties redisProperties) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(redisProperties.getMaxTotal());
        jedisPoolConfig.setMaxIdle(redisProperties.getMaxIdle());
        jedisPoolConfig.setMinIdle(redisProperties.getMinIdle());
        jedisPoolConfig.setMaxWaitMillis(redisProperties.getMaxWaitMillis());
        jedisPoolConfig.setTestOnBorrow(redisProperties.getTestOnBorrow());
        return LettucePoolingClientConfiguration.builder().poolConfig(jedisPoolConfig).build();
    }

    public RedisSentinelConfiguration redisSentinelConfiguration(RedisProperties redisProperties) {
        RedisSentinelConfiguration redisSentinelConfiguration = new RedisSentinelConfiguration();
        redisSentinelConfiguration.setMaster("mymaster");
        redisSentinelConfiguration.addSentinel(new RedisNode("192.168.13.111", 30011));
        redisSentinelConfiguration.addSentinel(new RedisNode("192.168.13.112", 30011));
        return redisSentinelConfiguration;
    }

    public RedisClusterConfiguration redisClusterConfiguration(RedisProperties redisProperties) {
        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();
        redisClusterConfiguration.setMaxRedirects(3);
        redisClusterConfiguration.addClusterNode(new RedisNode("192.168.13.111", 30010));
        redisClusterConfiguration.addClusterNode(new RedisNode("192.168.13.112", 30010));
        return redisClusterConfiguration;
    }

    private RedisStandaloneConfiguration redisStandaloneConfiguration(RedisProperties redisProperties) {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisProperties.getIp());
        redisStandaloneConfiguration.setPort(redisProperties.getPort());
        return redisStandaloneConfiguration;
    }

    @Bean
    public LettuceConnectionFactory lettuceConnectionFactory(RedisProperties redisProperties,
        LettuceClientConfiguration lettuceClientConfiguration) {
        if (redisProperties.isClusterMode()) {
            return new LettuceConnectionFactory(redisClusterConfiguration(redisProperties), lettuceClientConfiguration);
        }

        if (redisProperties.isSentinelMode()) {
            return new LettuceConnectionFactory(redisSentinelConfiguration(redisProperties),
                lettuceClientConfiguration);
        }

        return new LettuceConnectionFactory(redisStandaloneConfiguration(redisProperties), lettuceClientConfiguration);
    }

    /**
     * 默认情况下的模板只能支持RedisTemplate<String, String>，也就是只能存入字符串，因此支持序列化
     */
    @Bean
    public RedisTemplate<String, Serializable> redisCacheTemplate(LettuceConnectionFactory lettuceConnectionFactory) {
        RedisTemplate<String, Serializable> template = new RedisTemplate<>();
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        template.setHashValueSerializer(new JdkSerializationRedisSerializer());
        template.setConnectionFactory(lettuceConnectionFactory);
        return template;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new StringKeyGenerator();
    }

    @Bean
    public CacheManager cacheManager(LettuceConnectionFactory lettuceConnectionFactory,
        CacheProperties cacheProperties) {

        Map<String, RedisCacheConfiguration> cacheConfigurations = new HashedMap<>();
        cacheConfigurations.put("activeSessionCache",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(3600)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("shiro-kickout-session",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(3600))
                .prefixCacheNameWith(cacheProperties.getPrefix())
                .serializeKeysWith(
                    RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("authorizationCache",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(3600)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("authenticationCache",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(3600)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("oneMinute",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(60)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("default",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(3600)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("halfHour",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(1800)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("day",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(86400)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("week",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(604800)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());
        cacheConfigurations.put("month",
            RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(2592000)).prefixCacheNameWith(
                cacheProperties.getPrefix()).serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer())).serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new JdkSerializationRedisSerializer())).disableCachingNullValues());

        return RedisCacheManager.builder(RedisCacheWriter.lockingRedisCacheWriter(
            lettuceConnectionFactory)).transactionAware().withInitialCacheConfigurations(
            cacheConfigurations).build();
    }

    @Bean
    public CacheErrorHandler errorHandler() {
        // 异常处理，当Redis发生异常时，打印日志，但是程序正常走
        return new CacheErrorHandler() {
            @Override
            public void handleCacheGetError(RuntimeException e, Cache cache, Object key) {
                log.error("Redis occur handleCacheGetError. [key='{}']", key, e);
            }

            @Override
            public void handleCachePutError(RuntimeException e, Cache cache, Object key, Object value) {
                log.error("Redis occur handleCachePutError. [key='{}', value='{}']", key, value, e);
            }

            @Override
            public void handleCacheEvictError(RuntimeException e, Cache cache, Object key) {
                log.error("Redis occur handleCacheEvictError. [key='{}']", key, e);
            }

            @Override
            public void handleCacheClearError(RuntimeException e, Cache cache) {
                log.error("Redis occur handleCacheClearError. ", e);
            }
        };
    }
}
