package com.javabooter.common.configuration;

import com.javabooter.common.cache.CommonCacheManager;
import com.javabooter.common.captcha.CaptchaCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class CommonConfiguration {

    /**
     * CommonCache
     *
     * @param cacheManager
     * @return
     */
    @Bean
    public CommonCacheManager commonCacheManager(CacheManager cacheManager) {
        CommonCacheManager commonCacheManager = new CommonCacheManager();
        commonCacheManager.setCacheManager(cacheManager);
        return commonCacheManager;
    }

    /**
     * 验证码 Service
     *
     * @param commonCacheManager
     * @return
     */
    @Bean
    public CaptchaCacheManager captchaCacheService(CommonCacheManager commonCacheManager) {
        CaptchaCacheManager captchaCacheManager = new CaptchaCacheManager();
        captchaCacheManager.setCacheManager(commonCacheManager);
        captchaCacheManager.setCacheName("halfHour");
        return captchaCacheManager;
    }
}
