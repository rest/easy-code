package com.javabooter.common.configuration;

import com.javabooter.common.cache.GlobalCacheManager;
import com.javabooter.core.cache.StringKeyGenerator;
import com.javabooter.core.cache.guava.GuavaCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@EnableCaching
@Configuration
public class GuavaConfiguration extends CachingConfigurerSupport {

    @Bean
    public KeyGenerator keyGenerator() {
        return new StringKeyGenerator();
    }

    @Bean
    public CacheManager cacheManager() {
        GlobalCacheManager globalCacheManager = new GlobalCacheManager();
        globalCacheManager.setTransactionAware(true);

        Set<GuavaCache> caches = new HashSet<>();
        caches.add(new GuavaCache("default", 3600));
        caches.add(new GuavaCache("hour", 10800));
        caches.add(new GuavaCache("day", 86400));
        caches.add(new GuavaCache("week", 432000));
        globalCacheManager.setCaches(caches);
        return globalCacheManager;
    }
}
