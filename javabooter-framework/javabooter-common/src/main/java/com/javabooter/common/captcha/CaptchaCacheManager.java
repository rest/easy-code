package com.javabooter.common.captcha;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.javabooter.core.web.util.CookieUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码缓存
 *
 * @author taomk
 * @version 1.0
 * @since 2014/9/22 14:33
 */
@Data
@Slf4j
public class CaptchaCacheManager implements InitializingBean {

    public static CaptchaCacheManager me() {
        return SpringUtil.getBean(CaptchaCacheManager.class);
    }

    private static final String DEFAULT_COOKIE_NAME = "cc";

    private static final String DEFAULT_CACHE_NAME = "cache-captcha";

    /**
     * Cookie超时默认为session会话状态
     */
    private static final int DEFAULT_MAX_AGE = -1;

    private CacheManager cacheManager;

    private String cacheName;

    private String cookieName;

    private Cache<String, String> captchaCache;

    public CaptchaCacheManager() {
        this.cacheName = DEFAULT_CACHE_NAME;
        this.cookieName = DEFAULT_COOKIE_NAME;
    }

    public CaptchaCacheManager(CacheManager cacheManager) {
        this();
        this.cacheManager = cacheManager;
    }

    /**
     * 生成验证码
     */
    public void generate(HttpServletRequest request, HttpServletResponse response) {
        // 先检查cookie的uuid是否存在
        String cookieValue = CookieUtils.getCookie(cookieName, request);
        boolean hasCookie = true;
        if (StrUtil.isBlank(cookieValue)) {
            hasCookie = false;
            cookieValue = IdUtil.fastUUID();
        }
        String captchaCode = CaptchaUtils.generateCode().toUpperCase();
        // 不存在cookie时设置cookie
        if (!hasCookie) {
            CookieUtils.setCookie(response, cookieName, cookieValue, DEFAULT_MAX_AGE);
        }
        // 生成验证码
        CaptchaUtils.generate(response, captchaCode);
        captchaCache.put(cookieValue, captchaCode);
    }

    /**
     * 仅能验证一次，验证后立即删除
     *
     * @param request          HttpServletRequest
     * @param response         HttpServletResponse
     * @param userInputCaptcha 用户输入的验证码
     * @return 验证通过返回 true, 否则返回 false
     */
    public boolean validate(HttpServletRequest request, HttpServletResponse response, String userInputCaptcha) {
        String cookieValue = CookieUtils.getCookie(cookieName, request);
        if (StrUtil.isBlank(cookieValue)) {
            return false;
        }
        String captchaCode = captchaCache.get(cookieValue);
        if (StrUtil.isBlank(captchaCode)) {
            return false;
        }

        // 转成大写重要
        userInputCaptcha = userInputCaptcha.toUpperCase();
        boolean result = userInputCaptcha.equals(captchaCode);
        if (result) {
            captchaCache.remove(cookieValue);
            CookieUtils.removeCookie(cookieName, response);
        }
        return result;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(cacheManager, "cacheManager must not be null!");
        Assert.hasText(cacheName, "cacheName must not be empty!");
        Assert.hasText(cookieName, "cookieName must not be empty!");
        this.captchaCache = cacheManager.getCache(cacheName);
    }
}
