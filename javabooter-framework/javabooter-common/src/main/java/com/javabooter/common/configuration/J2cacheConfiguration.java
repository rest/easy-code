package com.javabooter.common.configuration;

import com.javabooter.common.cache.GlobalCacheManager;
import com.javabooter.core.cache.StringKeyGenerator;
import com.javabooter.core.cache.j2cache.TwoLevelCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@EnableCaching
@Configuration
public class J2cacheConfiguration extends CachingConfigurerSupport {

    @Bean
    public KeyGenerator keyGenerator() {
        return new StringKeyGenerator();
    }

    @Bean
    public CacheManager cacheManager() {
        GlobalCacheManager globalCacheManager = new GlobalCacheManager();
        globalCacheManager.setTransactionAware(true);

        Set<TwoLevelCache> caches = new HashSet<>();
        caches.add(new TwoLevelCache("default"));
        caches.add(new TwoLevelCache("hour"));
        caches.add(new TwoLevelCache("day"));
        caches.add(new TwoLevelCache("week"));
        globalCacheManager.setCaches(caches);
        return globalCacheManager;
    }
}
