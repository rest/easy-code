package com.javabooter.common.web.interceptor;

import com.javabooter.core.Func;
import com.javabooter.core.doc.monitor.MonitorUtils;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.exception.ServiceException;
import com.javabooter.core.exception.UtilException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.web.dto.ModelResult;
import com.javabooter.core.web.interceptor.AbstractResponseLogInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller层日志拦截器
 *
 * @author taomk
 * @version 1.0
 * @since 15-5-22 下午7:57
 */
@Slf4j
public class ControllerCostLogInterceptor extends AbstractResponseLogInterceptor {

    @Override
    protected boolean isThreadLocalEnable() {
        return true;
    }

    @Override
    protected ModelResult buildModelResult(final HttpServletRequest request, final HandlerMethod handler,
        final Exception ex) {
        ModelResult result = (ModelResult) request.getAttribute("result");
        if (Func.isNotEmpty(ex)) {

            MonitorUtils.incCountForException(handler.getBeanType().getName(), handler.getMethod().getName());

            if (Func.isEmpty(result)) {
                result = new ModelResult(CommonInfoEnum.INTERNAL_ERROR);
            } else {
                result.setBaseInfo(CommonInfoEnum.INTERNAL_ERROR);
            }

            if (ex instanceof ServiceException) {
                result.setCode(((ServiceException) ex).getCode());
                result.setMessage(ex.getMessage());
            } else if (ex instanceof MessageException) {
                result.setCode(((MessageException) ex).getCode());
                result.setMessage(ex.getMessage());
            } else if (ex instanceof UtilException) {
                result.setBaseInfo(CommonInfoEnum.INTERNAL_ERROR);
                result.setMessage(ex.getMessage());
            } else if (ex instanceof UnauthorizedException) {
                result.setBaseInfo(CommonInfoEnum.NOT_ACCEPTABLE);
            } else {
                if (Func.isNotEmpty(ex.getMessage())) {
                    result.setMessage(ex.getMessage());
                }
            }
        }
        return result;
    }

    @Override
    public boolean preHandleProcess(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) {
        return true;
    }

    @Override
    protected String getLogName() {
        return "Controller execute report ";
    }
}
