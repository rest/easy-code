package com.javabooter.common.safe;

import cn.hutool.core.convert.Convert;
import com.javabooter.core.Func;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * 安全的Filter(过滤SQL和XSS)
 *
 * @author taomk
 * @version 1.0
 * @since 15-8-4 下午4:45
 */
public class HttpServletRequestSafeFilter implements Filter {

    public static final String FILTER_XSS = "filterXSS";
    public static final String FILTER_SQL = "filterSQL";
    FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean filterXSS = false;
        boolean filterSQL = false;

        if (Func.isNotEmpty(filterConfig.getInitParameter(FILTER_XSS))) {
            filterXSS = Convert.toBool(filterConfig.getInitParameter(FILTER_XSS));
        }

        if (Func.isNotEmpty(filterConfig.getInitParameter(FILTER_SQL))) {
            filterSQL = Convert.toBool(filterConfig.getInitParameter(FILTER_SQL));
        }

        chain.doFilter(new HttpServletRequestSafeWrapper((HttpServletRequest) request, filterXSS, filterSQL), response);
    }
}
