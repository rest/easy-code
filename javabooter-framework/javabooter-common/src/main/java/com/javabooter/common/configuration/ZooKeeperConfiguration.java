package com.javabooter.common.configuration;

import com.javabooter.core.property.ZooKeeperProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@EnableConfigurationProperties(ZooKeeperProperties.class)
public class ZooKeeperConfiguration {

    @Bean
    public CuratorFramework curatorFramework(ZooKeeperProperties zooKeeperProperties) {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(zooKeeperProperties.getTimeout(), zooKeeperProperties.getRetry());
        CuratorFramework client = CuratorFrameworkFactory.newClient(zooKeeperProperties.getUrl(), retryPolicy);
        client.start();
        return client;
    }
}
