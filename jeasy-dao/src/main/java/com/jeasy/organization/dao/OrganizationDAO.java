package com.jeasy.organization.dao;

import com.jeasy.organization.po.OrganizationPO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 机构 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface OrganizationDAO extends BaseDAO<OrganizationPO> {
}
