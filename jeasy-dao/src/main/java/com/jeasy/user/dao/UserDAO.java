package com.jeasy.user.dao;

import com.jeasy.user.po.UserPO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 用户 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface UserDAO extends BaseDAO<UserPO> {
}
