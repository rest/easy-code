package com.jeasy.userrole.dao;

import com.jeasy.userrole.po.UserRolePO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 用户角色 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface UserRoleDAO extends BaseDAO<UserRolePO> {
}
