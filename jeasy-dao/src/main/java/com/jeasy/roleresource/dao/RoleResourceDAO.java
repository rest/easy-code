package com.jeasy.roleresource.dao;

import com.jeasy.roleresource.po.RoleResourcePO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 角色资源 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface RoleResourceDAO extends BaseDAO<RoleResourcePO> {
}
