package com.jeasy.fileattach.dao;

import com.jeasy.fileattach.po.FileAttachPO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 文件附件 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
public interface FileAttachDAO extends BaseDAO<FileAttachPO> {
}
