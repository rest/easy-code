package com.jeasy.dictionary.dao;

import com.jeasy.dictionary.po.DictionaryPO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 字典 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface DictionaryDAO extends BaseDAO<DictionaryPO> {
}
