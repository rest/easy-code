package com.jeasy.dao.configuration;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import com.alibaba.druid.wall.WallFilter;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.IllegalSQLInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Maps;
import com.javabooter.common.dao.DaoCostLogAspect;
import com.javabooter.common.datasource.ThreadLocalRoutingDataSource;
import com.javabooter.core.datasource.FixedDruidDataSource;
import com.javabooter.core.property.JdbcProperties;
import io.shardingsphere.core.api.ShardingDataSourceFactory;
import io.shardingsphere.core.api.config.MasterSlaveRuleConfiguration;
import io.shardingsphere.core.api.config.ShardingRuleConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableConfigurationProperties(JdbcProperties.class)
@MapperScan(basePackages = {"com.jeasy.*.dao"}, sqlSessionFactoryRef = "sqlSessionFactory")
public class DaoConfiguration {

    /**
     * 配置监控统计拦截的 filters
     *
     * @return
     */
    private List<Filter> filters() {
        List<Filter> filters = Lists.newArrayList();

        WallFilter wallFilter = new WallFilter();
        wallFilter.setDbType("mysql");
        filters.add(wallFilter);

        StatFilter statFilter = new StatFilter();
        statFilter.setSlowSqlMillis(10000);
        statFilter.setLogSlowSql(true);
        statFilter.setMergeSql(true);
        filters.add(statFilter);

        Slf4jLogFilter slf4jLogFilter = new Slf4jLogFilter();
        slf4jLogFilter.setStatementExecutableSqlLogEnable(true);
        filters.add(slf4jLogFilter);

        return filters;
    }

    @Bean(name = "ds_main_m0", initMethod = "init", destroyMethod = "close")
    public FixedDruidDataSource ds_main_m0(JdbcProperties jdbcProperties) {
        FixedDruidDataSource fixedDruidDataSource = new FixedDruidDataSource();
        // 基本属性 url、user、password
        fixedDruidDataSource.setUrl(jdbcProperties.getDsMainM0Url());
        fixedDruidDataSource.setUsername(jdbcProperties.getDsMainM0Username());
        fixedDruidDataSource.setPassword(jdbcProperties.getDsMainM0Password());

        // 配置初始化大小、最小、最大
        fixedDruidDataSource.setInitialSize(5);
        // 连接池激活的最大数据库连接总数。设为0表示无限制
        fixedDruidDataSource.setMaxActive(20);
        // 最大建立连接等待时间，单位为 ms，如果超过此时间将接到异常。设为-1表示无限制
        fixedDruidDataSource.setMaxWait(60000);
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        fixedDruidDataSource.setTimeBetweenEvictionRunsMillis(60000);
        // 配置连接池中连接可空闲的时间(针对连接池中的连接对象.空闲超过这个时间则断开，直到连接池中的连接数到minIdle为止)，单位是毫秒
        fixedDruidDataSource.setMinEvictableIdleTimeMillis(300000);
        // 用来检测连接是否有效的sql，要求是一个查询语句
        fixedDruidDataSource.setValidationQuery("SELECT 'x' FROM DUAL");
        // 建议配置为true，不影响性能，并且保证安全性
        fixedDruidDataSource.setTestWhileIdle(true);
        // 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnBorrow(false);
        // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnReturn(false);
        // 打开PSCache，并且指定每个连接上PSCache的大小(Oracle或mysql5.5及以上使用)
        fixedDruidDataSource.setPoolPreparedStatements(true);
        fixedDruidDataSource.setMaxPoolPreparedStatementPerConnectionSize(20);
        // 超过时间限制是否回收
        fixedDruidDataSource.setRemoveAbandoned(true);
        // 超时时间：单位为秒。180 秒=3 分钟
        fixedDruidDataSource.setRemoveAbandonedTimeout(180);
        // 关闭 abanded 连接时输出错误日志
        fixedDruidDataSource.setLogAbandoned(true);

        // 配置监控统计拦截的 filters
        fixedDruidDataSource.setProxyFilters(filters());

        return fixedDruidDataSource;
    }

    @Bean(name = "ds_main_m0_s0", initMethod = "init", destroyMethod = "close")
    public FixedDruidDataSource ds_main_m0_s0(JdbcProperties jdbcProperties) {
        FixedDruidDataSource fixedDruidDataSource = new FixedDruidDataSource();
        // 基本属性 url、user、password
        fixedDruidDataSource.setUrl(jdbcProperties.getDsMainM0S0Url());
        fixedDruidDataSource.setUsername(jdbcProperties.getDsMainM0S0Username());
        fixedDruidDataSource.setPassword(jdbcProperties.getDsMainM0S0Password());

        // 配置初始化大小、最小、最大
        fixedDruidDataSource.setInitialSize(5);
        // 连接池激活的最大数据库连接总数。设为0表示无限制
        fixedDruidDataSource.setMaxActive(20);
        // 最大建立连接等待时间，单位为 ms，如果超过此时间将接到异常。设为-1表示无限制
        fixedDruidDataSource.setMaxWait(60000);
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        fixedDruidDataSource.setTimeBetweenEvictionRunsMillis(60000);
        // 配置连接池中连接可空闲的时间(针对连接池中的连接对象.空闲超过这个时间则断开，直到连接池中的连接数到minIdle为止)，单位是毫秒
        fixedDruidDataSource.setMinEvictableIdleTimeMillis(300000);
        // 用来检测连接是否有效的sql，要求是一个查询语句
        fixedDruidDataSource.setValidationQuery("SELECT 'x' FROM DUAL");
        // 建议配置为true，不影响性能，并且保证安全性
        fixedDruidDataSource.setTestWhileIdle(true);
        // 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnBorrow(false);
        // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnReturn(false);
        // 打开PSCache，并且指定每个连接上PSCache的大小(Oracle或mysql5.5及以上使用)
        fixedDruidDataSource.setPoolPreparedStatements(true);
        fixedDruidDataSource.setMaxPoolPreparedStatementPerConnectionSize(20);
        // 超过时间限制是否回收
        fixedDruidDataSource.setRemoveAbandoned(true);
        // 超时时间：单位为秒。180 秒=3 分钟
        fixedDruidDataSource.setRemoveAbandonedTimeout(180);
        // 关闭 abanded 连接时输出错误日志
        fixedDruidDataSource.setLogAbandoned(true);

        // 配置监控统计拦截的 filters
        fixedDruidDataSource.setProxyFilters(filters());

        return fixedDruidDataSource;
    }

    @Bean(name = "ds_xxx_m0", initMethod = "init", destroyMethod = "close")
    public FixedDruidDataSource ds_xxx_m0(JdbcProperties jdbcProperties) {
        FixedDruidDataSource fixedDruidDataSource = new FixedDruidDataSource();
        // 基本属性 url、user、password
        fixedDruidDataSource.setUrl(jdbcProperties.getDsXxxM0Url());
        fixedDruidDataSource.setUsername(jdbcProperties.getDsXxxM0Username());
        fixedDruidDataSource.setPassword(jdbcProperties.getDsXxxM0Password());

        // 配置初始化大小、最小、最大
        fixedDruidDataSource.setInitialSize(5);
        // 连接池激活的最大数据库连接总数。设为0表示无限制
        fixedDruidDataSource.setMaxActive(20);
        // 最大建立连接等待时间，单位为 ms，如果超过此时间将接到异常。设为-1表示无限制
        fixedDruidDataSource.setMaxWait(60000);
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        fixedDruidDataSource.setTimeBetweenEvictionRunsMillis(60000);
        // 配置连接池中连接可空闲的时间(针对连接池中的连接对象.空闲超过这个时间则断开，直到连接池中的连接数到minIdle为止)，单位是毫秒
        fixedDruidDataSource.setMinEvictableIdleTimeMillis(300000);
        // 用来检测连接是否有效的sql，要求是一个查询语句
        fixedDruidDataSource.setValidationQuery("SELECT 'x' FROM DUAL");
        // 建议配置为true，不影响性能，并且保证安全性
        fixedDruidDataSource.setTestWhileIdle(true);
        // 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnBorrow(false);
        // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能，建议仅在DEV使用
        fixedDruidDataSource.setTestOnReturn(false);
        // 打开PSCache，并且指定每个连接上PSCache的大小(Oracle或mysql5.5及以上使用)
        fixedDruidDataSource.setPoolPreparedStatements(true);
        fixedDruidDataSource.setMaxPoolPreparedStatementPerConnectionSize(20);
        // 超过时间限制是否回收
        fixedDruidDataSource.setRemoveAbandoned(true);
        // 超时时间：单位为秒。180 秒=3 分钟
        fixedDruidDataSource.setRemoveAbandonedTimeout(180);
        // 关闭 abanded 连接时输出错误日志
        fixedDruidDataSource.setLogAbandoned(true);

        // 配置监控统计拦截的 filters
        fixedDruidDataSource.setProxyFilters(filters());

        return fixedDruidDataSource;
    }

    /**
     * 数据分片 + 读写分离
     *
     * @param dsMainM0
     * @param dsMainM0S0
     * @param dsXxxM0
     * @return
     * @throws SQLException
     */
    @Bean("dataSource")
    public DataSource dataSource(@Qualifier("ds_main_m0") DataSource dsMainM0,
        @Qualifier("ds_main_m0_s0") DataSource dsMainM0S0,
        @Qualifier("ds_xxx_m0") DataSource dsXxxM0) throws SQLException {
        // 配置真实数据源
        Map<String, DataSource> dataSourceMap = new HashMap<>();
        dataSourceMap.put("defaultDb", dsMainM0);
        dataSourceMap.put("ds_main_m0", dsMainM0);
        dataSourceMap.put("ds_main_m0_s0", dsMainM0S0);
        dataSourceMap.put("ds_xxx_m0", dsXxxM0);

        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        shardingRuleConfig.setDefaultDataSourceName("defaultDb");
        shardingRuleConfig.setMasterSlaveRuleConfigs(getMasterSlaveRuleConfigurations());

        Properties properties = new Properties();
        properties.setProperty("sql.show", Boolean.TRUE.toString());

        // 获取数据源对象
        DataSource shardingDataSource =
            ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, Maps.newHashMap(),
                properties);

        ThreadLocalRoutingDataSource threadLocalRoutingDataSource = new ThreadLocalRoutingDataSource();
        threadLocalRoutingDataSource.setDefaultTargetDataSource(shardingDataSource);

        Map<Object, Object> targetDataSources = Maps.newHashMap();
        targetDataSources.put("ds_main", shardingDataSource);
        targetDataSources.put("ds_xxx_m0", dsXxxM0);
        threadLocalRoutingDataSource.setTargetDataSources(targetDataSources);

        return threadLocalRoutingDataSource;
    }

    private List<MasterSlaveRuleConfiguration> getMasterSlaveRuleConfigurations() {
        MasterSlaveRuleConfiguration dsMain0 =
            new MasterSlaveRuleConfiguration("ds_main0", "ds_main_m0", Arrays.asList("ds_main_m0_s0"));
        return Lists.newArrayList(dsMain0);
    }

    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactoryBean(@Qualifier("dataSource") DataSource dataSource) {
        MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        mybatisSqlSessionFactoryBean.setDataSource(dataSource);
        try {
            mybatisSqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:sqlMapper/*Mapper.xml"));
            mybatisSqlSessionFactoryBean.setGlobalConfig(globalConfig());
            mybatisSqlSessionFactoryBean.setConfiguration(mybatisConfiguration());
            return mybatisSqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            log.error("SqlSessionFactoryBean init error.", e);
        }
        return null;
    }

    /**
     * MyBatisPlus 配置
     *
     * @return
     */
    private MybatisConfiguration mybatisConfiguration() {
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        // 这个配置使全局的映射器启用或禁用缓存
        mybatisConfiguration.setCacheEnabled(true);
        // 允许 JDBC 支持生成的键。需要适合的驱动。如果设置为 true 则这个设置强制生成的键被使用，尽管一些驱动拒绝兼容但仍然有效（比如 Derby）
        mybatisConfiguration.setUseGeneratedKeys(true);
        // 配置默认的执行器。SIMPLE 执行器没有什么特别之处。REUSE 执行器重用预处理语句。BATCH 执行器重用语句和批量更新
        mybatisConfiguration.setDefaultExecutorType(ExecutorType.REUSE);
        // 全局启用或禁用延迟加载。当禁用时，所有关联对象都会即时加载
        mybatisConfiguration.setLazyLoadingEnabled(true);
        // 设置超时时间，它决定驱动等待一个数据库响应的时间
        mybatisConfiguration.setDefaultStatementTimeout(2500);
        // 是否开启自动驼峰命名规则
        mybatisConfiguration.setMapUnderscoreToCamelCase(false);
        mybatisConfiguration.setJdbcTypeForNull(JdbcType.NULL);
        // 新的分页插件，一缓和二缓遵循mybatis的规则，需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题(该属性会在旧插件移除后一同移除)
        mybatisConfiguration.setUseDeprecatedExecutor(false);
        return mybatisConfiguration;
    }

    /**
     * 定义 DB 配置
     *
     * @return
     */
    private GlobalConfig.DbConfig dbConfig() {
        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
        dbConfig.setIdType(IdType.ASSIGN_UUID);
        dbConfig.setLogicDeleteField("isDel");
        dbConfig.setLogicDeleteValue("1");
        dbConfig.setLogicNotDeleteValue("0");
        return dbConfig;
    }

    /**
     * 定义 MP 全局策略
     *
     * @return
     */
    private GlobalConfig globalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setBanner(false);
        globalConfig.setDbConfig(dbConfig());
        return globalConfig;
    }


    /**
     * Druid 监控配置
     *
     * @return
     */
    @Bean
    public DruidStatInterceptor druidStatInterceptor() {
        return new DruidStatInterceptor();
    }

    /**
     * Druid 监控配置 切入点
     *
     * @param druidStatInterceptor
     * @return
     */
    @Bean
    public RegexpMethodPointcutAdvisor regexpMethodPointcutAdvisor(DruidStatInterceptor druidStatInterceptor) {
        RegexpMethodPointcutAdvisor regexpMethodPointcutAdvisor = new RegexpMethodPointcutAdvisor();
        regexpMethodPointcutAdvisor.setPatterns("execution(public * com..dao.*.*(..))",
            "execution(public * com..manager.*.*(..))", "execution(public * com..service.*.*(..))");
        regexpMethodPointcutAdvisor.setAdvice(druidStatInterceptor);
        return regexpMethodPointcutAdvisor;
    }

    /**
     * 设置 ProxyTargetClass 为 True
     *
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    /**
     * MyBatisPlus 分页插件
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        // 针对 update 和 delete 语句 作用: 阻止恶意的全表更新删除
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        // SQL 性能规范
        interceptor.addInnerInterceptor(new IllegalSQLInnerInterceptor());
        return interceptor;
    }

    /**
     * 切面配置：DAO 层方法执行日志
     *
     * @return
     */
    @Bean
    public DaoCostLogAspect daoCostLogAspect() {
        return new DaoCostLogAspect();
    }
}
