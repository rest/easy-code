package com.jeasy.role.dao;

import com.jeasy.role.po.RolePO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 角色 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface RoleDAO extends BaseDAO<RolePO> {
}
