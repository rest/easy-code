package com.jeasy.log.dao;

import com.jeasy.log.po.LogPO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 日志 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
public interface LogDAO extends BaseDAO<LogPO> {
}
