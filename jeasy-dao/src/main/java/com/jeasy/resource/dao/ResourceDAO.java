package com.jeasy.resource.dao;

import com.jeasy.resource.po.ResourcePO;
import com.javabooter.core.dao.BaseDAO;

/**
 * 菜单 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface ResourceDAO extends BaseDAO<ResourcePO> {
}
