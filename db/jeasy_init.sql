CREATE DATABASE IF NOT EXISTS `jeasy` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `jeasy`;
--
-- 用户管理-用户
--
DROP TABLE IF EXISTS `su_user`;
CREATE TABLE `su_user` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `loginName` VARCHAR(64) NOT NULL COMMENT '登录名',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `pwd` VARCHAR(64) NOT NULL COMMENT '密码',
  `salt` VARCHAR(64) DEFAULT '' COMMENT '加密盐',
  `mobile` VARCHAR(11) DEFAULT '' COMMENT '手机号',

  `statusVal` INT(4) DEFAULT 0 COMMENT '用户状态值:1000=启用,1001=停用',
  `statusCode` VARCHAR(16) DEFAULT '' COMMENT '用户状态编码:字典',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

--
-- 用户管理-用户角色
--
DROP TABLE IF EXISTS `su_user_role`;
CREATE TABLE `su_user_role` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `userId` VARCHAR(64) NOT NULL COMMENT '用户ID',
  `userName` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `userCode` VARCHAR(64) DEFAULT '' COMMENT '用户编码',

  `roleId` VARCHAR(64) NOT NULL COMMENT '角色ID',
  `roleName` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `roleCode` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色';

--
-- 用户管理-用户机构
--
DROP TABLE IF EXISTS `su_user_org`;
CREATE TABLE `su_user_org` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `userId` VARCHAR(64) NOT NULL COMMENT '用户ID',
  `userName` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `userCode` VARCHAR(64) DEFAULT '' COMMENT '用户标示',

  `orgId` VARCHAR(64) NOT NULL COMMENT '机构ID',
  `orgName` VARCHAR(32) DEFAULT '' COMMENT '机构名称',
  `orgCode` VARCHAR(64) DEFAULT '' COMMENT '机构编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户机构';


--
-- 权限管理-角色
--
DROP TABLE IF EXISTS `su_role`;
CREATE TABLE `su_role` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色';

--
-- 权限管理-资源(菜单&操作)
--
DROP TABLE IF EXISTS `su_resource`;
CREATE TABLE `su_resource` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(32) DEFAULT '' COMMENT '编码',
  `url` VARCHAR(128) DEFAULT '' COMMENT 'URL',
  `icon` VARCHAR(64) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `pid` VARCHAR(64) DEFAULT '' COMMENT '父ID',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `isMenu` TINYINT(1) NOT NULL COMMENT '是否菜单:0=否,1=是',
  `isLeaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单';

--
-- 权限管理-角色资源
--
DROP TABLE IF EXISTS `su_role_resource`;
CREATE TABLE `su_role_resource` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `roleId` VARCHAR(64) NOT NULL COMMENT '角色ID',
  `roleName` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `roleCode` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `resourceId` VARCHAR(64) NOT NULL COMMENT '资源ID',
  `resourceName` VARCHAR(32) DEFAULT '' COMMENT '资源名称',
  `resourceCode` VARCHAR(64) DEFAULT '' COMMENT '资源编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色资源';

--
-- 权限管理-机构
--
DROP TABLE IF EXISTS `su_organization`;
CREATE TABLE `su_organization` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `address` VARCHAR(128) DEFAULT '' COMMENT '地址',

  `typeVal` INT(4) DEFAULT 0 COMMENT '机构类型值',
  `typeCode` VARCHAR(16) DEFAULT '' COMMENT '机构类型编码:字典',

  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `isLeaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',
  `pid` VARCHAR(64) DEFAULT '' COMMENT '父ID',
  `icon` VARCHAR(128) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机构';

--
-- 基础数据-日志
--
DROP TABLE IF EXISTS `bd_log`;
CREATE TABLE `bd_log` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `tableName` VARCHAR(32) NOT NULL COMMENT '表名称',
  `recordId` VARCHAR(64) NOT NULL COMMENT '记录ID',
  `fieldName` VARCHAR(128) DEFAULT '' COMMENT '字段名称',

  `logTypeVal` INT(4) DEFAULT 0 COMMENT '日志类型值',
  `logTypeCode` VARCHAR(16) DEFAULT '' COMMENT '日志类型编码:字典',

  `optTypeVal` INT(4) DEFAULT 0 COMMENT '操作类型值',
  `optTypeCode` VARCHAR(16) DEFAULT '' COMMENT '操作类型编码:字典',
  `optDesc` VARCHAR(128) DEFAULT '' COMMENT '操作类型描述',

  `beforeValue` VARCHAR(128) DEFAULT '' COMMENT '操作前值',
  `afterValue` VARCHAR(128) DEFAULT '' COMMENT '操作后值',

  `remark` VARCHAR(255) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='日志';

--
-- 基础数据-字典
--
DROP TABLE IF EXISTS `bd_dictionary`;
CREATE TABLE `bd_dictionary` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(16) NOT NULL COMMENT '编号',
  `value` INT(4) NOT NULL COMMENT '值',

  `type` VARCHAR(16) DEFAULT '' COMMENT '类型',
  `typeName` VARCHAR(32) DEFAULT '' COMMENT '类型名称',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',

  `pid` VARCHAR(64) DEFAULT '' COMMENT '父ID',
  `pcode` VARCHAR(16) DEFAULT '' COMMENT '父编号',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典';

--
-- 基础数据-文件附件
--
DROP TABLE IF EXISTS `bd_file_attach`;
CREATE TABLE `bd_file_attach` (
  `id` VARCHAR(64) NOT NULL COMMENT '主键',

  `tableName` VARCHAR(32) NOT NULL COMMENT '表名称',
  `recordId` VARCHAR(64) NOT NULL COMMENT '记录ID',

  `name` VARCHAR(64) DEFAULT '' COMMENT '文件原名称',
  `url` VARCHAR(255) DEFAULT '' COMMENT '文件URL',
  `iconUrl` VARCHAR(255) DEFAULT '' COMMENT '文件图标URL',
  `previewUrl` VARCHAR(255) DEFAULT '' COMMENT '文件预览URL',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` VARCHAR(64) DEFAULT '' COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` VARCHAR(64) DEFAULT '' COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文件附件';

--
-- 初始化:字典
--
TRUNCATE `bd_dictionary`;
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a771', '启用', 'QY', 1000, 'YHZT', '用户状态', 0, '0', '', 1513563952466, '0', 'SYSTEM', 1513563952466, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a772', '停用', 'TY', 1001, 'YHZT', '用户状态', 0, '0', '', 1513563952490, '0', 'SYSTEM', 1513563952490, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a773', '其他', 'QT', 2000, 'JGLX', '机构类型', 0, '0', '', 1513563952491, '0', 'SYSTEM', 1513563952491, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a774', '登录登出', 'DLDC', 3000, 'RZLX', '日志类型', 0, '0', '', 1513563952491, '0', 'SYSTEM', 1513563952491, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a775', '登录', 'DL', 3001, 'CZLX', '操作类型', 0, '0d61f71f34b0305aabc5d1cdd9d2a774', '', 1513563952491, '0', 'SYSTEM', 1513563952491, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a776', '登出', 'DC', 3002, 'CZLX', '操作类型', 0, '0d61f71f34b0305aabc5d1cdd9d2a774', '', 1513563952491, '0', 'SYSTEM', 1513563952491, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a777', '启用停用', 'QYTY', 4000, 'RZLX', '日志类型', 0, '0', '', 1513563952491, '0', 'SYSTEM', 1513563952491, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a778', '启用', 'QY', 4001, 'CZLX', '操作类型', 0, '0d61f71f34b0305aabc5d1cdd9d2a777', '', 1513563952492, '0', 'SYSTEM', 1513563952492, '0', 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('0d61f71f34b0305aabc5d1cdd9d2a779', '停用', 'TY', 4002, 'CZLX', '操作类型', 0, '0d61f71f34b0305aabc5d1cdd9d2a777', '', 1513563952492, '0', 'SYSTEM', 1513563952492, '0', 'SYSTEM', 0, 0);

--
-- 初始化:菜单资源+角色资源
--
TRUNCATE `su_resource`;
TRUNCATE `su_role_resource`;
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a771', '用户管理', 'YHGL', '', 'ios-people', '', '', 0, 1, 0, 1513146159132, '0', 'SYSTEM', 1513146159132, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a771', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a771', '用户管理', 'YHGL', 1513146159148, '0', 'SYSTEM', 1513146159148, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a772', '人员管理', 'YHGL:RYGL', '/user', 'ios-body', '', '1d61f71f34b0305aabc5d1cdd9d2a771', 0, 1, 1, 1513146159148, '0', 'SYSTEM', 1513146159148, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a772', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a772', '人员管理', 'YHGL:RYGL', 1513146159148, '0', 'SYSTEM', 1513146159148, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a773', '查询', 'YHGL:RYGL:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a773', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a773', '查询', 'YHGL:RYGL:CX', 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a774', '查看', 'YHGL:RYGL:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a774', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a774', '查看', 'YHGL:RYGL:CK', 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a775', '新增', 'YHGL:RYGL:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a775', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a775', '新增', 'YHGL:RYGL:XZ', 1513146159149, '0', 'SYSTEM', 1513146159149, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a776', '编辑', 'YHGL:RYGL:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159150, '0', 'SYSTEM', 1513146159150, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a776', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a776', '编辑', 'YHGL:RYGL:BJ', 1513146159150, '0', 'SYSTEM', 1513146159150, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a777', '删除', 'YHGL:RYGL:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159150, '0', 'SYSTEM', 1513146159150, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a777', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a777', '删除', 'YHGL:RYGL:SC', 1513146159150, '0', 'SYSTEM', 1513146159150, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a778', '角色配置', 'YHGL:RYGL:JSPZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159150, '0', 'SYSTEM', 1513146159150, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a778', '2d61f71f34b0305aabc5d1cdd9d2a771','超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a778', '角色配置', 'YHGL:RYGL:JSPZ', 1513146159151, '0', 'SYSTEM', 1513146159151, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d2a779', '机构配置', 'YHGL:RYGL:JGPZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d2a772', 0, 0, 1, 1513146159151, '0', 'SYSTEM', 1513146159151, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d2a779', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d2a779', '机构配置', 'YHGL:RYGL:JGPZ', 1513146159151, '0', 'SYSTEM', 1513146159151, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27710', '角色管理', 'YHGL:JSGL', '/role', 'ios-person', '', '1d61f71f34b0305aabc5d1cdd9d2a771',0, 1, 1, 1513146159151, '0', 'SYSTEM', 1513146159151, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27710', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27710', '角色管理', 'YHGL:JSGL', 1513146159151, '0', 'SYSTEM', 1513146159151, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27711', '查询', 'YHGL:JSGL:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159152, '0', 'SYSTEM', 1513146159152, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27711', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27711', '查询', 'YHGL:JSGL:CX', 1513146159152, '0', 'SYSTEM', 1513146159152, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27712', '查看', 'YHGL:JSGL:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159152, '0', 'SYSTEM', 1513146159152, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27712', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27712', '查看', 'YHGL:JSGL:CK', 1513146159152, '0', 'SYSTEM', 1513146159152, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27713', '新增', 'YHGL:JSGL:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159152, '0', 'SYSTEM', 1513146159152, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27713', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27713', '新增', 'YHGL:JSGL:XZ', 1513146159153, '0', 'SYSTEM', 1513146159153, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27714', '编辑', 'YHGL:JSGL:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159153, '0', 'SYSTEM', 1513146159153, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27714', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27714', '编辑', 'YHGL:JSGL:BJ', 1513146159153, '0', 'SYSTEM', 1513146159153, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27715', '删除', 'YHGL:JSGL:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159153, '0', 'SYSTEM', 1513146159153, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27715', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27715', '删除', 'YHGL:JSGL:SC', 1513146159153, '0', 'SYSTEM', 1513146159153, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27716', '权限配置', 'YHGL:JSGL:QXPZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27710', 0, 0, 1, 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27716', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27716', '权限配置', 'YHGL:JSGL:QXPZ', 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27717', '组织机构', 'YHGL:ZZJG', '/organization', 'ios-people', '', '1d61f71f34b0305aabc5d1cdd9d2a771', 0, 1, 1, 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27717', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27717', '组织机构', 'YHGL:ZZJG', 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27718', '查询', 'YHGL:ZZJG:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27717', 0, 0, 1, 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27718', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27718', '查询', 'YHGL:ZZJG:CX', 1513146159154, '0', 'SYSTEM', 1513146159154, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27719', '查看', 'YHGL:ZZJG:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27717', 0, 0, 1, 1513146159155, '0', 'SYSTEM', 1513146159155, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27719', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27719', '查看', 'YHGL:ZZJG:CK', 1513146159155, '0', 'SYSTEM', 1513146159155, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27720', '新增', 'YHGL:ZZJG:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27717', 0, 0, 1, 1513146159155, '0', 'SYSTEM', 1513146159155, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27720', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27720', '新增', 'YHGL:ZZJG:XZ', 1513146159155, '0', 'SYSTEM', 1513146159155, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27721', '编辑', 'YHGL:ZZJG:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27717', 0, 0, 1, 1513146159155, '0', 'SYSTEM', 1513146159155, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27721', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27721', '编辑', 'YHGL:ZZJG:BJ', 1513146159156, '0', 'SYSTEM', 1513146159156, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27722', '删除', 'YHGL:ZZJG:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27717', 0, 0, 1, 1513146159156, '0', 'SYSTEM', 1513146159156, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27722', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27722', '删除', 'YHGL:ZZJG:SC', 1513146159156, '0', 'SYSTEM', 1513146159156, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27723', '菜单资源', 'YHGL:CDZY', '/resource', 'android-menu', '', '1d61f71f34b0305aabc5d1cdd9d2a771', 0, 1, 1, 1513146159156, '0', 'SYSTEM', 1513146159156, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27723', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27723', '菜单资源', 'YHGL:CDZY', 1513146159156, '0', 'SYSTEM', 1513146159156, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27724', '查询', 'YHGL:CDZY:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27723', 0, 0, 1, 1513146159157, '0', 'SYSTEM', 1513146159157, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27724', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27724', '查询', 'YHGL:CDZY:CX', 1513146159157, '0', 'SYSTEM', 1513146159157, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27725', '查看', 'YHGL:CDZY:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27723', 0, 0, 1, 1513146159157, '0', 'SYSTEM', 1513146159157, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27725', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27725', '查看', 'YHGL:CDZY:CK', 1513146159157, '0', 'SYSTEM', 1513146159157, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27726', '新增', 'YHGL:CDZY:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27723', 0, 0, 1, 1513146159157, '0', 'SYSTEM', 1513146159157, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27726', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27726', '新增', 'YHGL:CDZY:XZ', 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27727', '编辑', 'YHGL:CDZY:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27723', 0, 0, 1, 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27727', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27727', '编辑', 'YHGL:CDZY:BJ', 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27728', '删除', 'YHGL:CDZY:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27723', 0, 0, 1, 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27728', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27728', '删除', 'YHGL:CDZY:SC', 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27729', '基础数据', 'JCSJ', '', 'settings', '', '', 0, 1, 0, 1513146159158, '0', 'SYSTEM', 1513146159158, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27729', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27729', '基础数据', 'JCSJ', 1513146159159, '0', 'SYSTEM', 1513146159159, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27730', '公共码表', 'JCSJ:GGMB', '/dictionary', 'ios-book', '', '1d61f71f34b0305aabc5d1cdd9d27729', 0, 1, 1, 1513146159159, '0', 'SYSTEM', 1513146159159, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27730', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27730', '公共码表', 'JCSJ:GGMB', 1513146159159, '0', 'SYSTEM', 1513146159159, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27731', '查询', 'JCSJ:GGMB:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27730', 0, 0, 1, 1513146159159, '0', 'SYSTEM', 1513146159159, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27731', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27731', '查询', 'JCSJ:GGMB:CX', 1513146159160, '0', 'SYSTEM', 1513146159160, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27732', '查看', 'JCSJ:GGMB:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27730', 0, 0, 1, 1513146159160, '0', 'SYSTEM', 1513146159160, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27732', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27732', '查看', 'JCSJ:GGMB:CK', 1513146159160, '0', 'SYSTEM', 1513146159160, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27733', '新增', 'JCSJ:GGMB:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27730', 0, 0, 1, 1513146159161, '0', 'SYSTEM', 1513146159161, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27733', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27733', '新增', 'JCSJ:GGMB:XZ', 1513146159161, '0', 'SYSTEM', 1513146159161, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27734', '编辑', 'JCSJ:GGMB:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27730', 0, 0, 1, 1513146159161, '0', 'SYSTEM', 1513146159161, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27734', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27734', '编辑', 'JCSJ:GGMB:BJ', 1513146159161, '0', 'SYSTEM', 1513146159161, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27735', '删除', 'JCSJ:GGMB:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27730', 0, 0, 1, 1513146159162, '0', 'SYSTEM', 1513146159162, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27735', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27735', '删除', 'JCSJ:GGMB:SC', 1513146159162, '0', 'SYSTEM', 1513146159162, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27736', '文件管理', 'JCSJ:WJGL', '/fileAttach', 'ios-folder', '', '1d61f71f34b0305aabc5d1cdd9d27729', 0, 1, 1, 1513146159162, '0', 'SYSTEM', 1513146159162, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27736', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27736', '文件管理', 'JCSJ:WJGL', 1513146159162, '0', 'SYSTEM', 1513146159162, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27737', '查询', 'JCSJ:WJGL:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27736', 0, 0, 1, 1513146159163, '0', 'SYSTEM', 1513146159163, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27737', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27737', '查询', 'JCSJ:WJGL:CX', 1513146159163, '0', 'SYSTEM', 1513146159163, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27738', '查看', 'JCSJ:WJGL:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27736', 0, 0, 1, 1513146159163, '0', 'SYSTEM', 1513146159163, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27738', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27738', '查看', 'JCSJ:WJGL:CK', 1513146159163, '0', 'SYSTEM', 1513146159163, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27739', '新增', 'JCSJ:WJGL:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27736', 0, 0, 1, 1513146159164, '0', 'SYSTEM', 1513146159164, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27739', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27739', '新增', 'JCSJ:WJGL:XZ', 1513146159164, '0', 'SYSTEM', 1513146159164, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27740', '编辑', 'JCSJ:WJGL:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27736', 0, 0, 1, 1513146159164, '0', 'SYSTEM', 1513146159164, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27740', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27740', '编辑', 'JCSJ:WJGL:BJ', 1513146159164, '0', 'SYSTEM', 1513146159164, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27741', '删除', 'JCSJ:WJGL:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27736', 0, 0, 1, 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27741', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27741', '删除', 'JCSJ:WJGL:SC', 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27742', '代码平台', 'DMPT', '', 'code-working', '', '', 0, 1, 0, 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27742', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27742', '代码平台', 'DMPT', 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27743', '接口文档', 'DMPT:JKWD', '/api', 'usb', '', '1d61f71f34b0305aabc5d1cdd9d27742', 0, 1, 1, 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27743', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27743', '接口文档', 'DMPT:JKWD', 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27744', '查询', 'DMPT:JKWD:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27743', 0, 0, 1, 1513146159165, '0', 'SYSTEM', 1513146159165, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27744', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27744', '查询', 'DMPT:JKWD:CX', 1513146159166, '0', 'SYSTEM', 1513146159166, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27745', '查看', 'DMPT:JKWD:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27743', 0, 0, 1, 1513146159166, '0', 'SYSTEM', 1513146159166, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27745', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27745', '查看', 'DMPT:JKWD:CK', 1513146159166, '0', 'SYSTEM', 1513146159166, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27746', '新增', 'DMPT:JKWD:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27743', 0, 0, 1, 1513146159166, '0', 'SYSTEM', 1513146159166, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27746', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27746', '新增', 'DMPT:JKWD:XZ', 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27747', '编辑', 'DMPT:JKWD:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27743', 0, 0, 1, 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27747', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27747', '编辑', 'DMPT:JKWD:BJ', 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27748', '删除', 'DMPT:JKWD:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27743', 0, 0, 1, 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27748', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27748', '删除', 'DMPT:JKWD:SC', 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27749', '代码生成', 'DMPT:DMSC', '/code', 'code-download', '', '1d61f71f34b0305aabc5d1cdd9d27742', 0, 1, 1, 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27749', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27749', '代码生成', 'DMPT:DMSC', 1513146159167, '0', 'SYSTEM', 1513146159167, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27750', '查询', 'DMPT:DMSC:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27749', 0, 0, 1, 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27750', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27750', '查询', 'DMPT:DMSC:CX', 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27751', '查看', 'DMPT:DMSC:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27749', 0, 0, 1, 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27751', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27751', '查看', 'DMPT:DMSC:CK', 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27752', '新增', 'DMPT:DMSC:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27749', 0, 0, 1, 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27752', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27752', '新增', 'DMPT:DMSC:XZ', 1513146159168, '0', 'SYSTEM', 1513146159168, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27753', '编辑', 'DMPT:DMSC:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27749', 0, 0, 1, 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27753', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27753', '编辑', 'DMPT:DMSC:BJ', 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27754', '删除', 'DMPT:DMSC:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27749', 0, 0, 1, 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27754', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27754', '删除', 'DMPT:DMSC:SC', 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27755', '日志监控', 'RZJK', '', 'monitor', '', '', 0, 1, 0, 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27755', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27755', '日志监控', 'RZJK', 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27756', '操作日志', 'RZJK:CZRZ', '/log', 'ios-paper', '', '1d61f71f34b0305aabc5d1cdd9d27755', 0, 1, 1, 1513146159169, '0', 'SYSTEM', 1513146159169, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27756', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27756', '操作日志', 'RZJK:CZRZ', 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27757', '查询', 'RZJK:CZRZ:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27756', 0, 0, 1, 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27757', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27757', '查询', 'RZJK:CZRZ:CX', 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27758', '查看', 'RZJK:CZRZ:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27756', 0, 0, 1, 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27758', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27758', '查看', 'RZJK:CZRZ:CK', 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27759', '新增', 'RZJK:CZRZ:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27756', 0, 0, 1, 1513146159170, '0', 'SYSTEM', 1513146159170, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27759', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27759', '新增', 'RZJK:CZRZ:XZ', 1513146159171, '0', 'SYSTEM', 1513146159171, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27760', '编辑', 'RZJK:CZRZ:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27756', 0, 0, 1, 1513146159171, '0', 'SYSTEM', 1513146159171, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27760', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27760', '编辑', 'RZJK:CZRZ:BJ', 1513146159171, '0', 'SYSTEM', 1513146159171, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27761', '删除', 'RZJK:CZRZ:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27756', 0, 0, 1, 1513146159171, '0', 'SYSTEM', 1513146159171, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27761', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27761', '删除', 'RZJK:CZRZ:SC', 1513146159171, '0', 'SYSTEM', 1513146159171, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27762', '数据监控', 'RZJK:SJJK', '/druid', 'ios-analytics', '', '1d61f71f34b0305aabc5d1cdd9d27755', 0, 1, 1, 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27762', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27762', '数据监控', 'RZJK:SJJK', 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27763', '查询', 'RZJK:SJJK:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27762', 0, 0, 1, 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27763', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27763', '查询', 'RZJK:SJJK:CX', 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27764', '查看', 'RZJK:SJJK:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27762', 0, 0, 1, 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27764', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27764', '查看', 'RZJK:SJJK:CK', 1513146159172, '0', 'SYSTEM', 1513146159172, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27765', '新增', 'RZJK:SJJK:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27762', 0, 0, 1, 1513146159173, '0', 'SYSTEM', 1513146159173, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27765', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27765', '新增', 'RZJK:SJJK:XZ', 1513146159173, '0', 'SYSTEM', 1513146159173, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27766', '编辑', 'RZJK:SJJK:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27762', 0, 0, 1, 1513146159173, '0', 'SYSTEM', 1513146159173, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27766', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27766', '编辑', 'RZJK:SJJK:BJ', 1513146159173, '0', 'SYSTEM', 1513146159173, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27767', '删除', 'RZJK:SJJK:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27762', 0, 0, 1, 1513146159173, '0', 'SYSTEM', 1513146159173, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27767', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27767', '删除', 'RZJK:SJJK:SC', 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27768', '接口监控', 'RZJK:JKJK', '/monitor', 'usb', '', '1d61f71f34b0305aabc5d1cdd9d27755', 0, 1, 1, 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27768', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27768', '接口监控', 'RZJK:JKJK', 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27769', '查询', 'RZJK:JKJK:CX', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27768', 0, 0, 1, 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27769', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27769', '查询', 'RZJK:JKJK:CX', 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27770', '查看', 'RZJK:JKJK:CK', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27768', 0, 0, 1, 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27770', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27770', '查看', 'RZJK:JKJK:CK', 1513146159174, '0', 'SYSTEM', 1513146159174, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27771', '新增', 'RZJK:JKJK:XZ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27768', 0, 0, 1, 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27771', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27771', '新增', 'RZJK:JKJK:XZ', 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27772', '编辑', 'RZJK:JKJK:BJ', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27768', 0, 0, 1, 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27772', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27772', '编辑', 'RZJK:JKJK:BJ', 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('1d61f71f34b0305aabc5d1cdd9d27773', '删除', 'RZJK:JKJK:SC', '', '', '', '1d61f71f34b0305aabc5d1cdd9d27768', 0, 0, 1, 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES ('7d61f71f34b0305aabc5d1cdd9d27773', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '1d61f71f34b0305aabc5d1cdd9d27773', '删除', 'RZJK:JKJK:SC', 1513146159175, '0', 'SYSTEM', 1513146159175, '0', 'SYSTEM', 0, 0);

--
-- 初始化:角色
--
TRUNCATE `su_role`;
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`)
VALUES ('2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', '谨慎操作', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);

--
-- 初始化:机构
--
TRUNCATE `su_organization`;
INSERT INTO `su_organization` (`id`, `name`, `code`, `typeVal`, `typeCode`, `sort`, `isLeaf`, `pid`, `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('3d61f71f34b0305aabc5d1cdd9d2a771', '超级管理组', 'CJGLZ', 2000, 'QT', 0, 1, '', '谨慎操作',1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);

--
-- 初始化:用户+用户机构+用户角色
--
TRUNCATE `su_user`;
INSERT INTO `su_user` (`id`, `name`, `loginName`, `code`, `pwd`, `salt`, `mobile`, `statusVal`, `statusCode`,  `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('4d61f71f34b0305aabc5d1cdd9d2a771', 'admin', 'admin', 'ADMIN', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13011111111', 1000, 'QY', '谨慎操作', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `loginName`, `code`, `pwd`, `salt`, `mobile`, `statusVal`, `statusCode`,  `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('4d61f71f34b0305aabc5d1cdd9d2a772', 'super_admin', 'super_admin', 'SUPER_ADMIN', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13022222222', 1000, 'QY', '谨慎操作', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);

TRUNCATE `su_user_org`;
INSERT INTO `su_user_org` (`id`, `userId`, `userName`, `userCode`, `orgId`, `orgName`, `orgCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('5d61f71f34b0305aabc5d1cdd9d2a771', '4d61f71f34b0305aabc5d1cdd9d2a771', 'admin', 'ADMIN', '3d61f71f34b0305aabc5d1cdd9d2a771', '超级管理组', 'CJGLZ', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `userId`, `userName`, `userCode`, `orgId`, `orgName`, `orgCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('5d61f71f34b0305aabc5d1cdd9d2a772', '4d61f71f34b0305aabc5d1cdd9d2a772', 'super_admin', 'SUPER_ADMIN', '3d61f71f34b0305aabc5d1cdd9d2a771', '超级管理组', 'CJGLZ', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);

TRUNCATE `su_user_role`;
INSERT INTO `su_user_role` (`id`, `userId`, `userName`, `userCode`, `roleId`, `roleName`, `roleCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('6d61f71f34b0305aabc5d1cdd9d2a771', '4d61f71f34b0305aabc5d1cdd9d2a771', 'admin', 'ADMIN', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `userId`, `userName`, `userCode`, `roleId`, `roleName`, `roleCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  ('6d61f71f34b0305aabc5d1cdd9d2a772', '4d61f71f34b0305aabc5d1cdd9d2a772', 'super_admin', 'SUPER_ADMIN', '2d61f71f34b0305aabc5d1cdd9d2a771', '超级管理员', 'CJGLY', 1491805287470, '0', 'SYSTEM', 1491805287470, '0', 'SYSTEM', 0, 0);
