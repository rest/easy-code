package com.jeasy.leetcode.twopoint;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/24 23:26
 */
public class JudgeSquareSum {

    /**
     * 判断一个非负整数是否为两个整数的平方和 => 时间复杂度O(sqrt(target)), 空间复杂度O(1)
     * 可以看成，在元素为 0~target 的有序数组中查找两个数，使得这两个数的平方和为 target
     *
     * @param target 非负整数
     * @return
     */
    public static boolean judgeSquareSum(int target) {
        if (target < 0) {
            return false;
        }

        int i = 0; // 左指针固定为 0
        int j = (int) Math.sqrt(target); // 右指针的初始化，实现剪枝，从而降低时间复杂度，为了使 i2 + j2 的值尽可能接近 target，我们可以将 j 取为 sqrt(target)
        while (i <= j) {
            int powSum = i * i + j * j;
            if (powSum == target) {
                return true;
            } else if (powSum < target) {
                i++;
            } else {
                j--;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(judgeSquareSum(3));
    }
}
