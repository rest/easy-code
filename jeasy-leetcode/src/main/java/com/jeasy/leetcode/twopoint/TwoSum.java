package com.jeasy.leetcode.twopoint;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/24 23:09
 */
public class TwoSum {

    /**
     * 有序数组的 Two Sum => 时间复杂度O(n), 空间复杂度O(1)
     *
     * @param numbers 有序数组
     * @param target  目标值
     * @return
     */
    public static int[] twoSum(int[] numbers, int target) {
        if (numbers == null) {
            return null;
        }

        int i = 0; // 一个指针指向值较小的元素，从头向尾遍历
        int j = numbers.length - 1; // 一个指针指向值较大的元素，从尾向头遍历
        while (i < j) {
            int sum = numbers[i] + numbers[j];
            if (sum == target) {
                // 如果两个指针指向元素的和 sum == target，那么得到要求的结果
                return new int[] {i + 1, j + 1};
            } else if (sum < target) {
                // 如果 sum < target，移动较小的元素，使 sum 变大一些
                i++;
            } else {
                // 如果 sum > target，移动较大的元素，使 sum 变小一些
                j--;
            }
        }

        return null;
    }

    public static void main(String[] args) {
        int[] result = twoSum(new int[] {1, 2, 4, 5}, 6);
        for (int r : result) {
            System.out.println(r);
        }
    }
}
