package com.jeasy.leetcode.twopoint;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/25 00:08
 */
public class HasCycle {

    /**
     * 判断链表是否存在环 => 时间复杂度O(n)，空间复杂度O(1)
     *
     * @param head
     * @return
     */
    public static boolean hasCycle(ListNode head) {
        if (head == null) {
            return false;
        }

        ListNode l1 = head;
        ListNode l2 = head.next;
        while (l1 != null && l2 != null && l2.next != null) {
            if (l1 == l2) {
                // 如果存在环，那么这两个指针一定会相遇
                return true;
            }
            // 一个指针每次移动一个节点
            l1 = l1.next;
            // 一个指针每次移动两个节点
            l2 = l2.next.next;
        }
        return false;
    }

    public static void main(String[] args) {

    }
}

class ListNode {
    public ListNode next;
}
