package com.jeasy.leetcode.twopoint;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/25 00:08
 */
public class MergeSort {

    /**
     * 归并两个有序数组，把归并结果存到第一个数组上
     *
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    public static void mergeSort(int[] nums1, int m, int[] nums2, int n) {
        // 需要从尾开始遍历，否则在 nums1 上归并得到的值会覆盖还未进行归并比较的值。
        int index1 = m - 1;
        int index2 = n - 1;
        int indexMerge = m + n - 1;
        while (index1 >= 0 || index2 >= 0) {
            if (index1 < 0) {
                nums1[indexMerge--] = nums2[index2--];
            } else if (index2 < 0) {
                nums1[indexMerge--] = nums1[index1--];
            } else if (nums1[index1] > nums2[index2]) {
                nums1[indexMerge--] = nums1[index1--];
            } else {
                nums1[indexMerge--] = nums2[index2--];
            }
        }

    }

    public static void main(String[] args) {
        int[] nums1 = new int[] {1, 2, 3, 0, 0, 0};
        int[] nums2 = new int[] {2, 5, 6};

        mergeSort(nums1, 3, nums2, 3);
        for (int num : nums1) {
            System.out.println(num);
        }
    }
}
