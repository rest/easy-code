package com.jeasy.leetcode.twopoint;

import java.util.Arrays;
import java.util.HashSet;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/24 23:52
 */
public class ReverseVowels {

    private final static HashSet<Character> vowels =
        new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));

    /**
     * 反转字符串中的元音字符 => 时间复杂度O(n), 空间复杂度为O(1)
     *
     * @param s
     * @return
     */
    public static String reverseVowels(String s) {
        if (s == null) {
            return null;
        }

        int i = 0;
        int j = s.length() - 1;
        char[] result = new char[s.length()];
        while (i <= j) {
            char ci = s.charAt(i);
            char cj = s.charAt(j);
            if (!vowels.contains(ci)) {
                // 一个指针从头向尾遍历
                result[i++] = ci;
            } else if (!vowels.contains(cj)) {
                // 一个指针从尾到头遍历
                result[j--] = cj;
            } else {
                // 当两个指针都遍历到元音字符时，交换这两个元音字符
                result[i++] = cj;
                result[j--] = ci;
            }
        }
        return new String(result);
    }

    public static void main(String[] args) {
        System.out.println(reverseVowels("leetcode"));
    }
}
