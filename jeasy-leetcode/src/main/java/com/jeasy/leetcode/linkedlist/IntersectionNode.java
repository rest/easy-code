package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 20:32
 */
public class IntersectionNode {

    /**
     * 找出两个链表的交点
     *
     * 如果只是判断是否存在交点，那么就是另一个问题，即 编程之美 3.6 的问题。有两种解法：
     *
     * 1. 把第一个链表的结尾连接到第二个链表的开头，看第二个链表是否存在环。
     * 2. 或者直接比较两个链表的最后一个节点是否相同。
     *
     * @param headA
     * @param headB
     * @return
     */
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode l1 = headA;
        ListNode l2 = headB;
        while (l1 != l2) {
            // 当访问 A 链表的指针访问到链表尾部时，令它从链表 B 的头部开始访问链表 B
            l1 = (l1 == null) ? headB : l1.next;
            // 当访问 B 链表的指针访问到链表尾部时，令它从链表 A 的头部开始访问链表 A
            l2 = (l2 == null) ? headA : l2.next;
            // 这样就能控制访问 A 和 B 两个链表的指针能同时访问到交点。
        }
        return l1;
    }

    public static void main(String[] args) {

    }
}


class ListNode {
    public int val;
    public ListNode next;
}
