package com.jeasy.leetcode.linkedlist;

import java.util.Stack;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/29 18:10
 */
public class AddTwoNumbers {

    /**
     * 两个链表求和
     *
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> l1Stack = buildStack(l1);
        Stack<Integer> l2Stack = buildStack(l2);

        int carry = 0;
        ListNode head = new ListNode();
        while (!l1Stack.isEmpty() || !l2Stack.isEmpty() || carry != 0) {
            int x = l1Stack.isEmpty() ? 0 : l1Stack.pop();
            int y = l2Stack.isEmpty() ? 0 : l2Stack.pop();
            int sum = x + y + carry;
            ListNode node = new ListNode();
            node.val = sum % 10;

            node.next = head.next;
            head.next = node;
            carry = sum / 10;
        }
        return head.next;
    }

    private static Stack<Integer> buildStack(ListNode head) {
        Stack<Integer> stack = new Stack<>();
        while (head != null) {
            stack.push(head.val);
            head = head.next;
        }
        return stack;
    }

    public static void main(String[] args) {

    }
}
