package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/29 17:55
 */
public class SwapPairs {

    /**
     * 交换链表中的相邻结点
     *
     * @param head
     */
    public static ListNode swapPairs(ListNode head) {
        ListNode node = new ListNode();
        node.next = head;
        ListNode pre = node;

        while (pre.next != null && pre.next.next != null) {
            ListNode l1 = pre.next;
            ListNode l2 = pre.next.next;
            ListNode next = l2.next;

            l1.next = next;
            l2.next = l1;
            pre.next = l2;

            pre = l1;
        }
        return node.next;
    }

    public static void main(String[] args) {

    }
}
