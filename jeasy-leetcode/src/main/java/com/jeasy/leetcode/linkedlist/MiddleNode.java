package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/30 18:15
 */
public class MiddleNode {

    /**
     * 获取链表的中间节点
     *
     * @param head
     * @return
     */
    public static ListNode getMiddle(ListNode head) {
        if (head == null) {
            return null;
        }

        // 初始化两个指针 slow, fast。其均指向链表的头节点处
        ListNode slow = head;
        ListNode fast = head;

        while (fast != null && fast.next != null) {
            // slow 指针每次走一步，称为慢指针
            slow = slow.next;
            // fast 指针每次走两步，称为快指针
            fast = fast.next.next;
        }

        // 当 fast 指针直线末尾的时候，slow 指针所指向的节点便是链表的中间节点
        return slow;
    }

    public static void main(String[] args) {

    }
}
