package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/29 15:26
 */
public class MergeTwoSortedLinkedLists {

    /**
     * 归并两个有序的链表
     *
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode mergeTwoSortedLinkedLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }

        if (l2 == null) {
            return l1;
        }

        if (l1.val < l2.val) {
            l1.next = mergeTwoSortedLinkedLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoSortedLinkedLists(l1, l2.next);
            return l2;
        }
    }

    public static void main(String[] args) {

    }
}
