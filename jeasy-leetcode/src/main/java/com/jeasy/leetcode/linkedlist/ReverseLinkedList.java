package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/29 12:03
 */
public class ReverseLinkedList {

    /**
     * 链表翻转（递归）
     *
     * @param head
     * @return
     */
    public static ListNode reverseLinkedList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode next = head.next;
        ListNode newHead = reverseLinkedList(next);
        next.next = head;
        head.next = null;
        return newHead;
    }

    /**
     * 链表翻转（头插法）
     *
     * @param head
     * @return
     */
    public static ListNode reverseLinkedList2(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode newHead = new ListNode();
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead.next = head;
            head = next;
        }
        return newHead.next;
    }

    public static void main(String[] args) {

    }
}
