package com.jeasy.leetcode.linkedlist;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/29 19:31
 */
public class PalindromeLinkedList {

    /**
     * 判断是否为回文链表
     *
     * @param head
     * @return
     */
    public static boolean isPalindrome(ListNode head) {
        if (head == null || head.next == null) {
            return true;
        }
        // 快慢指针找出中间位置
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        if (fast != null) {
            // 偶数节点，让 slow 指向下一个节点
            slow = slow.next;
        }
        // 切成两个链表
        cut(head, slow);
        return isEqual(head, reverse(slow));
    }

    private static void cut(ListNode head, ListNode cut) {
        while (head.next != cut) {
            head = head.next;
        }
        head.next = null;
    }

    private static ListNode reverse(ListNode head) {
        ListNode newHead = null;
        while (head != null) {
            ListNode nextNode = head.next;
            head.next = newHead;
            newHead = head;
            head = nextNode;
        }
        return newHead;
    }

    private static boolean isEqual(ListNode l1, ListNode l2) {
        while (l1 != null && l2 != null) {
            if (l1.val != l2.val) {
                return false;
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
