package com.jeasy.leetcode.sort;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/26 07:03
 */
public class SortColors {

    /**
     * 按颜色顺序正确地排列
     *
     * @param nums
     */
    public static void sortColors(int[] nums) {
        int zero = -1;
        int one = 0;
        int two = nums.length;

        while (one < two) {
            if (nums[one] == 0) {
                // 等于红色，挪到前面位置
                swap(nums, ++zero, one);
            } else if (nums[one] == 2) {
                // 等于蓝色，挪到后面位置
                swap(nums, --two, one);
            } else {
                // 其他 ++
                one++;
            }
        }
    }

    private static void swap(int[] nums, int i, int j) {
        int t = nums[i];
        nums[i] = nums[j];
        nums[j] = t;
    }

    public static void main(String[] args) {

    }
}
