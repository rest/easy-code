package com.jeasy.leetcode.sort;

import java.util.PriorityQueue;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 15:18
 */
public class FindKthLargest {

    /**
     * 求解 Kth Element 问题
     * 找到倒数第 k 大的元素 => 时间复杂度O(nlogk)，空间复杂度O(1)
     * 使用快速排序的 partition() 进行实现。需要先打乱数组，否则最坏情况下时间复杂度为 O(N2)
     *
     * @param nums
     * @param k
     * @return
     */
    public static int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>(); // 小顶堆
        for (int val : nums) {
            pq.add(val);
            if (pq.size() > k) { // 维护堆的大小
                pq.poll();
            }
        }
        return pq.peek();
    }

    public static void main(String[] args) {
        System.out.println(findKthLargest(new int[] {1, 2, 1, 2, 5, 3}, 2));
    }
}
