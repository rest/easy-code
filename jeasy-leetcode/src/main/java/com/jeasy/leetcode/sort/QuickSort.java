package com.jeasy.leetcode.sort;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/26 00:15
 */
public class QuickSort {

    public static void quickSort(int[] nums, int begin, int end) {
        if (end <= begin) {
            return;
        }

        int pivot = partition(nums, begin, end);
        quickSort(nums, begin, pivot - 1);
        quickSort(nums, pivot + 1, end);
    }

    private static int partition(int[] nums, int begin, int end) {
        int counter = begin; // 小于 pivot 的元素的个数
        int pivot = end; // 标杆位置

        for (int i = begin; i < end; i++) {
            if (nums[i] < nums[pivot]) {
                int temp = nums[counter];
                nums[counter] = nums[i]; // 小的往左边放
                nums[i] = temp;
                counter++; // 统计有多少个小于 pivot 的元素的个数
            }
        }

        // 最终，再把 pivot 和 counter 互换位置
        int temp = nums[pivot];
        nums[pivot] = nums[counter];
        nums[counter] = temp;

        return counter;
    }

    public static void main(String[] args) {

    }
}
