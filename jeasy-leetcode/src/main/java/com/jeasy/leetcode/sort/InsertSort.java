package com.jeasy.leetcode.sort;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 20:02
 */
public class InsertSort {

    /**
     * 插入排序
     *
     * @param a
     */
    public static void insertSort(int[] a) {
        if (a == null || a.length == 0) {
            return;
        }

        for (int i = 1; i < a.length; i++) {
            int j = i - 1;
            // 先取出待插入数据保存，因为向后移位过程中会把覆盖掉待插入数
            int temp = a[i];
            while (j >= 0 && a[j] > temp) {
                // 如果待是比待插入数据大，就后移
                a[j + 1] = a[j];
                j--;
            }
            // 找到比待插入数据小的位置，将待插入数据插入
            a[j + 1] = temp;
        }
    }

    public static void main(String[] args) {

    }
}
