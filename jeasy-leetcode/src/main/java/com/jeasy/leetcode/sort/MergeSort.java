package com.jeasy.leetcode.sort;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/26 00:44
 */
public class MergeSort {

    public static void mergeSort(int[] nums, int left, int right) {
        if (right <= left) {
            return;
        }

        // 中间位置
        int mid = (left + right) >> 1;
        // 归并排序左边
        mergeSort(nums, left, mid - 1);
        // 归并排序右边
        mergeSort(nums, mid + 1, right);
        // 合并
        merge(nums, left, mid, right);
    }

    public static void merge(int[] nums, int left, int mid, int right) {
        int[] temp = new int[right - left + 1];
        int i = left;
        int j = mid + 1;
        int k = 0;

        while (i <= mid && j <= right) {
            // 较小值放在前面
            temp[k++] = nums[i] <= nums[j] ? nums[i++] : nums[j++];
        }

        while (i <= mid) {
            // 左边剩余
            temp[k++] = nums[i++];
        }

        while (j <= right) {
            // 右边剩余
            temp[k++] = nums[j++];
        }

        for (int p = 0; p < temp.length; p++) {
            // 替换 nums
            nums[left + p] = temp[p];
        }
    }

    public static void main(String[] args) {

    }
}
