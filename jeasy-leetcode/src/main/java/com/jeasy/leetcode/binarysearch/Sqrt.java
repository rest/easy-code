package com.jeasy.leetcode.binarysearch;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 17:11
 */
public class Sqrt {

    /**
     * 求开方
     *
     * @param x
     * @return
     */
    public static int sqrt(int x) {
        if (x <= 1) {
            return x;
        }

        int l = 1;
        int h = x;
        while (l <= h) {
            int m = l + (h - l) / 2;
            if (m * m == x) {
                return m;
            } else if (m * m > x) {
                h = m - 1;
            } else {
                l = m + 1;
            }
        }
        return h;
    }

    public static void main(String[] args) {

    }
}
