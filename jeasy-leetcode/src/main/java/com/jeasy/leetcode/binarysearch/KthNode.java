package com.jeasy.leetcode.binarysearch;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 15:19
 */
public class KthNode {

    private static int cnt = 0;
    private static int val;

    /**
     * 寻找二叉查找树的第 k 个元素
     * 二叉查找树（BST）：根节点大于等于左子树所有节点，小于等于右子树所有节点。
     * 二叉查找树中序遍历有序。
     *
     * @param root
     * @param k
     * @return
     */
    public static int kthSmallest(TreeNode root, int k) {
        inOrder(root, k);
        return val;
    }

    /**
     * 解法：中序遍历
     *
     * @param node
     * @param k
     */
    private static void inOrder(TreeNode node, int k) {
        if (node == null) {
            return;
        }
        inOrder(node.left, k);
        cnt++;
        if (cnt == k) {
            val = node.val;
            return;
        }
        inOrder(node.right, k);
    }

    /**
     * 寻找二叉查找树的第 k 个元素 (递归解法)
     * 二叉查找树（BST）：根节点大于等于左子树所有节点，小于等于右子树所有节点。
     * 二叉查找树中序遍历有序。
     *
     * @param root
     * @param k
     * @return
     */
    public static int kthSmallest2(TreeNode root, int k) {
        int leftCnt = count(root.left);
        if (leftCnt == k - 1) {
            return root.val;
        } else if (leftCnt > k - 1) {
            return kthSmallest2(root.left, k);
        } else {
            return kthSmallest2(root.right, k - leftCnt - 1);
        }
    }

    private static int count(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return 1 + count(node.left) + count(node.right);
    }

    public static void main(String[] args) {

    }
}


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
}
