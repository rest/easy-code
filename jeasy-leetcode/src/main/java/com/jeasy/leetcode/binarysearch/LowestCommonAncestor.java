package com.jeasy.leetcode.binarysearch;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 15:33
 */
public class LowestCommonAncestor {

    /**
     * 二叉查找树的最近公共祖先
     *
     * @param root
     * @param p
     * @param q
     * @return
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // 都大，一定在 left
        if (root.val > p.val && root.val > q.val) return lowestCommonAncestor(root.left, p, q);
        // 都小，一定在 right
        if (root.val < p.val && root.val < q.val) return lowestCommonAncestor(root.right, p, q);
        return root;
    }

    /**
     * 二叉树的最近公共祖先
     *
     * @param root
     * @param p
     * @param q
     * @return
     */
    public TreeNode lowestCommonAncestor2(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) return root;
        TreeNode left = lowestCommonAncestor2(root.left, p, q);
        TreeNode right = lowestCommonAncestor2(root.right, p, q);
        return left == null ? right : right == null ? left : root;
    }

    public static void main(String[] args) {

    }
}
