package com.jeasy.leetcode.binarysearch;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/26 07:38
 */
public class NextGreatestLetter {

    /**
     * 给定一个有序的字符数组 letters 和一个字符 target，
     * 要求找出 letters 中大于 target 的最小字符，如果找不到就返回第 1 个字符。
     *
     * @param letters
     * @param target
     * @return
     */
    public static char nextGreatestLetter(char[] letters, char target) {
        int n = letters.length;
        int l = 0;
        int h = n - 1;

        while (l <= h) {
            int m = l + (h - l) / 2;
            if (letters[l] <= target) {
                l = m + 1;
            } else {
                h = m - 1;
            }
        }

        return l < n ? letters[l] : letters[0];
    }

    public static void main(String[] args) {

    }
}
