package com.jeasy.leetcode.dynamic;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 18:02
 */
public class CoinChange {

    /**
     * 找零钱的最少硬币数
     *
     * @param coins
     * @param amount
     * @return
     */
    public static int coinChange(int[] coins, int amount) {
        if (coins == null || amount == 0) {
            return 0;
        }

        int[] dp = new int[amount + 1];
        for (int coin : coins) {
            // 因为硬币可以重复使用，因此这是一个完全背包问题。
            // 完全背包只需要将 0-1 背包的逆序遍历 dp 数组改为正序遍历即可。
            for (int i = coin; i <= amount; i++) {
                if (i == coin) {
                    dp[i] = 1;
                } else if (dp[i] == 0 && dp[i - coin] != 0) {
                    dp[i] = dp[i - coin] + 1;
                } else if (dp[i - coin] != 0) {
                    dp[i] = Math.min(dp[i], dp[i - coin] + 1);
                }
            }
        }

        return dp[amount] == 0 ? -1 : dp[amount];
    }

    /**
     * 找零钱的硬币数组合
     *
     * @param coins
     * @param amount
     * @return
     */
    public static int change(int[] coins, int amount) {
        if (coins == null || amount == 0) {
            return 0;
        }

        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int coin : coins) {
            for (int i = coin; i <= amount; i++) {
                dp[i] += dp[i - coin];
            }
        }
        return dp[amount];
    }

    public static void main(String[] args) {

    }
}
