package com.jeasy.leetcode.dynamic;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 17:43
 */
public class ClimbStairs {

    /**
     * 有 N 阶楼梯，每次可以上一阶或者两阶，求有多少种上楼梯的方法
     *
     * @param n
     * @return
     */
    public static int climbStairs(int n) {
        if (n <= 2) {
            return n;
        }

        int pre2 = 1;
        int pre1 = 2;
        for (int i = 2; i < n; i++) {
            // dp 公式：第 i 个楼梯可以从第 i-1 和 i-2 个楼梯再走一步到达，
            // 走到第 i 个楼梯的方法数为走到第 i-1 和第 i-2 个楼梯的方法数之和
            int cur = pre2 + pre1;
            pre2 = pre1;
            pre1 = cur;
        }

        return pre1;
    }

    public static void main(String[] args) {

    }
}
