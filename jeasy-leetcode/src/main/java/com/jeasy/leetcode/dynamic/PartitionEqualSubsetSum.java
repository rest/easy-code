package com.jeasy.leetcode.dynamic;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/27 22:32
 */
public class PartitionEqualSubsetSum {

    /**
     * 划分数组为和相等的两部分
     *
     * @param nums
     * @return
     */
    public static boolean canPartition(int[] nums) {
        int sum = computArraySum(nums);
        if (sum % 2 != 0) {
            return false;
        }

        int w = sum / 2;
        boolean[] dp = new boolean[w + 1];
        dp[0] = true;
        for (int num : nums) {
            // 从 0 - 1 背包中的物品只能用一次
            for (int i = w; i >= num; i--) {
                // 从后往前，先计算 dp[i] 再计算 dp[i-num]
                dp[i] = dp[i] || dp[i - num];
            }
        }
        return dp[w];
    }

    private static int computArraySum(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return sum;
    }

    public static void main(String[] args) {

    }
}
