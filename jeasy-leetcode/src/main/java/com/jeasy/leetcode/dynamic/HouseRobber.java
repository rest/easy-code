package com.jeasy.leetcode.dynamic;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 17:48
 */
public class HouseRobber {

    /**
     * 抢劫一排住户，但是不能抢邻近的住户，求最大抢劫量
     *
     * @param nums
     * @return
     */
    public static int rob(int[] nums) {
        int pre2 = 0;
        int pre1 = 0;

        for (int i = 0; i < nums.length; i++) {
            // dp 公式：由于不能抢劫邻近住户，如果抢劫了第 i -1 个住户，
            // 那么就不能再抢劫第 i 个住户
            int cur = Math.max(pre2 + nums[i], pre1);
            pre2 = pre1;
            pre1 = cur;
        }

        return pre1;
    }

    public static void main(String[] args) {

    }
}
