package com.jeasy.leetcode.dynamic;

import java.util.Arrays;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 11:00
 */
public class CombinationSum {

    /**
     * 组合总和等于某个数
     *
     * @param nums
     * @param target
     * @return
     */
    public static int combinationSum(int[] nums, int target) {
        if (nums == null || nums.length == 0 || target == 0) {
            return 0;
        }

        int[] maxinum = new int[target + 1];
        maxinum[0] = 1;
        Arrays.sort(nums);
        for (int i = 0; i <= target; i++) {
            for (int j = 0; j < nums.length && nums[i] <= i; j++) {
                maxinum[i] += maxinum[i - nums[j]];
            }
        }
        return maxinum[target];
    }

    public static void main(String[] args) {
    }
}
