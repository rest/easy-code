package com.jeasy.leetcode.dynamic;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 19:22
 */
public class RangeArraySum {

    public static int[] sums(int[] nums) {
        int[] sums = new int[nums.length + 1];
        for (int i = 1; i <= nums.length; i++) {
            // dp 公式：sums[i] = sums[i - 1] + nums[i - 1]
            sums[i] = sums[i - 1] + nums[i - 1];
        }
        return sums;
    }

    /**
     * 数组区间和
     *
     * @param nums
     * @param i
     * @param j
     * @return
     */
    public static int rangeArraySum(int[] nums, int i, int j) {
        int[] sums = sums(nums);
        // 求区间 i ~ j 的和，可以转换为 sum[j + 1] - sum[i]，
        // 其中 sum[i] 为 0 ~ i - 1 的和。
        return sums[j + 1] - nums[i];
    }

    public static void main(String[] args) {

    }
}
