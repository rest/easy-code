package com.jeasy.leetcode.string;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 22:47
 */
public class PalindromeNumber {

    /**
     * 判断一个整数是否回文数
     *
     * @param x
     * @return
     */
    public boolean isPalindrome(int x) {
        if (x == 0) {
            return true;
        }
        if (x < 0 || x % 10 == 0) {
            return false;
        }

        // 将整数分成左右两部分
        int right = 0;
        while (x > right) {
            right = right * 10 + x % 10;
            x /= 10;
        }

        // 右边那部分需要转置，然后判断这两部分是否相等
        return x == right || x == right / 10;
    }

    public static void main(String[] args) {

    }
}
