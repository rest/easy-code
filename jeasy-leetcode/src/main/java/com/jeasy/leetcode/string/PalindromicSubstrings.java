package com.jeasy.leetcode.string;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 22:40
 */
public class PalindromicSubstrings {

    private static int cnt = 0;

    /**
     * 回文子字符串个数
     *
     * @param s
     * @return
     */
    public static int countSubstrings(String s) {
        // 从字符串的某一位开始，尝试着去扩展子字符串。
        for (int i = 0; i < s.length(); i++) {
            // 奇数长度
            extendSubstrings(s, i, i);
            // 偶数长度
            extendSubstrings(s, i, i + 1);
        }
        return cnt;
    }

    private static void extendSubstrings(String s, int start, int end) {
        while (start >= 0 && end < s.length() && s.charAt(start) == s.charAt(end)) {
            start--;
            end++;
            cnt++;
        }
    }

    public static void main(String[] args) {

    }
}
