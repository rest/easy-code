package com.jeasy.leetcode.string;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/02 08:01
 */
public class ReverseWords {

    /**
     * 翻转字符串的单词
     * "the sky is blue" => "blue is sky the"
     *
     * @param s
     * @return
     */
    public static String reverseWords(String s) {
        int left = 0, right = s.length() - 1;
        // 去掉字符串开头的空白字符
        while (left <= right && s.charAt(left) == ' ') {
            ++left;
        }

        // 去掉字符串末尾的空白字符
        while (left <= right && s.charAt(right) == ' ') {
            --right;
        }

        Deque<String> d = new ArrayDeque<>();
        StringBuilder word = new StringBuilder();

        while (left <= right) {
            char c = s.charAt(left);
            if ((word.length() != 0) && (c == ' ')) {
                // 将单词 push 到队列的头部
                d.offerFirst(word.toString());
                word.setLength(0);
            } else if (c != ' ') {
                word.append(c);
            }
            ++left;
        }
        d.offerFirst(word.toString());

        return String.join(" ", d);
    }

    /**
     * 用三反转等效左/右移字符串
     * 循环左移 n 位 => 首先将原字符串进行反转，然后把整个字符串分成 原字符串的长度-n 和后 n 个字符并分别进行反转，
     * 最后合并两个字符串即可。
     * 循环右移 n 位 => 首先把原字符串分成 原字符串的长度-n 和后 n 个字符并分别进行反转，
     * 然后合并两个字符串再次进行反转即可。
     *
     * @param from  需要移动的字符串
     * @param index 需要移动的位数
     */
    public static void print(String from, int index) {
        from = reChange(from);    //循环左移 （先整体，再部分）
        System.out.println(from);
        String first = reChange(from.substring(0, from.length() - index));
        System.out.println(first);
        String second = reChange(from.substring(from.length() - index));
        System.out.println(second);
        from = first + second;
        // from = reChange(from); //循环右移（先部分在整体）
        System.out.println(from);
    }

    /**
     * 反转字符串，把 from 字符串反转过来（很常见牢记）
     * 字符串求长度用 length(), 数组求长度用 length
     *
     * @param from
     * @return
     */
    public static String reChange(String from) {
        char[] froms = from.toCharArray();
        for (int i = 0; i < from.length() / 2; i++) {
            char temp = froms[i];
            froms[i] = froms[from.length() - 1 - i];
            froms[froms.length - 1 - i] = temp;
        }
        return String.valueOf(froms);
    }

    /**
     * 字符串循环移位后是否可包含
     * <p>
     * 解法二 => 拼接字符串 s1+s1，如果 s2 可以由 s1 循环移位得到，那么 s2 一定在 s1s1 上
     */
    public static boolean isContainsByConcat(String s1, String s2) {
        if (s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty()) return false;

        String newStr = s1.concat(s1);
        return newStr.contains(s2);
    }

    /**
     * 字符串循环移位后是否可包含
     * 解法三 => 指针循环查找
     * 用一个指针标记遍历 s1，最多两遍，
     * 与 s2 中的第一个字符比对，如果命中则进入 s2，进行逐位匹配，匹配失败的话跳出 s2 继续查找 s1。
     */
    public static boolean isContainsByPointer(String s1, String s2) {
        if (s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty()) return false;

        // s1 的指针
        int p = 0;

        int s1Len = s1.length();
        int s2Len = s2.length();
        for (int i = 0; i < s1Len * 2; i++) {
            char c1 = s1.charAt(p % s1Len);

            // 匹配到第一个字符相同后，遍历s2字符串
            if (c1 == s2.charAt(0)) {
                for (int j = 0; j < s2Len; j++) {
                    if (s1.charAt((p + j) % s1Len) != s2.charAt(j)) {
                        // 本次不完全匹配，继续往后
                        break;
                    }
                }
                return true;
            }
            p++;
        }
        return false;
    }

    public static void main(String[] args) {

    }
}
