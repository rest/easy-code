package com.jeasy.leetcode.string;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 22:17
 */
public class ValidAnagram {

    /**
     * 两个字符串包含的字符是否完全相同（字符相同 & 出现次数也相同）
     *
     * @param s
     * @param t
     * @return
     */
    public static boolean isAnagram(String s, String t) {
        // 字符串只包含 26 个小写字符
        int[] cnts = new int[26];

        // 可以使用长度为 26 的整型数组对字符串出现的字符进行统计
        for (char c : s.toCharArray()) {
            cnts[c - 'a']++;
        }

        for (char c : t.toCharArray()) {
            cnts[c - 'a']--;
        }

        // 只要不为 0 ，就不相同
        for (int cnt : cnts) {
            if (cnt != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
