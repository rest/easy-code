package com.jeasy.leetcode.string;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 22:23
 */
public class LongestPalindrome {

    /**
     * 计算一组字符集合可以组成的回文字符串的最大长度
     *
     * @param s
     * @return
     */
    public static int longestPalindrome(String s) {
        // 使用长度为 256 的整型数组来统计每个字符出现的个数，每个字符有偶数个可以用来构成回文字符串
        int[] cnts = new int[256];
        for (char c : s.toCharArray()) {
            cnts[c]++;
        }

        int palindrome = 0;
        for (int cnt : cnts) {
            palindrome += (cnt / 2) * 2;
        }

        if (palindrome < s.length()) {
            // 这个条件下 s 中一定有单个未使用的字符存在，可以把这个字符放到回文的最中间
            palindrome++;
        }
        return palindrome;
    }

    public static void main(String[] args) {

    }
}
