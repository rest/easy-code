package com.jeasy.leetcode.skiplist;

import java.util.Random;

public class SkipList {

    Node head = new Node(null, null, 0);

    /**
     * 查找
     *
     * @param target
     * @return
     */
    public boolean search(int target) {
        // 遍历每一层
        for (Node p = head; p != null; p = p.down) {
            // 向右遍历
            while (p.right != null) {
                // 值相等
                if (p.right.val == target) {
                    return true;
                }
                p = p.right;
            }
        }
        return false;
    }

    Random rand = new Random();
    // 2^64 已经相当大了
    Node[] stack = new Node[64];

    /**
     * 添加
     *
     * @param num
     */
    public void add(int num) {
        int lv = -1;
        for (Node p = head; p != null; p = p.down) {
            while (p.right != null && p.right.val < num) {
                // 此时 p 为当前层最后一个节点
                p = p.right;
            }
            stack[++lv] = p;
        }
        // 从最底层不断进行插入新节点
        boolean insertUp = true;
        // 新插入节点时，用于建立 down path，用于构建蓝色节点的 down 指针
        Node downNode = null;
        while (insertUp && lv >= 0) {
            // 最底层
            Node insert = stack[lv--];
            insert.right = new Node(insert.right, downNode, num);
            downNode = insert.right;
            // 概率性提升
            insertUp = (rand.nextInt() & 1) == 0;
        }

        if (insertUp) {
            head = new Node(new Node(null, downNode, num), head, 0);
        }
    }

    /**
     * 删除
     *
     * @param num
     * @return
     */
    public boolean erase(int num) {
        boolean exists = false;
        for (Node p = head; p != null; p = p.down) {
            while (p.right != null && p.right.val < num) {
                p = p.right;
            }
            if (p.right != null && p.right.val == num) {
                exists = true;
                p.right = p.right.right;
            }
        }
        return exists;
    }

    static class Node {
        Node right;
        Node down;
        int val;

        public Node(Node right, Node down, int val) {
            this.right = right;
            this.down = down;
            this.val = val;
        }
    }
}
