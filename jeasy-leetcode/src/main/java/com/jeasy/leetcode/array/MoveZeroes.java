package com.jeasy.leetcode.array;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/02 09:10
 */
public class MoveZeroes {

    /**
     * 把数组中的 0 移到末尾
     *
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int idx = 0;
        for (int num : nums) {
            if (num != 0) {
                nums[idx++] = num;
            }
        }
        while (idx < nums.length) {
            nums[idx++] = 0;
        }
    }

    public static void main(String[] args) {

    }
}
