package com.jeasy.leetcode.array;

import static java.lang.System.out;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/10 16:22
 */
public class GenerateParenthesis {

    /**
     * 生成括号
     *
     * @param left
     * @param right
     * @param n
     * @param s
     */
    public static void generateParenthesis(int left, int right, int n, String s) {
        if (left == n && right == n) {
            out.println(s);
            return;
        }

        if (left < n) {
            generateParenthesis(left + 1, right, n, s + "(");
        }
        if (left > right) {
            generateParenthesis(left, right + 1, n, s + ")");
        }
    }

    public static void main(String[] args) {
        generateParenthesis(0, 0, 3, "");
    }
}
