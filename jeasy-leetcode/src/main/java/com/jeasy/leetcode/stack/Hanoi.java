package com.jeasy.leetcode.stack;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 13:11
 */
public class Hanoi {

    // 用于记录移动次数
    private static int c = 0;

    /**
     * 将塔座 x 上按照直径由小到大且自上而下的编号为 1 至 n 的 n 个圆盘按照规则移到塔座 z 上，y 用作辅助塔座
     * @param n 圆盘的数目
     * @param x 原塔座
     * @param y 辅助塔座
     * @param z 目标塔座
     */
    public static void hanoi(int n, char x, char y, char z) {
        if (n == 1) {
            // 将编号为 1 的圆盘从 x 移动到 z
            move(x, n, z);
        } else {
            // 将 x 上编号为 1 到 n-1 的圆盘移动到 y，z 做辅助塔座
            hanoi(n - 1, x, z, y);
            // 将编号为 n 的圆盘从 x 移动到 z
            move(x, n, z);
            // 将 y 上编号为 1 至 n-1 的圆盘移动到 z，x 做辅助塔座
            hanoi(n-1, y, x, z);
        }

    }

    /**
     * 用于移动操作
     * @param x 原塔盘
     * @param n 圆盘编号
     * @param z 目标塔盘
     */
    public static void move(char x, int n, char z) {
        System.out.println("第"+(++c)+"次移动:"+n+"号圆盘,"+x+"->"+z);
    }

    public static void main(String[] args) {
        hanoi(5, 'x', 'y', 'z');
    }
}
