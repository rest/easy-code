package com.jeasy.leetcode.search;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 16:37
 */
public class BinaryTreePath {

    /**
     * 输出二叉树中所有从根到叶子的路径
     *
     * @param root
     * @return
     */
    public static List<String> binaryTreePaths(TreeNode root) {
        List<String> paths = new ArrayList<>();
        if (root == null) {
            return paths;
        }

        List<Integer> values = new ArrayList<>();
        backtracking(root, values, paths);
        return paths;
    }

    private static void backtracking(TreeNode node, List<Integer> values, List<String> paths) {
        if (node == null) {
            return;
        }
        values.add(node.val);
        if (isLeaf(node)) {
            paths.add(buildPaths(values));
        } else {
            backtracking(node.left, values, paths);
            backtracking(node.right, values, paths);
        }
        values.remove(values.size() - 1);
    }

    private static boolean isLeaf(TreeNode node) {
        return node.left == null && node.right == null;
    }

    private static String buildPaths(List<Integer> values) {
        StringBuilder sbr = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            sbr.append(values.get(i));
            if (i != values.size() - 1) {
                sbr.append("->");
            }
        }
        return sbr.toString();
    }

    public static void main(String[] args) {

    }

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }
}
