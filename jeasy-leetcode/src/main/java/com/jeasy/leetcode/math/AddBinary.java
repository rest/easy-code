package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 19:55
 */
public class AddBinary {

    /**
     * 二进制加法
     *
     * @param a
     * @param b
     * @return
     */
    public static String addBinary(String a, String b) {
        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        StringBuilder sbr = new StringBuilder();
        while (carry == 1 || i >= 0 || j >= 0) {
            if (i >= 0 && a.charAt(i--) == '1') {
                carry++;
            }

            if (j >= 0 && b.charAt(j--) == '1') {
                carry++;
            }
            sbr.append(carry % 2);
            carry /= 2;
        }
        return sbr.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(addBinary("11", "1"));
    }
}
