package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/30 15:16
 */
public class NumReverse {

    /**
     * 实现将整形数字反转
     *
     * @param num
     * @return
     */
    public static int numReverse(int num) {
        int mod = 0; // 余数
        int result = 0;

        while (num / 10 != 0) { // 当 num 为一位数时，跳出循环
            mod = num % 10;
            num = num / 10;
            result = result * 10 + mod;
        }

        return result * 10 + num; // 当num为一位数时，返回结果
    }

    public static void main(String[] args) {

    }
}
