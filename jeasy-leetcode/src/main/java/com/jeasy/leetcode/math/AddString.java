package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 20:03
 */
public class AddString {

    /**
     * 十进制字符串加法
     *
     * @param a
     * @param b
     * @return
     */
    public static String addString(String a, String b) {
        int carry = 0;
        int i = a.length() - 1;
        int j = b.length() - 1;
        StringBuilder sbr = new StringBuilder();
        while (carry == 1 || i >= 0 || j >= 0) {
            int x = i < 0 ? 0 : a.charAt(i--) - '0';
            int y = j < 0 ? 0 : b.charAt(j--) - '0';
            sbr.append((x + y + carry) % 10);
            carry = (x + y + carry) / 10;
        }
        return sbr.reverse().toString();
    }

    public static void main(String[] args) {

    }
}
