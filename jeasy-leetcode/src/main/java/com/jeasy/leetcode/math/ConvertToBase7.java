package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 18:09
 */
public class ConvertToBase7 {

    /**
     * 转换为 7 进制
     *
     * @param num
     * @return
     */
    public static String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }

        StringBuilder sbr = new StringBuilder();
        boolean isNegative = num < 0;
        if (isNegative) {
            num = -num;
        }

        while (num > 0) {
            sbr.append(num % 7);
            num /= 7;
        }

        return isNegative ? "-" + sbr.reverse().toString() : sbr.reverse().toString();
    }

    /**
     * 转换为 16 进制
     *
     * @param num
     * @return
     */
    public static String toHex(int num) {
        char[] map = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        if (num == 0) {
            return "0";
        }
        StringBuilder sbr = new StringBuilder();
        while (num != 0) {
            sbr.append(map[num & 0b1111]);
            // 因为考虑的是补码形式，因此符号位就不能有特殊的意义，需要使用无符号右移，左边填 0
            num >>>= 4;
        }
        return sbr.reverse().toString();
    }

    /**
     * 转换为 26 进制
     * 1 -> A
     * 2 -> B
     * 3 -> C
     * ...
     * 26 -> Z
     * 27 -> AA
     * 28 -> AB
     *
     * @param num
     * @return
     */
    public static String convertToBase26(int num) {
        if (num == 0) {
            return "";
        }

        num--;
        return convertToBase26(num / 26) + (char) (num % 26 + 'A');
    }

    public static void main(String[] args) {

    }
}
