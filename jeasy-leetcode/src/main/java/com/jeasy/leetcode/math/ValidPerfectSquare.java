package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 20:11
 */
public class ValidPerfectSquare {

    /**
     * 校验是否为平方数
     *
     * @param num
     * @return
     */
    public boolean isPerfectSquare(int num) {
        // 平方序列：1,4,9,16,..
        // 间隔：3,5,7,...，间隔为等差数列，使用这个特性可以得到从 1 开始的平方序列。
        int subNum = 1;
        while (num > 0) {
            num -= subNum;
            subNum += 2;
        }
        return num == 0;
    }

    public static void main(String[] args) {

    }
}
