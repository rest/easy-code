package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 12:35
 */
public class BigNumAdd {

    /**
     * 大数相加
     *
     * @param s1
     * @param s2
     * @return
     */
    public static String add(String s1, String s2) {
        // 反转
        StringBuilder a = new StringBuilder(s1);
        StringBuilder b = new StringBuilder(s2);
        a.reverse();
        b.reverse();

        int m = a.length();
        int n = b.length();
        int max = Math.max(m, n);
        // 补齐 0
        if (m < n) {
            for (int i = m; i < n; i++) {
                a.append('0');
            }
        } else {
            for (int i = n; i < m; i++) {
                b.append('0');
            }
        }

        // 相加
        int[] ans = new int[max + 1];
        for (int i = 0; i < max; i++) {
            ans[i] = (a.charAt(i) - '0') + (b.charAt(i) - '0');
        }

        // 求余、取整
        for (int i = 0; i < max; i++) {
            ans[i + 1] += ans[i] / 10;
            ans[i] %= 10;
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < max; i++) {
            result.append(ans[i]);
        }

        if (ans[max] != 0) {
            result.append(ans[max]);
        }

        return result.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(add("99999999999999999999999999999999", "1"));
    }
}
