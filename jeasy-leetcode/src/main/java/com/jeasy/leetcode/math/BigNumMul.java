package com.jeasy.leetcode.math;

import java.util.jar.JarEntry;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 12:51
 */
public class BigNumMul {

    public static String mul(String s1, String s2) {
        // 反转
        StringBuilder a = new StringBuilder(s1);
        StringBuilder b = new StringBuilder(s2);
        a.reverse();
        b.reverse();

        int m = a.length();
        int n = b.length();

        // 相乘
        int[] ans = new int[m + n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                ans[i+j] += (a.charAt(i) - '0') * (b.charAt(j) - '0');
            }
        }

        // 求余、取整
        for (int i = 0; i < ans.length - 1; i++) {
            ans[i + 1] += ans[i] / 10;
            ans[i] %= 10;
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < ans.length - 1; i++) {
            result.append(ans[i]);
        }

        return result.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(mul("9999999999999999999999", "2"));
    }
}
