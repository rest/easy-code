package com.jeasy.leetcode.math;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/28 17:14
 */
public class CountPrime {

    /**
     * 生成素数序列，统计素数的数量
     *
     * @param n
     * @return
     */
    public static int countPrimes(int n) {
        int count = 0;
        boolean[] notPrimes = new boolean[n + 1];

        for (int i = 2; i < n; i++) {
            if (notPrimes[i]) {
                continue;
            }
            count++;
            for (long j = (long) (i) * i; j < n; j += i) {
                notPrimes[(int) j] = true;
            }
        }

        return count;
    }

    /**
     * 最大公约数
     *
     * @param a
     * @param b
     * @return
     */
    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    /**
     * 最小公倍数为两数的乘积除以最大公约数
     *
     * @param a
     * @param b
     * @return
     */
    public static int lcm(int a, int b) {
        return (a * b) / gcd(a, b);
    }

    public static void main(String[] args) {
        System.out.println(countPrimes(20));
        System.out.println(gcd(8, 10));
        System.out.println(lcm(8, 10));
    }
}
