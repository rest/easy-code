package com.jeasy.leetcode.binarytree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/30 15:47
 */
public class DiameterOfBinaryTree {

    private static int max;

    /**
     * 二叉树的直径
     * 一条路径的长度为该路径经过的节点数减一，
     * 所以求直径（即求路径长度的最大值）等效于求路径经过节点数的最大值减一
     *
     * @param root
     * @return
     */
    public static int diameterOfBinaryTree(TreeNode root) {
        return depth(root) - 1;
    }

    public static int depth(TreeNode node) {
        if (node == null) {
            // 访问到空节点了，返回0
            return 0;
        }

        // 左儿子为根的子树的深度
        int l = depth(node.left);
        // 右儿子为根的子树的深度
        int r = depth(node.right);
        // 计算 node ans 即 L+R+1 并更新 max
        max = Math.max(l + r + 1, max);
        return max;
    }

    public static void main(String[] args) {

    }
}


class TreeNode {
    TreeNode left;
    TreeNode right;
}
