package com.jeasy.leetcode.binarytree;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/30 16:51
 */
public class MaxDepth {

    /**
     * N 叉树的最大深度（递归）
     *
     * @param root
     * @return
     */
    public static int maxDepth(Node root) {
        if (root == null) {
            return 0;
        } else if (root.children.isEmpty()) {
            return 1;
        } else {
            List<Integer> heights = new LinkedList<>();
            for (Node item : root.children) {
                heights.add(maxDepth(item));
            }
            // 当前层级 子节点深度最大值
            return Collections.max(heights) + 1;
        }
    }

    /**
     * N 叉树的最大深度（非递归）
     *
     * @param root
     * @return
     */
    public static int maxDepthByStack(Node root) {
        Queue<Pair<Node, Integer>> stack = new LinkedList<>();
        if (root != null) {
            stack.add(Pair.of(root, 1));
        }

        int depth = 0;
        while (!stack.isEmpty()) {
            Pair<Node, Integer> current = stack.poll();
            root = current.getKey();
            int currentDepth = current.getValue();
            if (root != null) {
                depth = Math.max(depth, currentDepth);
                for (Node c : root.children) {
                    stack.add(Pair.of(c, currentDepth + 1));
                }
            }
        }
        return depth;
    }

    public static void main(String[] args) {

    }
}


// 节点定义
class Node {
    public int val;
    public List<Node> children;

    public Node() {
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
}
