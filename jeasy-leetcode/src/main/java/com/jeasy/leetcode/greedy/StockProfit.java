package com.jeasy.leetcode.greedy;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 16:10
 */
public class StockProfit {

    /**
     * 一次股票交易包含买入和卖出，只进行一次交易，求最大收益
     *
     * @param prices
     * @return
     */
    public static int maxProfit(int[] prices) {
        int n = prices.length;
        if (n == 0) {
            return 0;
        }
        int soFarMin = prices[0];
        int maxProfit = 0;
        for (int price : prices) {
            if (soFarMin > price) {
                // 只要记录前面的最小价格，将这个最小价格作为买入价格
                soFarMin = price;
            } else {
                // 将当前的价格作为售出价格，查看当前收益是不是最大收益
                maxProfit = Math.max(maxProfit, price - soFarMin);
            }
        }
        return maxProfit;
    }

    /**
     * 多次交易，多次交易之间不能交叉进行，可以进行多次交易，求最大收益
     *
     * @param prices
     * @return
     */
    public static int maxProfitPlus(int[] prices) {
        int n = prices.length;
        if (n == 0) {
            return 0;
        }

        int totalProfit = 0;
        for (int i = 1; i < n; i++) {
            // 对于 [a, b, c, d]，如果有 a <= b <= c <= d ，那么最大收益为 d - a。而 d - a = (d - c) + (c - b) + (b - a) ，
            // 因此当访问到一个 prices[i] 且 prices[i] - prices[i-1] > 0，那么就把 prices[i] - prices[i-1] 添加到收益中。
            if (prices[i] > prices[i - 1]) {
                totalProfit += prices[i] - prices[i - 1];
            }
        }
        return totalProfit;
    }


    public static void main(String[] args) {

    }
}
