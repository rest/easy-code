package com.jeasy.leetcode.greedy;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 16:24
 */
public class Subsequence {

    /**
     * 判断是否为子序列
     *
     * @param s s = "abc",
     * @param t t = "ahbgdc" => return true
     * @return
     */
    public static boolean isSubsequence(String s, String t) {
        int index = -1;
        for (char c : s.toCharArray()) {
            index = t.indexOf(c, index + 1);
            if (index == -1) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
