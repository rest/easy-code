package com.jeasy.leetcode.greedy;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 16:34
 */
public class MaximumSubarray {

    /**
     * 子数组最大的和
     *
     * @param nums
     * @return
     */
    public static int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        int preSum = nums[0];
        int maxSum = preSum;
        for (int i = 0; i < nums.length; i++) {
            preSum = preSum > 0 ? preSum + nums[i] : nums[i];
            maxSum = Math.max(maxSum, preSum);
        }

        return maxSum;
    }

    public static void main(String[] args) {

    }
}
