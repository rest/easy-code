package com.jeasy.leetcode.greedy;

import java.util.Arrays;

/**
 * TODO
 *
 * @author taomingkai
 * @version 1.0.0
 * @since 2020/12/25 15:49
 */
public class AssignCookies {

    /**
     * 分配饼干
     *
     * @param grid
     * @param size
     * @return
     */
    public static int findContentChildren(int[] grid, int[] size) {
        if (grid == null || size == null) {
            return 0;
        }

        Arrays.sort(grid);
        Arrays.sort(size);

        int gi = 0;
        int si = 0;
        while (gi < grid.length && si < size.length) {
            if (grid[gi] <= size[si]) {
                // 给一个孩子的饼干应当尽量小并且又能满足该孩子，这样大饼干才能拿来给满足度比较大的孩子
                gi++;
            }
            si++;
        }
        return gi;
    }

    public static void main(String[] args) {

    }
}
