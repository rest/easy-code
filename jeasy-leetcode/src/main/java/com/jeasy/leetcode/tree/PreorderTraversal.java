package com.jeasy.leetcode.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 14:49
 */
public class PreorderTraversal {

    /**
     * 非递归实现二叉树的前序遍历
     *
     * @param root
     * @return
     */
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ret = new ArrayList<>();
        if (root == null) {
            return ret;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            if (node == null) {
                continue;
            }
            ret.add(node.val);
            // 先右后左，保证左子树先遍历
            stack.push(root.right);
            stack.push(root.left);
            // root -> left -> right
        }
        return ret;
    }

    public static void main(String[] args) {

    }
}
