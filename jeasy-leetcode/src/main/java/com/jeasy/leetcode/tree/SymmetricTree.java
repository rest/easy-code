package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 22:54
 */
public class SymmetricTree {

    /**
     * 判断树是否对称
     *
     * @param root
     * @return
     */
    public static boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private static boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }

        if (left == null || right == null) {
            return false;
        }

        if (left.val != right.val) {
            return false;
        }

        return isSymmetric(left.left, right.right) && isSymmetric(left.right, right.left);
    }


    public static void main(String[] args) {

    }
}
