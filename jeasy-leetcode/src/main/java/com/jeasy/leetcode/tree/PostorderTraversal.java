package com.jeasy.leetcode.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 14:57
 */
public class PostorderTraversal {

    /**
     * 非递归实现二叉树的后序遍历
     *
     * @param root
     * @return
     */
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> ret = new ArrayList<>();
        if (root == null) {
            return ret;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            if (node == null) {
                continue;
            }
            ret.add(node.val);
            stack.push(node.left);
            stack.push(node.right);
            // root -> right -> left
        }
        // left -> right -> root
        Collections.reverse(ret);
        return ret;
    }

    public static void main(String[] args) {

    }
}
