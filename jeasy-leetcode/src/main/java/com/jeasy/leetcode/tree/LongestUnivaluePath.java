package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 23:15
 */
public class LongestUnivaluePath {

    private static int path = 0;

    /**
     * 相同节点值的最大路径长度
     *
     * @param root
     * @return
     */
    public static int longestUnivaluePath(TreeNode root) {
        dfs(root);
        return path;
    }

    private static int dfs(TreeNode root) {
        if (root == null) {
            return 0;
        }

        int left = dfs(root.left);
        int right = dfs(root.right);

        int leftPath = root.left != null && root.left.val == root.val ? left + 1 : 0;
        int rightPath = root.right != null && root.right.val == root.val ? right + 1 : 0;

        path = Math.max(path, leftPath + rightPath);
        return Math.max(leftPath, rightPath);
    }

    public static void main(String[] args) {

    }
}
