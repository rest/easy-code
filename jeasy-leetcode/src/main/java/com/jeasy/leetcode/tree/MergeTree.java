package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 20:39
 */
public class MergeTree {

    /**
     * 归并两棵树
     *
     * @param t1
     * @param t2
     * @return
     */
    public static TreeNode mergeTree(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return null;
        }
        if (t1 == null) {
            return t2;
        }
        if (t2 == null) {
            return t1;
        }

        TreeNode root = new TreeNode();
        root.val = t1.val + t2.val;
        root.left = mergeTree(t1.left, t2.left);
        root.right = mergeTree(t1.right, t2.right);
        return root;
    }

    public static void main(String[] args) {

    }
}
