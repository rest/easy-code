package com.jeasy.leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/01/01 09:56
 */
public class BottomLeftValue {

    /**
     * 获取左下角的叶子节点
     *
     * @param root
     * @return
     */
    public static int findBottomLeftValue(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            root = queue.poll();
            if (root.right != null) {
                queue.add(root.right);
            }

            if (root.left != null) {
                queue.add(root.left);
            }
        }
        return root.val;
    }

    public static void main(String[] args) {

    }
}
