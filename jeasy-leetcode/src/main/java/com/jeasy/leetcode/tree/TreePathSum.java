package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 22:05
 */
public class TreePathSum {

    /**
     * 判断路径和是否等于一个数
     *
     * @param root
     * @param sum
     * @return
     */
    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }

        if (root.left == null && root.right == null && root.val == sum) {
            return true;
        }

        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }

    /**
     * 统计路径和等于一个数的路径数量
     * 路径不一定以 root 开头，也不一定以 leaf 结尾，但是必须连续。
     *
     * @param node
     * @param sum
     * @return
     */
    public static int pathSum(TreeNode node, int sum) {
        if (node == null) {
            return 0;
        }
        return pathSum(node.left, sum) + pathSum(node.right, sum) + pathSumStartWithRoot(node, sum);
    }

    public static int pathSumStartWithRoot(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }

        int ret = 0;
        if (root.val == sum) {
            ret++;
        }

        ret += pathSumStartWithRoot(root.left, sum - root.val) + pathSumStartWithRoot(root.right, sum - root.val);
        return ret;
    }

    public static void main(String[] args) {

    }
}
