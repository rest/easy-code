package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 22:39
 */
public class SubTree {

    /**
     * 判断子树
     *
     * @param node
     * @param sub
     * @return
     */
    public static boolean isSubTree(TreeNode node, TreeNode sub) {
        if (node == null) {
            return false;
        }

        return isSubTree(node.left, sub) || isSubTree(node.right, sub) || isSubtreeWithRoot(node, sub);
    }

    private static boolean isSubtreeWithRoot(TreeNode root, TreeNode sub) {
        if (root == null && sub == null) {
            return true;
        }

        if (root == null || sub == null) {
            return false;
        }

        if (root.val != sub.val) {
            return false;
        }

        return isSubtreeWithRoot(root.left, sub.left) && isSubtreeWithRoot(root.right, sub.right);
    }

    public static void main(String[] args) {

    }
}
