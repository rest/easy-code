package com.jeasy.leetcode.tree;

/**
 * TODO
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/12/31 23:27
 */
public class HouseRobber {

    /**
     * 间隔遍历
     * 抢劫住户，但是不能抢临近的住户，求最大抢劫量
     *
     * @param root
     * @return
     */
    public static int rob(TreeNode root) {
        if (root == null) {
            return 0;
        }

        int val1 = root.val;
        if (root.left != null) {
            val1 += rob(root.left.left) + rob(root.left.right);
        }

        if (root.right != null) {
            val1 += rob(root.right.left) + rob(root.right.right);
        }

        int val2 = rob(root.left) + rob(root.right);
        return Math.max(val1, val2);
    }

    public static void main(String[] args) {

    }
}
