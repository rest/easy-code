package com.jeasy.web.organization.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 机构 详情 ResVO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationShowResVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 主键
     */
    @InitField(value = "", desc = "主键")
    private String id;

    /**
     * 名称
     */
    @InitField(value = "", desc = "名称")
    private String name;

    /**
     * 编码
     */
    @InitField(value = "", desc = "编码")
    private String code;

    /**
     * 排序
     */
    @InitField(value = "", desc = "排序")
    private Integer sort;

    /**
     * 是否叶子节点:0=否,1=是
     */
    @InitField(value = "", desc = "是否叶子节点:0=否,1=是")
    private Integer isLeaf;

    /**
     * 父ID
     */
    @InitField(value = "", desc = "父ID")
    private String pid;

    /**
     * 父名称
     */
    @InitField(value = "", desc = "父名称")
    private String pname;

    /**
     * 备注/描述
     */
    @InitField(value = "", desc = "备注/描述")
    private String remark;
}
