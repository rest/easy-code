package com.jeasy.web.organization.controller;

import com.jeasy.organization.dto.OrganizationAddReqDTO;
import com.jeasy.organization.dto.OrganizationListReqDTO;
import com.jeasy.organization.dto.OrganizationListResDTO;
import com.jeasy.organization.dto.OrganizationModifyReqDTO;
import com.jeasy.organization.dto.OrganizationPageReqDTO;
import com.jeasy.organization.dto.OrganizationPageResDTO;
import com.jeasy.organization.dto.OrganizationShowResDTO;
import com.jeasy.organization.service.OrganizationService;
import com.jeasy.web.organization.mapper.OrganizationVOMapper;
import com.jeasy.web.organization.vo.OrganizationAddReqVO;
import com.jeasy.web.organization.vo.OrganizationListReqVO;
import com.jeasy.web.organization.vo.OrganizationListResVO;
import com.jeasy.web.organization.vo.OrganizationModifyReqVO;
import com.jeasy.web.organization.vo.OrganizationPageReqVO;
import com.jeasy.web.organization.vo.OrganizationPageResVO;
import com.jeasy.web.organization.vo.OrganizationShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 机构 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class OrganizationController extends BaseController<OrganizationService, OrganizationVOMapper> {

    @RequiresPermissions(value = {"YHGL:ZZJG:CX"})
    @MethodDoc(items = OrganizationListResVO.class, desc = {"PC端", "机构-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/list", method = {RequestMethod.GET})
    public void list(final @FromJson OrganizationListReqVO organizationListReqVO) {
        OrganizationListReqDTO organizationListReqDTO = mapper.convertToOrganizationListReqDTO(organizationListReqVO);

        List<OrganizationListResDTO> organizationListResDTOList = service.list(organizationListReqDTO);
        List<OrganizationListResVO> items = mapper.convertToOrganizationListResVOList(organizationListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(pages = OrganizationPageResVO.class, desc = {"PC端", "机构-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/page", method = {RequestMethod.GET})
    public void page(final @FromJson OrganizationPageReqVO organizationPageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        OrganizationPageReqDTO organizationPageReqDTO = mapper.convertToOrganizationPageReqDTO(organizationPageReqVO);

        Page<OrganizationPageResDTO> organizationPageResDTOPage = service.pagination(organizationPageReqDTO, current, size);
        Page<OrganizationPageResVO> organizationPageResVOPage = mapper.convertToOrganizationPageResVOPage(organizationPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, organizationPageResVOPage.getTotal(), organizationPageResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"YHGL:ZZJG:XZ"})
    @MethodDoc(desc = {"PC端", "机构-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/add", method = {RequestMethod.POST})
    public void add(final @FromJson OrganizationAddReqVO organizationAddReqVO) {
        OrganizationAddReqDTO organizationAddReqDTO = mapper.convertToOrganizationAddReqDTO(organizationAddReqVO);

        Boolean isSuccess = service.add(organizationAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:ZZJG:CK"})
    @MethodDoc(item = OrganizationShowResVO.class, desc = {"PC端", "机构-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        OrganizationShowResDTO organizationShowResDTO = service.show(id);

        OrganizationShowResVO item = mapper.convertToOrganizationShowResVO(organizationShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"YHGL:ZZJG:BJ"})
    @MethodDoc(desc = {"PC端", "机构-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson OrganizationModifyReqVO organizationModifyReqVO) {
        OrganizationModifyReqDTO organizationModifyReqDTO = mapper.convertToOrganizationModifyReqDTO(organizationModifyReqVO);

        Boolean isSuccess = service.modify(organizationModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:ZZJG:SC"})
    @MethodDoc(desc = {"PC端", "机构-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "organization/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
