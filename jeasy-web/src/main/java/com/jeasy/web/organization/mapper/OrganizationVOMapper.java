package com.jeasy.web.organization.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.organization.dto.OrganizationAddReqDTO;
import com.jeasy.organization.dto.OrganizationDTO;
import com.jeasy.organization.dto.OrganizationListReqDTO;
import com.jeasy.organization.dto.OrganizationListResDTO;
import com.jeasy.organization.dto.OrganizationModifyReqDTO;
import com.jeasy.organization.dto.OrganizationPageReqDTO;
import com.jeasy.organization.dto.OrganizationPageResDTO;
import com.jeasy.organization.dto.OrganizationRemoveReqDTO;
import com.jeasy.organization.dto.OrganizationShowResDTO;
import com.jeasy.web.organization.vo.OrganizationAddReqVO;
import com.jeasy.web.organization.vo.OrganizationListReqVO;
import com.jeasy.web.organization.vo.OrganizationListResVO;
import com.jeasy.web.organization.vo.OrganizationModifyReqVO;
import com.jeasy.web.organization.vo.OrganizationPageReqVO;
import com.jeasy.web.organization.vo.OrganizationPageResVO;
import com.jeasy.web.organization.vo.OrganizationRemoveReqVO;
import com.jeasy.web.organization.vo.OrganizationShowResVO;
import com.jeasy.web.organization.vo.OrganizationVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface OrganizationVOMapper extends BaseVOMapper<OrganizationDTO, OrganizationVO> {

    default OrganizationVOMapper me() {
        return SpringUtil.getBean(OrganizationVOMapper.class);
    }

    OrganizationAddReqDTO convertToOrganizationAddReqDTO(OrganizationAddReqVO organizationAddReqVO);

    OrganizationShowResVO convertToOrganizationShowResVO(OrganizationShowResDTO organizationShowResDTO);

    OrganizationModifyReqDTO convertToOrganizationModifyReqDTO(OrganizationModifyReqVO organizationModifyReqVO);

    OrganizationRemoveReqDTO convertToOrganizationRemoveReqDTO(OrganizationRemoveReqVO organizationRemoveReqVO);

    OrganizationListReqDTO convertToOrganizationListReqDTO(OrganizationListReqVO organizationReqVO);

    OrganizationPageReqDTO convertToOrganizationPageReqDTO(OrganizationPageReqVO organizationPageReqVO);

    OrganizationListResVO convertToOrganizationListResVO(OrganizationListResDTO organizationListResDTO);

    List<OrganizationListResVO> convertToOrganizationListResVOList(List<OrganizationListResDTO> organizationListResDTOList);

    Page<OrganizationPageResVO> convertToOrganizationPageResVOPage(Page<OrganizationPageResDTO> organizationPageResDTOPage);

    List<OrganizationAddReqDTO> convertToOrganizationAddReqDTOList(List<OrganizationAddReqVO> organizationAddReqVOList);

    List<OrganizationShowResVO> convertToOrganizationShowResVOList(List<OrganizationShowResDTO> organizationShowResDTOList);
}
