package com.jeasy.web.dictionary.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 字典 VO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/03/28 15:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DictionaryTypeListResVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 字典类型名称
     */
    @InitField(value = "XXXX", desc = "字典类型名称")
    private String name;

    /**
     * 字典类型编号
     */
    @InitField(value = "YHZT", desc = "字典类型编号")
    private String code;
}
