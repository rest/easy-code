package com.jeasy.web.dictionary.controller;

import com.jeasy.dictionary.dto.DictionaryAddReqDTO;
import com.jeasy.dictionary.dto.DictionaryListReqDTO;
import com.jeasy.dictionary.dto.DictionaryListResDTO;
import com.jeasy.dictionary.dto.DictionaryModifyReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageResDTO;
import com.jeasy.dictionary.dto.DictionaryShowResDTO;
import com.jeasy.dictionary.dto.DictionaryTypeListResDTO;
import com.jeasy.dictionary.service.DictionaryService;
import com.jeasy.web.dictionary.mapper.DictionaryVOMapper;
import com.jeasy.web.dictionary.vo.DictionaryAddReqVO;
import com.jeasy.web.dictionary.vo.DictionaryListReqVO;
import com.jeasy.web.dictionary.vo.DictionaryListResVO;
import com.jeasy.web.dictionary.vo.DictionaryModifyReqVO;
import com.jeasy.web.dictionary.vo.DictionaryPageReqVO;
import com.jeasy.web.dictionary.vo.DictionaryPageResVO;
import com.jeasy.web.dictionary.vo.DictionaryShowResVO;
import com.jeasy.web.dictionary.vo.DictionaryTypeListResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 字典 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class DictionaryController extends BaseController<DictionaryService, DictionaryVOMapper> {

    @MethodDoc(items = DictionaryListResVO.class, desc = {"5.基础数据", "1.公共码表", "1.字典列表查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/list", method = {RequestMethod.GET})
    public void list(final @FromJson DictionaryListReqVO dictionaryReqVO) {
        DictionaryListReqDTO dictionaryListReqDTO = mapper.convertToDictionaryListReqDTO(dictionaryReqVO);

        List<DictionaryListResDTO> dictionaryListResDTOList = service.list(dictionaryListReqDTO);
        List<DictionaryListResVO> items = mapper.convertToDictionaryListResVOList(dictionaryListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = DictionaryTypeListResVO.class, desc = {"5.基础数据", "1.公共码表", "2.字典类型查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/listType", method = {RequestMethod.GET})
    public void listType() {
        List<DictionaryTypeListResDTO> dictionaryTypeListResDTOList = service.listType();

        List<DictionaryTypeListResVO> items = mapper.convertToDictionaryTypeListResVOList(dictionaryTypeListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"JCSJ:GGMB:CX"})
    @MethodDoc(pages = DictionaryPageResVO.class, desc = {"5.基础数据", "1.公共码表", "3.字典分页列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/page", method = {RequestMethod.GET})
    public void page(final @FromJson DictionaryPageReqVO dictionaryPageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;

        DictionaryPageReqDTO dictionaryPageReqDTO = mapper.convertToDictionaryPageReqDTO(dictionaryPageReqVO);
        Page<DictionaryPageResDTO> dictionaryPageResDTOPage = service.pagination(dictionaryPageReqDTO, current, size);
        Page<DictionaryPageResVO> dictionaryPageResVOPage = mapper.convertToDictionaryPageResVOPage(dictionaryPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, dictionaryPageResVOPage.getTotal(), dictionaryPageResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"JCSJ:GGMB:XZ"})
    @MethodDoc(desc = {"5.基础数据", "1.公共码表", "4.字典新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/add", method = {RequestMethod.POST})
    public void add(final @FromJson DictionaryAddReqVO dictionaryAddReqVO) {
        DictionaryAddReqDTO dictionaryAddReqDTO = mapper.convertToDictionaryAddReqDTO(dictionaryAddReqVO);

        Boolean result = service.add(dictionaryAddReqDTO);

        responseMessage(result ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"JCSJ:GGMB:CK"})
    @MethodDoc(item = DictionaryShowResVO.class, desc = {"5.基础数据", "1.公共码表", "5.字典详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "用户ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        DictionaryShowResDTO dictionaryShowResDTO = service.show(id);
        DictionaryShowResVO dictionaryShowResVO = mapper.convertToDictionaryShowResVO(dictionaryShowResDTO);

        responseItem(Func.isNotEmpty(dictionaryShowResVO) ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR, dictionaryShowResVO);
    }

    @RequiresPermissions(value = {"JCSJ:GGMB:BJ"})
    @MethodDoc(desc = {"5.基础数据", "1.公共码表", "6.字典更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson DictionaryModifyReqVO dictionaryModifyReqVO) {
        DictionaryModifyReqDTO dictionaryModifyReqDTO = mapper.convertToDictionaryModifyReqDTO(dictionaryModifyReqVO);
        Boolean result = service.modify(dictionaryModifyReqDTO);

        responseMessage(result ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"JCSJ:GGMB:SC"})
    @MethodDoc(desc = {"5.基础数据", "1.公共码表", "7.字典删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "dictionary/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "字典ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean result = service.remove(id);

        responseMessage(result ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
