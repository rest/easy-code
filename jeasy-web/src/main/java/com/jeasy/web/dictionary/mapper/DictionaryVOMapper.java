package com.jeasy.web.dictionary.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.dictionary.dto.DictionaryAddReqDTO;
import com.jeasy.dictionary.dto.DictionaryDTO;
import com.jeasy.dictionary.dto.DictionaryListReqDTO;
import com.jeasy.dictionary.dto.DictionaryListResDTO;
import com.jeasy.dictionary.dto.DictionaryModifyReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageResDTO;
import com.jeasy.dictionary.dto.DictionaryRemoveReqDTO;
import com.jeasy.dictionary.dto.DictionaryShowResDTO;
import com.jeasy.dictionary.dto.DictionaryTypeListResDTO;
import com.jeasy.web.dictionary.vo.DictionaryAddReqVO;
import com.jeasy.web.dictionary.vo.DictionaryListReqVO;
import com.jeasy.web.dictionary.vo.DictionaryListResVO;
import com.jeasy.web.dictionary.vo.DictionaryModifyReqVO;
import com.jeasy.web.dictionary.vo.DictionaryPageReqVO;
import com.jeasy.web.dictionary.vo.DictionaryPageResVO;
import com.jeasy.web.dictionary.vo.DictionaryRemoveReqVO;
import com.jeasy.web.dictionary.vo.DictionaryShowResVO;
import com.jeasy.web.dictionary.vo.DictionaryTypeListResVO;
import com.jeasy.web.dictionary.vo.DictionaryVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface DictionaryVOMapper extends BaseVOMapper<DictionaryDTO, DictionaryVO> {

    default DictionaryVOMapper me() {
        return SpringUtil.getBean(DictionaryVOMapper.class);
    }

    DictionaryAddReqDTO convertToDictionaryAddReqDTO(DictionaryAddReqVO dictionaryAddReqVO);

    DictionaryShowResVO convertToDictionaryShowResVO(DictionaryShowResDTO dictionaryShowResDTO);

    DictionaryModifyReqDTO convertToDictionaryModifyReqDTO(DictionaryModifyReqVO dictionaryModifyReqVO);

    DictionaryRemoveReqDTO convertToDictionaryRemoveReqDTO(DictionaryRemoveReqVO dictionaryRemoveReqVO);

    DictionaryListReqDTO convertToDictionaryListReqDTO(DictionaryListReqVO dictionaryReqVO);

    DictionaryPageReqDTO convertToDictionaryPageReqDTO(DictionaryPageReqVO dictionaryPageReqVO);

    DictionaryListResVO convertToDictionaryListResVO(DictionaryListResDTO dictionaryListResDTO);

    List<DictionaryListResVO> convertToDictionaryListResVOList(List<DictionaryListResDTO> dictionaryListResDTOList);

    List<DictionaryTypeListResVO> convertToDictionaryTypeListResVOList(List<DictionaryTypeListResDTO> dictionaryTypeListResDTOList);

    Page<DictionaryPageResVO> convertToDictionaryPageResVOPage(Page<DictionaryPageResDTO> dictionaryPageResDTOPage);

    List<DictionaryAddReqDTO> convertToDictionaryAddReqDTOList(List<DictionaryAddReqVO> dictionaryAddReqVOList);

    List<DictionaryShowResVO> convertToDictionaryShowResVOList(List<DictionaryShowResDTO> dictionaryShowResDTOList);
}
