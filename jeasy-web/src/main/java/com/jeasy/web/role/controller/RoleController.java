package com.jeasy.web.role.controller;

import com.jeasy.role.dto.RoleAddReqDTO;
import com.jeasy.role.dto.RoleListPermissionReqDTO;
import com.jeasy.role.dto.RoleListPermissionResDTO;
import com.jeasy.role.dto.RoleListReqDTO;
import com.jeasy.role.dto.RoleListResDTO;
import com.jeasy.role.dto.RoleModifyPermissionReqDTO;
import com.jeasy.role.dto.RoleModifyReqDTO;
import com.jeasy.role.dto.RolePageReqDTO;
import com.jeasy.role.dto.RolePageResDTO;
import com.jeasy.role.dto.RoleRemoveReqDTO;
import com.jeasy.role.dto.RoleShowResDTO;
import com.jeasy.role.service.RoleService;
import com.jeasy.web.role.mapper.RoleVOMapper;
import com.jeasy.web.role.vo.RoleAddReqVO;
import com.jeasy.web.role.vo.RoleListPermissionReqVO;
import com.jeasy.web.role.vo.RoleListPermissionResVO;
import com.jeasy.web.role.vo.RoleListReqVO;
import com.jeasy.web.role.vo.RoleListResVO;
import com.jeasy.web.role.vo.RoleModifyPermissionReqVO;
import com.jeasy.web.role.vo.RoleModifyReqVO;
import com.jeasy.web.role.vo.RolePageReqVO;
import com.jeasy.web.role.vo.RolePageResVO;
import com.jeasy.web.role.vo.RoleRemoveReqVO;
import com.jeasy.web.role.vo.RoleShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class RoleController extends BaseController<RoleService, RoleVOMapper> {

    @MethodDoc(items = RoleListResVO.class, desc = {"PC端", "角色-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/list", method = {RequestMethod.GET})
    public void list(final @FromJson RoleListReqVO roleListReqVO) {
        RoleListReqDTO roleListReqDTO = mapper.convertToRoleListReqDTO(roleListReqVO);

        List<RoleListResDTO> roleListResDTOList = service.list(roleListReqDTO);
        List<RoleListResVO> items = mapper.convertToRoleListResVOList(roleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:QXPZ"})
    @MethodDoc(items = RoleListPermissionResVO.class, desc = {"PC端", "角色-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/listPermission", method = {RequestMethod.GET})
    public void listPermission(final @FromJson RoleListPermissionReqVO roleListPermissionReqVO) {
        RoleListPermissionReqDTO roleListPermissionReqDTO = mapper.convertToRoleListPermissionReqDTO(roleListPermissionReqVO);

        List<RoleListPermissionResDTO> roleListPermissionResDTOList = service.listPermission(roleListPermissionReqDTO);
        List<RoleListPermissionResVO> items = mapper.convertToRoleListPermissionResVOList(roleListPermissionResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleListResVO.class, desc = {"PC端", "角色-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson RoleListReqVO roleListReqVO) {
        RoleListReqDTO roleListReqDTO = mapper.convertToRoleListReqDTO(roleListReqVO);

        List<RoleListResDTO> roleListResDTOList = service.listByVersion1(roleListReqDTO);
        List<RoleListResVO> items = mapper.convertToRoleListResVOList(roleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleListResVO.class, desc = {"PC端", "角色-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson RoleListReqVO roleListReqVO) {
        RoleListReqDTO roleListReqDTO = mapper.convertToRoleListReqDTO(roleListReqVO);

        List<RoleListResDTO> roleListResDTOList = service.listByVersion2(roleListReqDTO);
        List<RoleListResVO> items = mapper.convertToRoleListResVOList(roleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleListResVO.class, desc = {"PC端", "角色-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson RoleListReqVO roleListReqVO) {
        RoleListReqDTO roleListReqDTO = mapper.convertToRoleListReqDTO(roleListReqVO);

        List<RoleListResDTO> roleListResDTOList = service.listByVersion3(roleListReqDTO);
        List<RoleListResVO> items = mapper.convertToRoleListResVOList(roleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = RoleListResVO.class, desc = {"PC端", "角色-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson RoleListReqVO roleListReqVO) {
        RoleListReqDTO roleListReqDTO = mapper.convertToRoleListReqDTO(roleListReqVO);

        RoleListResDTO roleListResDTO = service.listOne(roleListReqDTO);
        RoleListResVO item = mapper.convertToRoleListResVO(roleListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:CX"})
    @MethodDoc(pages = RolePageResVO.class, desc = {"PC端", "角色-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/page", method = {RequestMethod.GET})
    public void page(final @FromJson RolePageReqVO rolePageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        RolePageReqDTO rolePageReqDTO = mapper.convertToRolePageReqDTO(rolePageReqVO);

        Page<RolePageResDTO> rolePageResDTOPage = service.pagination(rolePageReqDTO, current, size);
        Page<RolePageResVO> rolePageResVOPage = mapper.convertToRolePageResVOPage(rolePageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, rolePageResVOPage.getTotal(), rolePageResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:XZ"})
    @MethodDoc(desc = {"PC端", "角色-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/add", method = {RequestMethod.POST})
    public void add(final @FromJson RoleAddReqVO roleAddReqVO) {
        RoleAddReqDTO roleAddReqDTO = mapper.convertToRoleAddReqDTO(roleAddReqVO);

        Boolean isSuccess = service.add(roleAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson RoleAddReqVO roleAddReqVO) {
        RoleAddReqDTO roleAddReqDTO = mapper.convertToRoleAddReqDTO(roleAddReqVO);

        Boolean isSuccess = service.addAllColumn(roleAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<RoleAddReqVO> roleAddReqVOList) {
        List<RoleAddReqDTO> roleAddReqDTOList = mapper.convertToRoleAddReqDTOList(roleAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(roleAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:CK"})
    @MethodDoc(item = RoleShowResVO.class, desc = {"PC端", "角色-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        RoleShowResDTO roleShowResDTO = service.show(id);

        RoleShowResVO item = mapper.convertToRoleShowResVO(roleShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = RoleShowResVO.class, desc = {"PC端", "角色-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<RoleShowResDTO> roleShowResDTOList = service.showByIds(ids);

        List<RoleShowResVO> items = mapper.convertToRoleShowResVOList(roleShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:BJ"})
    @MethodDoc(desc = {"PC端", "角色-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson RoleModifyReqVO roleModifyReqVO) {
        RoleModifyReqDTO roleModifyReqDTO = mapper.convertToRoleModifyReqDTO(roleModifyReqVO);

        Boolean isSuccess = service.modify(roleModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:QXPZ"})
    @MethodDoc(desc = {"PC端", "角色-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/modifyPermission", method = {RequestMethod.POST})
    public void modifyPermission(final @FromJson RoleModifyPermissionReqVO roleModifyPermissionReqVO) {
        RoleModifyPermissionReqDTO roleModifyPermissionReqDTO = mapper.convertToRoleModifyPermissionReqDTO(roleModifyPermissionReqVO);

        Boolean isSuccess = service.modifyPermission(roleModifyPermissionReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson RoleModifyReqVO roleModifyReqVO) {
        RoleModifyReqDTO roleModifyReqDTO = mapper.convertToRoleModifyReqDTO(roleModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(roleModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:JSGL:SC"})
    @MethodDoc(desc = {"PC端", "角色-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "role/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson RoleRemoveReqVO roleRemoveReqVO) {
        RoleRemoveReqDTO roleRemoveReqDTO = mapper.convertToRoleRemoveReqDTO(roleRemoveReqVO);

        Boolean isSuccess = service.removeByParams(roleRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
