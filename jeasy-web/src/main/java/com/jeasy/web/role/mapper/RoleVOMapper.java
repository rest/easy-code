package com.jeasy.web.role.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.role.dto.RoleAddReqDTO;
import com.jeasy.role.dto.RoleDTO;
import com.jeasy.role.dto.RoleListPermissionReqDTO;
import com.jeasy.role.dto.RoleListPermissionResDTO;
import com.jeasy.role.dto.RoleListReqDTO;
import com.jeasy.role.dto.RoleListResDTO;
import com.jeasy.role.dto.RoleModifyPermissionReqDTO;
import com.jeasy.role.dto.RoleModifyReqDTO;
import com.jeasy.role.dto.RolePageReqDTO;
import com.jeasy.role.dto.RolePageResDTO;
import com.jeasy.role.dto.RoleRemoveReqDTO;
import com.jeasy.role.dto.RoleShowResDTO;
import com.jeasy.web.role.vo.RoleAddReqVO;
import com.jeasy.web.role.vo.RoleListPermissionReqVO;
import com.jeasy.web.role.vo.RoleListPermissionResVO;
import com.jeasy.web.role.vo.RoleListReqVO;
import com.jeasy.web.role.vo.RoleListResVO;
import com.jeasy.web.role.vo.RoleModifyPermissionReqVO;
import com.jeasy.web.role.vo.RoleModifyReqVO;
import com.jeasy.web.role.vo.RolePageReqVO;
import com.jeasy.web.role.vo.RolePageResVO;
import com.jeasy.web.role.vo.RoleRemoveReqVO;
import com.jeasy.web.role.vo.RoleShowResVO;
import com.jeasy.web.role.vo.RoleVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface RoleVOMapper extends BaseVOMapper<RoleDTO, RoleVO> {

    default RoleVOMapper me() {
        return SpringUtil.getBean(RoleVOMapper.class);
    }

    RoleAddReqDTO convertToRoleAddReqDTO(RoleAddReqVO roleAddReqVO);

    RoleShowResVO convertToRoleShowResVO(RoleShowResDTO roleShowResDTO);

    RoleModifyReqDTO convertToRoleModifyReqDTO(RoleModifyReqVO roleModifyReqVO);

    RoleRemoveReqDTO convertToRoleRemoveReqDTO(RoleRemoveReqVO roleRemoveReqVO);

    RoleListReqDTO convertToRoleListReqDTO(RoleListReqVO roleReqVO);

    RolePageReqDTO convertToRolePageReqDTO(RolePageReqVO rolePageReqVO);

    RoleListResVO convertToRoleListResVO(RoleListResDTO roleListResDTO);

    List<RoleListResVO> convertToRoleListResVOList(List<RoleListResDTO> roleListResDTOList);

    Page<RolePageResVO> convertToRolePageResVOPage(Page<RolePageResDTO> rolePageResDTOPage);

    List<RoleAddReqDTO> convertToRoleAddReqDTOList(List<RoleAddReqVO> roleAddReqVOList);

    List<RoleShowResVO> convertToRoleShowResVOList(List<RoleShowResDTO> roleShowResDTOList);

    RoleListPermissionReqDTO convertToRoleListPermissionReqDTO(RoleListPermissionReqVO roleListPermissionReqVO);

    List<RoleListPermissionResVO> convertToRoleListPermissionResVOList(List<RoleListPermissionResDTO> roleListPermissionResDTOList);

    RoleModifyPermissionReqDTO convertToRoleModifyPermissionReqDTO(RoleModifyPermissionReqVO roleModifyPermissionReqVO);
}
