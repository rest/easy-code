package com.jeasy.web.role.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import com.javabooter.core.validate.handler.ValidateNotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 用户 修改 ReqVO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleModifyPermissionReqVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 角色ID
     */
    @InitField(value = "", desc = "角色ID")
    @ValidateNotNull(message = "角色ID不允许为空")
    private String roleId;

    /**
     * 资源ID集合
     */
    @InitField(value = "", desc = "资源ID集合")
    private List<Long> permissionIds;
}
