package com.jeasy.web.userorg.controller;

import com.jeasy.userorg.dto.UserOrgAddReqDTO;
import com.jeasy.userorg.dto.UserOrgListReqDTO;
import com.jeasy.userorg.dto.UserOrgListResDTO;
import com.jeasy.userorg.dto.UserOrgModifyReqDTO;
import com.jeasy.userorg.dto.UserOrgPageReqDTO;
import com.jeasy.userorg.dto.UserOrgPageResDTO;
import com.jeasy.userorg.dto.UserOrgRemoveReqDTO;
import com.jeasy.userorg.dto.UserOrgShowResDTO;
import com.jeasy.userorg.service.UserOrgService;
import com.jeasy.web.userorg.mapper.UserOrgVOMapper;
import com.jeasy.web.userorg.vo.UserOrgAddReqVO;
import com.jeasy.web.userorg.vo.UserOrgListReqVO;
import com.jeasy.web.userorg.vo.UserOrgListResVO;
import com.jeasy.web.userorg.vo.UserOrgModifyReqVO;
import com.jeasy.web.userorg.vo.UserOrgPageReqVO;
import com.jeasy.web.userorg.vo.UserOrgPageResVO;
import com.jeasy.web.userorg.vo.UserOrgRemoveReqVO;
import com.jeasy.web.userorg.vo.UserOrgShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户机构 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class UserOrgController extends BaseController<UserOrgService, UserOrgVOMapper> {

    @MethodDoc(items = UserOrgListResVO.class, desc = {"PC端", "用户机构-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/list", method = {RequestMethod.GET})
    public void list(final @FromJson UserOrgListReqVO userOrgListReqVO) {
        UserOrgListReqDTO userOrgListReqDTO = mapper.convertToUserOrgListReqDTO(userOrgListReqVO);

        List<UserOrgListResDTO> userOrgListResDTOList = service.list(userOrgListReqDTO);
        List<UserOrgListResVO> items = mapper.convertToUserOrgListResVOList(userOrgListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserOrgListResVO.class, desc = {"PC端", "用户机构-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson UserOrgListReqVO userOrgListReqVO) {
        UserOrgListReqDTO userOrgListReqDTO = mapper.convertToUserOrgListReqDTO(userOrgListReqVO);

        List<UserOrgListResDTO> userOrgListResDTOList = service.listByVersion1(userOrgListReqDTO);
        List<UserOrgListResVO> items = mapper.convertToUserOrgListResVOList(userOrgListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserOrgListResVO.class, desc = {"PC端", "用户机构-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson UserOrgListReqVO userOrgListReqVO) {
        UserOrgListReqDTO userOrgListReqDTO = mapper.convertToUserOrgListReqDTO(userOrgListReqVO);

        List<UserOrgListResDTO> userOrgListResDTOList = service.listByVersion2(userOrgListReqDTO);
        List<UserOrgListResVO> items = mapper.convertToUserOrgListResVOList(userOrgListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserOrgListResVO.class, desc = {"PC端", "用户机构-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson UserOrgListReqVO userOrgListReqVO) {
        UserOrgListReqDTO userOrgListReqDTO = mapper.convertToUserOrgListReqDTO(userOrgListReqVO);

        List<UserOrgListResDTO> userOrgListResDTOList = service.listByVersion3(userOrgListReqDTO);
        List<UserOrgListResVO> items = mapper.convertToUserOrgListResVOList(userOrgListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = UserOrgListResVO.class, desc = {"PC端", "用户机构-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson UserOrgListReqVO userOrgListReqVO) {
        UserOrgListReqDTO userOrgListReqDTO = mapper.convertToUserOrgListReqDTO(userOrgListReqVO);

        UserOrgListResDTO userOrgListResDTO = service.listOne(userOrgListReqDTO);
        UserOrgListResVO item = mapper.convertToUserOrgListResVO(userOrgListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(pages = UserOrgPageResVO.class, desc = {"PC端", "用户机构-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/page", method = {RequestMethod.GET})
    public void page(final @FromJson UserOrgPageReqVO userOrgPageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        UserOrgPageReqDTO userOrgPageReqDTO = mapper.convertToUserOrgPageReqDTO(userOrgPageReqVO);

        Page<UserOrgPageResDTO> userOrgPageResDTOPage = service.pagination(userOrgPageReqDTO, current, size);
        Page<UserOrgPageResVO> userOrgPageResVOPage = mapper.convertToUserOrgPageResVOPage(userOrgPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, userOrgPageResVOPage.getTotal(), userOrgPageResVOPage.getRecords(), size, current);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/add", method = {RequestMethod.POST})
    public void add(final @FromJson UserOrgAddReqVO userOrgAddReqVO) {
        UserOrgAddReqDTO userOrgAddReqDTO = mapper.convertToUserOrgAddReqDTO(userOrgAddReqVO);

        Boolean isSuccess = service.add(userOrgAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson UserOrgAddReqVO userOrgAddReqVO) {
        UserOrgAddReqDTO userOrgAddReqDTO = mapper.convertToUserOrgAddReqDTO(userOrgAddReqVO);

        Boolean isSuccess = service.addAllColumn(userOrgAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<UserOrgAddReqVO> userOrgAddReqVOList) {
        List<UserOrgAddReqDTO> userOrgAddReqDTOList = mapper.convertToUserOrgAddReqDTOList(userOrgAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(userOrgAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(item = UserOrgShowResVO.class, desc = {"PC端", "用户机构-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        UserOrgShowResDTO userOrgShowResDTO = service.show(id);

        UserOrgShowResVO item = mapper.convertToUserOrgShowResVO(userOrgShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = UserOrgShowResVO.class, desc = {"PC端", "用户机构-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<UserOrgShowResDTO> userOrgShowResDTOList = service.showByIds(ids);

        List<UserOrgShowResVO> items = mapper.convertToUserOrgShowResVOList(userOrgShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "用户机构-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson UserOrgModifyReqVO userOrgModifyReqVO) {
        UserOrgModifyReqDTO userOrgModifyReqDTO = mapper.convertToUserOrgModifyReqDTO(userOrgModifyReqVO);

        Boolean isSuccess = service.modify(userOrgModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson UserOrgModifyReqVO userOrgModifyReqVO) {
        UserOrgModifyReqDTO userOrgModifyReqDTO = mapper.convertToUserOrgModifyReqDTO(userOrgModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(userOrgModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userOrg/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson UserOrgRemoveReqVO userOrgRemoveReqVO) {
        UserOrgRemoveReqDTO userOrgRemoveReqDTO = mapper.convertToUserOrgRemoveReqDTO(userOrgRemoveReqVO);

        Boolean isSuccess = service.removeByParams(userOrgRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
