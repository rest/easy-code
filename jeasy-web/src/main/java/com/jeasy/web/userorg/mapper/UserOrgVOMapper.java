package com.jeasy.web.userorg.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.fileattach.dto.FileAttachDTO;
import com.jeasy.userorg.dto.UserOrgAddReqDTO;
import com.jeasy.userorg.dto.UserOrgListReqDTO;
import com.jeasy.userorg.dto.UserOrgListResDTO;
import com.jeasy.userorg.dto.UserOrgModifyReqDTO;
import com.jeasy.userorg.dto.UserOrgPageReqDTO;
import com.jeasy.userorg.dto.UserOrgPageResDTO;
import com.jeasy.userorg.dto.UserOrgRemoveReqDTO;
import com.jeasy.userorg.dto.UserOrgShowResDTO;
import com.jeasy.web.fileattach.vo.FileAttachVO;
import com.jeasy.web.userorg.vo.UserOrgAddReqVO;
import com.jeasy.web.userorg.vo.UserOrgListReqVO;
import com.jeasy.web.userorg.vo.UserOrgListResVO;
import com.jeasy.web.userorg.vo.UserOrgModifyReqVO;
import com.jeasy.web.userorg.vo.UserOrgPageReqVO;
import com.jeasy.web.userorg.vo.UserOrgPageResVO;
import com.jeasy.web.userorg.vo.UserOrgRemoveReqVO;
import com.jeasy.web.userorg.vo.UserOrgShowResVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface UserOrgVOMapper extends BaseVOMapper<FileAttachDTO, FileAttachVO> {

    default UserOrgVOMapper me() {
        return SpringUtil.getBean(UserOrgVOMapper.class);
    }

    UserOrgAddReqDTO convertToUserOrgAddReqDTO(UserOrgAddReqVO userOrgAddReqVO);

    UserOrgShowResVO convertToUserOrgShowResVO(UserOrgShowResDTO userOrgShowResDTO);

    UserOrgModifyReqDTO convertToUserOrgModifyReqDTO(UserOrgModifyReqVO userOrgModifyReqVO);

    UserOrgRemoveReqDTO convertToUserOrgRemoveReqDTO(UserOrgRemoveReqVO userOrgRemoveReqVO);

    UserOrgListReqDTO convertToUserOrgListReqDTO(UserOrgListReqVO userOrgReqVO);

    UserOrgPageReqDTO convertToUserOrgPageReqDTO(UserOrgPageReqVO userOrgPageReqVO);

    UserOrgListResVO convertToUserOrgListResVO(UserOrgListResDTO userOrgListResDTO);

    List<UserOrgListResVO> convertToUserOrgListResVOList(List<UserOrgListResDTO> userOrgListResDTOList);

    Page<UserOrgPageResVO> convertToUserOrgPageResVOPage(Page<UserOrgPageResDTO> userOrgPageResDTOPage);

    List<UserOrgAddReqDTO> convertToUserOrgAddReqDTOList(List<UserOrgAddReqVO> userOrgAddReqVOList);

    List<UserOrgShowResVO> convertToUserOrgShowResVOList(List<UserOrgShowResDTO> userOrgShowResDTOList);
}
