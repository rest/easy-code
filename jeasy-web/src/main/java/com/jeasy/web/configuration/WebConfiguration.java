package com.jeasy.web.configuration;

import cn.hutool.core.util.CharsetUtil;
import com.alibaba.druid.support.http.StatViewServlet;
import com.google.common.collect.Lists;
import com.javabooter.common.cache.CommonCacheManager;
import com.javabooter.common.web.aspect.ParameterValidateAspect;
import com.javabooter.common.web.csrf.CookieCsrfTokenRepository;
import com.javabooter.common.web.csrf.CsrfInterceptor;
import com.javabooter.common.web.interceptor.ControllerCostLogInterceptor;
import com.javabooter.common.web.interceptor.TransferSecurityInterceptor;
import com.javabooter.core.doc.interceptor.TransferDocInterceptor;
import com.javabooter.core.web.resolver.ArgumentFromJsonResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.validation.Validator;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Slf4j
@Configuration
@ComponentScan(basePackages = {"com.jeasy.*.controller", "com.jeasy.*.mapper"})
public class WebConfiguration extends WebMvcConfigurationSupport {

    /**
     * DruidStatView Servlet
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean<StatViewServlet> statViewServlet() {
        ServletRegistrationBean<StatViewServlet> servletRegistrationBean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        servletRegistrationBean.addInitParameter("loginUsername", "admin");
        servletRegistrationBean.addInitParameter("loginPassword", "123456");
        servletRegistrationBean.addInitParameter("resetEnable", "true");
        return servletRegistrationBean;
    }

    /**
     * 自定义参数解析
     *
     * @return
     */
    @Bean
    public RequestMappingHandlerAdapter requestMappingHandlerAdapter(
        @Qualifier("mvcContentNegotiationManager") ContentNegotiationManager contentNegotiationManager,
        @Qualifier("mvcConversionService") FormattingConversionService conversionService,
        @Qualifier("mvcValidator") Validator validator) {
        RequestMappingHandlerAdapter requestMappingHandlerAdapter = super.requestMappingHandlerAdapter(contentNegotiationManager, conversionService, validator);
        requestMappingHandlerAdapter.setCustomArgumentResolvers(Lists.newArrayList(new ArgumentFromJsonResolver()));
        return requestMappingHandlerAdapter;
    }

    /**
     * 文件上传
     *
     * @return
     */
    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setDefaultEncoding(CharsetUtil.defaultCharsetName());
        resolver.setMaxUploadSize(10485760000l);
        resolver.setMaxInMemorySize(40960);
        return resolver;
    }

    /**
     * CSRF Repository
     *
     * @param commonCacheManager
     * @return
     */
    @Bean
    public CookieCsrfTokenRepository cookieCsrfTokenRepository(CommonCacheManager commonCacheManager) {
        CookieCsrfTokenRepository cookieCsrfTokenRepository = new CookieCsrfTokenRepository();
        cookieCsrfTokenRepository.setCacheManager(commonCacheManager);
        cookieCsrfTokenRepository.setCsrfTokenCacheName("oneMinute");
        return cookieCsrfTokenRepository;
    }

    /**
     * Controller请求方法入参校验Aspect
     *
     * @return
     */
    @Bean
    public ParameterValidateAspect parameterValidateAspect() {
        return new ParameterValidateAspect();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ControllerCostLogInterceptor())
            .addPathPatterns("/**")
            .order(Ordered.HIGHEST_PRECEDENCE);

        registry.addInterceptor(new TransferDocInterceptor())
            .addPathPatterns("/**")
            .order(Ordered.HIGHEST_PRECEDENCE + 10);

        TransferSecurityInterceptor transferSecurityInterceptor = new TransferSecurityInterceptor();
        transferSecurityInterceptor.setCheckReferer(false);

        registry.addInterceptor(transferSecurityInterceptor)
            .addPathPatterns("/**")
            .order(Ordered.HIGHEST_PRECEDENCE + 20);

        registry.addInterceptor(new CsrfInterceptor())
            .addPathPatterns("/**")
            .order(Ordered.HIGHEST_PRECEDENCE + 30);
    }
}
