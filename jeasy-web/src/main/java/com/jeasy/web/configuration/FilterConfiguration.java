package com.jeasy.web.configuration;

import com.alibaba.druid.support.http.WebStatFilter;
import com.javabooter.common.safe.HttpServletRequestSafeFilter;
import com.javabooter.core.charset.CharsetUtils;
import com.javabooter.core.web.filter.MdcLogFilter;
import com.thetransactioncompany.cors.CORSFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.filter.OrderedCharacterEncodingFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.DispatcherType;

@Slf4j
@Configuration
public class FilterConfiguration implements WebMvcConfigurer {

    /**
     * request 编码, 放在所有 filter 之前
     *
     * @return
     */
    private CharacterEncodingFilter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new OrderedCharacterEncodingFilter();
        characterEncodingFilter.setEncoding(CharsetUtils.DEFAULT_ENCODE);
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

    @Bean
    public FilterRegistrationBean<CharacterEncodingFilter> registerCharacterEncodingFilter() {
        FilterRegistrationBean<CharacterEncodingFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(characterEncodingFilter());
        registration.addUrlPatterns("/*");
        registration.setName("CharacterEncodingFilter");
        registration.setOrder(FilterOrders.CHARACTER_ENCODING_FILTER);
        return registration;
    }

    /**
     * 解决 xss & sql 漏洞
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean<HttpServletRequestSafeFilter> registerHttpRequestSafeFilter() {
        FilterRegistrationBean<HttpServletRequestSafeFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new HttpServletRequestSafeFilter());
        registration.addUrlPatterns("/*");
        registration.setName("HttpServletRequestSafeFilter");
        registration.setOrder(FilterOrders.HTTP_REQUEST_SAFE_FILTER);
        registration.setDispatcherTypes(DispatcherType.REQUEST);

        registration.addInitParameter("filterXSS", "true");
        registration.addInitParameter("filterSQL", "true");
        return registration;
    }

    /**
     * 解决跨域问题 Filter
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean<CORSFilter> registerCORSFilter() {
        FilterRegistrationBean<CORSFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CORSFilter());
        registration.addUrlPatterns("/*");
        registration.setName("CORSFilter");
        registration.setOrder(FilterOrders.CORS_FILTER);

        registration.addInitParameter("cors.allowOrigin", "*");
        registration.addInitParameter("cors.supportedMethods", "GET, POST, HEAD, PUT, DELETE");
        registration.addInitParameter("cors.supportedHeaders",
            "Accept, Origin, X-Requested-With, Content-Type, Last-Modified");
        registration.addInitParameter("cors.exposedHeaders", "Set-Cookie, oauthstatus");
        registration.addInitParameter("cors.supportsCredentials", "true");
        return registration;
    }

    /**
     * 连接池 启用 Web 监控统计功能
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean<WebStatFilter> registerWebStatFilter() {
        FilterRegistrationBean<WebStatFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new WebStatFilter());
        registration.addUrlPatterns("/*");
        registration.setName("WebStatFilter");
        registration.setOrder(FilterOrders.WEB_STAT_FILTER);

        registration.addInitParameter("exclusions", "*.js,*mp3,*.swf,*.xls,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        registration.addInitParameter("sessionStatMaxCount", "1000");
        return registration;
    }

    /**
     * MdcLogFilter 配置
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean<MdcLogFilter> registerMdcLogFilter() {
        FilterRegistrationBean<MdcLogFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new MdcLogFilter());
        registration.addUrlPatterns("/*");
        registration.setName("MdcLogFilter");
        registration.setOrder(FilterOrders.MDC_LOG_FILTER);
        return registration;
    }

    class FilterOrders {
        public static final int CHARACTER_ENCODING_FILTER = Ordered.HIGHEST_PRECEDENCE;
        public static final int MDC_LOG_FILTER = 0;
        public static final int HTTP_REQUEST_SAFE_FILTER = 10;
        public static final int CORS_FILTER = 20;
        public static final int WEB_STAT_FILTER = 30;
        public static final int REFERER_FILTER = 40;
    }
}


