package com.jeasy.web.userrole.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userrole.dto.UserRoleAddReqDTO;
import com.jeasy.userrole.dto.UserRoleDTO;
import com.jeasy.userrole.dto.UserRoleListReqDTO;
import com.jeasy.userrole.dto.UserRoleListResDTO;
import com.jeasy.userrole.dto.UserRoleModifyReqDTO;
import com.jeasy.userrole.dto.UserRolePageReqDTO;
import com.jeasy.userrole.dto.UserRolePageResDTO;
import com.jeasy.userrole.dto.UserRoleRemoveReqDTO;
import com.jeasy.userrole.dto.UserRoleShowResDTO;
import com.jeasy.web.userrole.vo.UserRoleAddReqVO;
import com.jeasy.web.userrole.vo.UserRoleListReqVO;
import com.jeasy.web.userrole.vo.UserRoleListResVO;
import com.jeasy.web.userrole.vo.UserRoleModifyReqVO;
import com.jeasy.web.userrole.vo.UserRolePageReqVO;
import com.jeasy.web.userrole.vo.UserRolePageResVO;
import com.jeasy.web.userrole.vo.UserRoleRemoveReqVO;
import com.jeasy.web.userrole.vo.UserRoleShowResVO;
import com.jeasy.web.userrole.vo.UserRoleVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface UserRoleVOMapper extends BaseVOMapper<UserRoleDTO, UserRoleVO> {

    default UserRoleVOMapper me() {
        return SpringUtil.getBean(UserRoleVOMapper.class);
    }

    UserRoleAddReqDTO convertToUserRoleAddReqDTO(UserRoleAddReqVO userRoleAddReqVO);

    UserRoleShowResVO convertToUserRoleShowResVO(UserRoleShowResDTO userRoleShowResDTO);

    UserRoleModifyReqDTO convertToUserRoleModifyReqDTO(UserRoleModifyReqVO userRoleModifyReqVO);

    UserRoleRemoveReqDTO convertToUserRoleRemoveReqDTO(UserRoleRemoveReqVO userRoleRemoveReqVO);

    UserRoleListReqDTO convertToUserRoleListReqDTO(UserRoleListReqVO userRoleReqVO);

    UserRolePageReqDTO convertToUserRolePageReqDTO(UserRolePageReqVO userRolePageReqVO);

    UserRoleListResVO convertToUserRoleListResVO(UserRoleListResDTO userRoleListResDTO);

    List<UserRoleListResVO> convertToUserRoleListResVOList(List<UserRoleListResDTO> userRoleListResDTOList);

    Page<UserRolePageResVO> convertToUserRolePageResVOPage(Page<UserRolePageResDTO> userRolePageResDTOPage);

    List<UserRoleAddReqDTO> convertToUserRoleAddReqDTOList(List<UserRoleAddReqVO> userRoleAddReqVOList);

    List<UserRoleShowResVO> convertToUserRoleShowResVOList(List<UserRoleShowResDTO> userRoleShowResDTOList);
}
