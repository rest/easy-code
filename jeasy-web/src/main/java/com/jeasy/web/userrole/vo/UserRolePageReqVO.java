package com.jeasy.web.userrole.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户角色 分页 ReqVO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRolePageReqVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 主键
     */
    @InitField(value = "", desc = "主键")
    private String id;

    /**
     * 用户ID
     */
    @InitField(value = "", desc = "用户ID")
    private String userId;

    /**
     * 用户名称
     */
    @InitField(value = "", desc = "用户名称")
    private String userName;

    /**
     * 用户编码
     */
    @InitField(value = "", desc = "用户编码")
    private String userCode;

    /**
     * 角色ID
     */
    @InitField(value = "", desc = "角色ID")
    private String roleId;

    /**
     * 角色名称
     */
    @InitField(value = "", desc = "角色名称")
    private String roleName;

    /**
     * 角色编码
     */
    @InitField(value = "", desc = "角色编码")
    private String roleCode;

    /**
     * 创建时间
     */
    @InitField(value = "", desc = "创建时间")
    private Long createAt;

    /**
     * 创建人ID
     */
    @InitField(value = "", desc = "创建人ID")
    private String createBy;

    /**
     * 创建人名称
     */
    @InitField(value = "", desc = "创建人名称")
    private String createName;

    /**
     * 更新时间
     */
    @InitField(value = "", desc = "更新时间")
    private Long updateAt;

    /**
     * 更新人ID
     */
    @InitField(value = "", desc = "更新人ID")
    private String updateBy;

    /**
     * 更新人名称
     */
    @InitField(value = "", desc = "更新人名称")
    private String updateName;

    /**
     * 是否删除
     */
    @InitField(value = "", desc = "是否删除")
    private Integer isDel;

    /**
     * 是否测试
     */
    @InitField(value = "", desc = "是否测试")
    private Integer isTest;

}
