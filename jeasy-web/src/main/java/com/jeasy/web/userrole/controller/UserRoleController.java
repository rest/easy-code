package com.jeasy.web.userrole.controller;

import com.jeasy.userrole.dto.UserRoleAddReqDTO;
import com.jeasy.userrole.dto.UserRoleListReqDTO;
import com.jeasy.userrole.dto.UserRoleListResDTO;
import com.jeasy.userrole.dto.UserRoleModifyReqDTO;
import com.jeasy.userrole.dto.UserRolePageReqDTO;
import com.jeasy.userrole.dto.UserRolePageResDTO;
import com.jeasy.userrole.dto.UserRoleRemoveReqDTO;
import com.jeasy.userrole.dto.UserRoleShowResDTO;
import com.jeasy.userrole.service.UserRoleService;
import com.jeasy.web.userrole.mapper.UserRoleVOMapper;
import com.jeasy.web.userrole.vo.UserRoleAddReqVO;
import com.jeasy.web.userrole.vo.UserRoleListReqVO;
import com.jeasy.web.userrole.vo.UserRoleListResVO;
import com.jeasy.web.userrole.vo.UserRoleModifyReqVO;
import com.jeasy.web.userrole.vo.UserRolePageReqVO;
import com.jeasy.web.userrole.vo.UserRolePageResVO;
import com.jeasy.web.userrole.vo.UserRoleRemoveReqVO;
import com.jeasy.web.userrole.vo.UserRoleShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户角色 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class UserRoleController extends BaseController<UserRoleService, UserRoleVOMapper> {

    @MethodDoc(items = UserRoleListResVO.class, desc = {"PC端", "用户角色-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/list", method = {RequestMethod.GET})
    public void list(final @FromJson UserRoleListReqVO userRoleListReqVO) {
        UserRoleListReqDTO userRoleListReqDTO = mapper.convertToUserRoleListReqDTO(userRoleListReqVO);

        List<UserRoleListResDTO> userRoleListResDTOList = service.list(userRoleListReqDTO);
        List<UserRoleListResVO> items = mapper.convertToUserRoleListResVOList(userRoleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserRoleListResVO.class, desc = {"PC端", "用户角色-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson UserRoleListReqVO userRoleListReqVO) {
        UserRoleListReqDTO userRoleListReqDTO = mapper.convertToUserRoleListReqDTO(userRoleListReqVO);

        List<UserRoleListResDTO> userRoleListResDTOList = service.listByVersion1(userRoleListReqDTO);
        List<UserRoleListResVO> items = mapper.convertToUserRoleListResVOList(userRoleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserRoleListResVO.class, desc = {"PC端", "用户角色-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson UserRoleListReqVO userRoleListReqVO) {
        UserRoleListReqDTO userRoleListReqDTO = mapper.convertToUserRoleListReqDTO(userRoleListReqVO);

        List<UserRoleListResDTO> userRoleListResDTOList = service.listByVersion2(userRoleListReqDTO);
        List<UserRoleListResVO> items = mapper.convertToUserRoleListResVOList(userRoleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserRoleListResVO.class, desc = {"PC端", "用户角色-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson UserRoleListReqVO userRoleListReqVO) {
        UserRoleListReqDTO userRoleListReqDTO = mapper.convertToUserRoleListReqDTO(userRoleListReqVO);

        List<UserRoleListResDTO> userRoleListResDTOList = service.listByVersion3(userRoleListReqDTO);
        List<UserRoleListResVO> items = mapper.convertToUserRoleListResVOList(userRoleListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = UserRoleListResVO.class, desc = {"PC端", "用户角色-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson UserRoleListReqVO userRoleListReqVO) {
        UserRoleListReqDTO userRoleListReqDTO = mapper.convertToUserRoleListReqDTO(userRoleListReqVO);

        UserRoleListResDTO userRoleListResDTO = service.listOne(userRoleListReqDTO);
        UserRoleListResVO item = mapper.convertToUserRoleListResVO(userRoleListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(pages = UserRolePageResVO.class, desc = {"PC端", "用户角色-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/page", method = {RequestMethod.GET})
    public void page(final @FromJson UserRolePageReqVO userRolePageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        UserRolePageReqDTO userRolePageReqDTO = mapper.convertToUserRolePageReqDTO(userRolePageReqVO);

        Page<UserRolePageResDTO> userRolePageResDTOPage = service.pagination(userRolePageReqDTO, current, size);
        Page<UserRolePageResVO> userRolePageResVOPage = mapper.convertToUserRolePageResVOPage(userRolePageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, userRolePageResVOPage.getTotal(), userRolePageResVOPage.getRecords(), size, current);
    }

    @MethodDoc(desc = {"PC端", "用户角色-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/add", method = {RequestMethod.POST})
    public void add(final @FromJson UserRoleAddReqVO userRoleAddReqVO) {
        UserRoleAddReqDTO userRoleAddReqDTO = mapper.convertToUserRoleAddReqDTO(userRoleAddReqVO);

        Boolean isSuccess = service.add(userRoleAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson UserRoleAddReqVO userRoleAddReqVO) {
        UserRoleAddReqDTO userRoleAddReqDTO = mapper.convertToUserRoleAddReqDTO(userRoleAddReqVO);

        Boolean isSuccess = service.addAllColumn(userRoleAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<UserRoleAddReqVO> userRoleAddReqVOList) {
        List<UserRoleAddReqDTO> userRoleAddReqDTOList = mapper.convertToUserRoleAddReqDTOList(userRoleAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(userRoleAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(item = UserRoleShowResVO.class, desc = {"PC端", "用户角色-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        UserRoleShowResDTO userRoleShowResDTO = service.show(id);

        UserRoleShowResVO item = mapper.convertToUserRoleShowResVO(userRoleShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = UserRoleShowResVO.class, desc = {"PC端", "用户角色-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<UserRoleShowResDTO> userRoleShowResDTOList = service.showByIds(ids);

        List<UserRoleShowResVO> items = mapper.convertToUserRoleShowResVOList(userRoleShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "用户角色-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson UserRoleModifyReqVO userRoleModifyReqVO) {
        UserRoleModifyReqDTO userRoleModifyReqDTO = mapper.convertToUserRoleModifyReqDTO(userRoleModifyReqVO);

        Boolean isSuccess = service.modify(userRoleModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson UserRoleModifyReqVO userRoleModifyReqVO) {
        UserRoleModifyReqDTO userRoleModifyReqDTO = mapper.convertToUserRoleModifyReqDTO(userRoleModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(userRoleModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户角色-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userRole/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson UserRoleRemoveReqVO userRoleRemoveReqVO) {
        UserRoleRemoveReqDTO userRoleRemoveReqDTO = mapper.convertToUserRoleRemoveReqDTO(userRoleRemoveReqVO);

        Boolean isSuccess = service.removeByParams(userRoleRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
