package com.jeasy.web.user.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.user.dto.UserAddReqDTO;
import com.jeasy.user.dto.UserDTO;
import com.jeasy.user.dto.UserListOrganizationReqDTO;
import com.jeasy.user.dto.UserListOrganizationResDTO;
import com.jeasy.user.dto.UserListReqDTO;
import com.jeasy.user.dto.UserListResDTO;
import com.jeasy.user.dto.UserListRoleReqDTO;
import com.jeasy.user.dto.UserListRoleResDTO;
import com.jeasy.user.dto.UserModifyOrganizationReqDTO;
import com.jeasy.user.dto.UserModifyReqDTO;
import com.jeasy.user.dto.UserModifyRoleReqDTO;
import com.jeasy.user.dto.UserPageReqDTO;
import com.jeasy.user.dto.UserPageResDTO;
import com.jeasy.user.dto.UserPageRoleReqDTO;
import com.jeasy.user.dto.UserPageRoleResDTO;
import com.jeasy.user.dto.UserRemoveReqDTO;
import com.jeasy.user.dto.UserShowResDTO;
import com.jeasy.web.user.vo.UserAddReqVO;
import com.jeasy.web.user.vo.UserListOrganizationReqVO;
import com.jeasy.web.user.vo.UserListOrganizationResVO;
import com.jeasy.web.user.vo.UserListReqVO;
import com.jeasy.web.user.vo.UserListResVO;
import com.jeasy.web.user.vo.UserListRoleReqVO;
import com.jeasy.web.user.vo.UserListRoleResVO;
import com.jeasy.web.user.vo.UserModifyOrganizationReqVO;
import com.jeasy.web.user.vo.UserModifyReqVO;
import com.jeasy.web.user.vo.UserModifyRoleReqVO;
import com.jeasy.web.user.vo.UserPageReqVO;
import com.jeasy.web.user.vo.UserPageResVO;
import com.jeasy.web.user.vo.UserPageRoleReqVO;
import com.jeasy.web.user.vo.UserPageRoleResVO;
import com.jeasy.web.user.vo.UserRemoveReqVO;
import com.jeasy.web.user.vo.UserShowResVO;
import com.jeasy.web.user.vo.UserVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface UserVOMapper extends BaseVOMapper<UserDTO, UserVO> {

    default UserVOMapper me() {
        return SpringUtil.getBean(UserVOMapper.class);
    }

    UserAddReqDTO convertToUserAddReqDTO(UserAddReqVO userAddReqVO);

    UserShowResVO convertToUserShowResVO(UserShowResDTO userShowResDTO);

    UserModifyReqDTO convertToUserModifyReqDTO(UserModifyReqVO userModifyReqVO);

    UserRemoveReqDTO convertToUserRemoveReqDTO(UserRemoveReqVO userRemoveReqVO);

    UserListReqDTO convertToUserListReqDTO(UserListReqVO userReqVO);

    UserPageReqDTO convertToUserPageReqDTO(UserPageReqVO userPageReqVO);

    UserListResVO convertToUserListResVO(UserListResDTO userListResDTO);

    List<UserListResVO> convertToUserListResVOList(List<UserListResDTO> userListResDTOList);

    Page<UserPageResVO> convertToUserPageResVOPage(Page<UserPageResDTO> userPageResDTOPage);

    List<UserAddReqDTO> convertToUserAddReqDTOList(List<UserAddReqVO> userAddReqVOList);

    List<UserShowResVO> convertToUserShowResVOList(List<UserShowResDTO> userShowResDTOList);

    UserPageRoleReqDTO convertToUserPageRoleReqDTO(UserPageRoleReqVO userPageRoleReqVO);

    Page<UserPageRoleResVO> convertToUserPageRoleResVOPage(Page<UserPageRoleResDTO> userPageRoleResDTOPage);

    UserListRoleReqDTO convertToUserListRoleReqDTO(UserListRoleReqVO userListRoleReqVO);

    List<UserListRoleResVO> convertToUserListRoleResVOList(List<UserListRoleResDTO> userListRoleResDTOList);

    UserListOrganizationReqDTO convertToUserListOrganizationReqDTO(UserListOrganizationReqVO userListOrganizationReqVO);

    List<UserListOrganizationResVO> convertToUserListOrganizationResVOList(List<UserListOrganizationResDTO> userListOrganizationResDTOList);

    UserModifyRoleReqDTO convertToUserModifyRoleReqDTO(UserModifyRoleReqVO userModifyRoleReqVO);

    UserModifyOrganizationReqDTO convertToUserModifyOrganizationReqDTO(UserModifyOrganizationReqVO userModifyOrganizationReqVO);
}
