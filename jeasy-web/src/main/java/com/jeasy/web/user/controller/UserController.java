package com.jeasy.web.user.controller;

import com.jeasy.user.dto.UserAddReqDTO;
import com.jeasy.user.dto.UserListOrganizationReqDTO;
import com.jeasy.user.dto.UserListOrganizationResDTO;
import com.jeasy.user.dto.UserListReqDTO;
import com.jeasy.user.dto.UserListResDTO;
import com.jeasy.user.dto.UserListRoleReqDTO;
import com.jeasy.user.dto.UserListRoleResDTO;
import com.jeasy.user.dto.UserModifyOrganizationReqDTO;
import com.jeasy.user.dto.UserModifyReqDTO;
import com.jeasy.user.dto.UserModifyRoleReqDTO;
import com.jeasy.user.dto.UserPageReqDTO;
import com.jeasy.user.dto.UserPageResDTO;
import com.jeasy.user.dto.UserPageRoleReqDTO;
import com.jeasy.user.dto.UserPageRoleResDTO;
import com.jeasy.user.dto.UserRemoveReqDTO;
import com.jeasy.user.dto.UserShowResDTO;
import com.jeasy.user.service.UserService;
import com.jeasy.web.user.mapper.UserVOMapper;
import com.jeasy.web.user.vo.UserAddReqVO;
import com.jeasy.web.user.vo.UserListOrganizationReqVO;
import com.jeasy.web.user.vo.UserListOrganizationResVO;
import com.jeasy.web.user.vo.UserListReqVO;
import com.jeasy.web.user.vo.UserListResVO;
import com.jeasy.web.user.vo.UserListRoleReqVO;
import com.jeasy.web.user.vo.UserListRoleResVO;
import com.jeasy.web.user.vo.UserModifyOrganizationReqVO;
import com.jeasy.web.user.vo.UserModifyReqVO;
import com.jeasy.web.user.vo.UserModifyRoleReqVO;
import com.jeasy.web.user.vo.UserPageReqVO;
import com.jeasy.web.user.vo.UserPageResVO;
import com.jeasy.web.user.vo.UserPageRoleReqVO;
import com.jeasy.web.user.vo.UserPageRoleResVO;
import com.jeasy.web.user.vo.UserRemoveReqVO;
import com.jeasy.web.user.vo.UserShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class UserController extends BaseController<UserService, UserVOMapper> {

    @MethodDoc(items = UserListResVO.class, desc = {"PC端", "用户-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/list", method = {RequestMethod.GET})
    public void list(final @FromJson UserListReqVO userListReqVO) {
        UserListReqDTO userListReqDTO = mapper.convertToUserListReqDTO(userListReqVO);

        List<UserListResDTO> userListResDTOList = service.list(userListReqDTO);
        List<UserListResVO> items = mapper.convertToUserListResVOList(userListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserListResVO.class, desc = {"PC端", "用户-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson UserListReqVO userListReqVO) {
        UserListReqDTO userListReqDTO = mapper.convertToUserListReqDTO(userListReqVO);

        List<UserListResDTO> userListResDTOList = service.listByVersion1(userListReqDTO);
        List<UserListResVO> items = mapper.convertToUserListResVOList(userListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserListResVO.class, desc = {"PC端", "用户-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson UserListReqVO userListReqVO) {
        UserListReqDTO userListReqDTO = mapper.convertToUserListReqDTO(userListReqVO);

        List<UserListResDTO> userListResDTOList = service.listByVersion2(userListReqDTO);
        List<UserListResVO> items = mapper.convertToUserListResVOList(userListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserListResVO.class, desc = {"PC端", "用户-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson UserListReqVO userListReqVO) {
        UserListReqDTO userListReqDTO = mapper.convertToUserListReqDTO(userListReqVO);

        List<UserListResDTO> userListResDTOList = service.listByVersion3(userListReqDTO);
        List<UserListResVO> items = mapper.convertToUserListResVOList(userListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = UserListResVO.class, desc = {"PC端", "用户-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson UserListReqVO userListReqVO) {
        UserListReqDTO userListReqDTO = mapper.convertToUserListReqDTO(userListReqVO);

        UserListResDTO userListResDTO = service.listOne(userListReqDTO);
        UserListResVO item = mapper.convertToUserListResVO(userListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:CX"})
    @MethodDoc(pages = UserPageResVO.class, desc = {"PC端", "用户-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/page", method = {RequestMethod.GET})
    public void page(final @FromJson UserPageReqVO userPageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        UserPageReqDTO userPageReqDTO = mapper.convertToUserPageReqDTO(userPageReqVO);

        Page<UserPageResDTO> userPageResDTOPage = service.pagination(userPageReqDTO, current, size);
        Page<UserPageResVO> userPageResVOPage = mapper.convertToUserPageResVOPage(userPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, userPageResVOPage.getTotal(), userPageResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:JSPZ"})
    @MethodDoc(pages = UserPageRoleResVO.class, desc = {"PC端", "用户-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/pageRole", method = {RequestMethod.GET})
    public void pageRole(final @FromJson UserPageRoleReqVO userPageRoleReqVO,
                         final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                         final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        UserPageRoleReqDTO userPageRoleReqDTO = mapper.convertToUserPageRoleReqDTO(userPageRoleReqVO);

        Page<UserPageRoleResDTO> userPageRoleResDTOPage = service.pagination(userPageRoleReqDTO, current, size);
        Page<UserPageRoleResVO> userPageRoleResVOPage = mapper.convertToUserPageRoleResVOPage(userPageRoleResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, userPageRoleResVOPage.getTotal(), userPageRoleResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:JSPZ"})
    @MethodDoc(pages = UserListRoleResVO.class, desc = {"PC端", "用户-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/listRole", method = {RequestMethod.GET})
    public void listRole(final @FromJson UserListRoleReqVO userListRoleReqVO) {
        UserListRoleReqDTO userListRoleReqDTO = mapper.convertToUserListRoleReqDTO(userListRoleReqVO);

        List<UserListRoleResDTO> userListRoleResDTOList = service.listRole(userListRoleReqDTO);
        List<UserListRoleResVO> items = mapper.convertToUserListRoleResVOList(userListRoleResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:XZ"})
    @MethodDoc(desc = {"PC端", "用户-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/add", method = {RequestMethod.POST})
    public void add(final @FromJson UserAddReqVO userAddReqVO) {
        UserAddReqDTO userAddReqDTO = mapper.convertToUserAddReqDTO(userAddReqVO);

        Boolean isSuccess = service.add(userAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson UserAddReqVO userAddReqVO) {
        UserAddReqDTO userAddReqDTO = mapper.convertToUserAddReqDTO(userAddReqVO);

        Boolean isSuccess = service.addAllColumn(userAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<UserAddReqVO> userAddReqVOList) {
        List<UserAddReqDTO> userAddReqDTOList = mapper.convertToUserAddReqDTOList(userAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(userAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:CK"})
    @MethodDoc(item = UserShowResVO.class, desc = {"PC端", "用户-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        UserShowResDTO userShowResDTO = service.show(id);

        UserShowResVO item = mapper.convertToUserShowResVO(userShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:JGPZ"})
    @MethodDoc(item = UserListOrganizationResVO.class, desc = {"PC端", "用户-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/listOrganization", method = {RequestMethod.GET})
    public void listOrganization(final @FromJson UserListOrganizationReqVO userListOrganizationReqVO) {
        UserListOrganizationReqDTO userListOrganizationReqDTO = mapper.convertToUserListOrganizationReqDTO(userListOrganizationReqVO);

        List<UserListOrganizationResDTO> userListOrganizationResDTOList = service.listOrganization(userListOrganizationReqDTO);
        List<UserListOrganizationResVO> items = mapper.convertToUserListOrganizationResVOList(userListOrganizationResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = UserShowResVO.class, desc = {"PC端", "用户-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<UserShowResDTO> userShowResDTOList = service.showByIds(ids);

        List<UserShowResVO> items = mapper.convertToUserShowResVOList(userShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:BJ"})
    @MethodDoc(desc = {"PC端", "用户-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson UserModifyReqVO userModifyReqVO) {
        UserModifyReqDTO userModifyReqDTO = mapper.convertToUserModifyReqDTO(userModifyReqVO);

        Boolean isSuccess = service.modify(userModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:JSPZ"})
    @MethodDoc(desc = {"PC端", "用户-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/modifyRole", method = {RequestMethod.POST})
    public void modifyRole(final @FromJson UserModifyRoleReqVO userModifyRoleReqVO) {
        UserModifyRoleReqDTO userModifyRoleReqDTO = mapper.convertToUserModifyRoleReqDTO(userModifyRoleReqVO);

        Boolean isSuccess = service.modifyRole(userModifyRoleReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:JGPZ"})
    @MethodDoc(desc = {"PC端", "用户-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/modifyOrganization", method = {RequestMethod.POST})
    public void modifyOrganization(final @FromJson UserModifyOrganizationReqVO userModifyOrganizationReqVO) {
        UserModifyOrganizationReqDTO userModifyOrganizationReqDTO = mapper.convertToUserModifyOrganizationReqDTO(userModifyOrganizationReqVO);

        Boolean isSuccess = service.modifyOrganization(userModifyOrganizationReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson UserModifyReqVO userModifyReqVO) {
        UserModifyReqDTO userModifyReqDTO = mapper.convertToUserModifyReqDTO(userModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(userModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:RYGL:SC"})
    @MethodDoc(desc = {"PC端", "用户-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "用户-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "user/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson UserRemoveReqVO userRemoveReqVO) {
        UserRemoveReqDTO userRemoveReqDTO = mapper.convertToUserRemoveReqDTO(userRemoveReqVO);

        Boolean isSuccess = service.removeByParams(userRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
