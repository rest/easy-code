package com.jeasy.web.user.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户 添加 ReqVO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAddReqVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 名称
     */
    @InitField(value = "", desc = "名称")
    private String name;

    /**
     * 登录名
     */
    @InitField(value = "", desc = "登录名")
    private String loginName;

    /**
     * 密码
     */
    @InitField(value = "", desc = "密码")
    private String pwd;

    /**
     * 加密盐
     */
    @InitField(value = "", desc = "加密盐")
    private String salt;

    /**
     * 手机号
     */
    @InitField(value = "", desc = "手机号")
    private String mobile;

    /**
     * 用户状态编码:字典
     */
    @InitField(value = "", desc = "用户状态编码:字典")
    private String statusCode;

    /**
     * 备注
     */
    @InitField(value = "", desc = "备注")
    private String remark;
}
