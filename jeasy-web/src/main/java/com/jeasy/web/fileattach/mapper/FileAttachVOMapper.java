package com.jeasy.web.fileattach.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.fileattach.dto.FileAttachAddReqDTO;
import com.jeasy.fileattach.dto.FileAttachDTO;
import com.jeasy.fileattach.dto.FileAttachListReqDTO;
import com.jeasy.fileattach.dto.FileAttachListResDTO;
import com.jeasy.fileattach.dto.FileAttachModifyReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageResDTO;
import com.jeasy.fileattach.dto.FileAttachRemoveReqDTO;
import com.jeasy.fileattach.dto.FileAttachShowResDTO;
import com.jeasy.web.fileattach.vo.FileAttachAddReqVO;
import com.jeasy.web.fileattach.vo.FileAttachListReqVO;
import com.jeasy.web.fileattach.vo.FileAttachListResVO;
import com.jeasy.web.fileattach.vo.FileAttachModifyReqVO;
import com.jeasy.web.fileattach.vo.FileAttachPageReqVO;
import com.jeasy.web.fileattach.vo.FileAttachPageResVO;
import com.jeasy.web.fileattach.vo.FileAttachRemoveReqVO;
import com.jeasy.web.fileattach.vo.FileAttachShowResVO;
import com.jeasy.web.fileattach.vo.FileAttachVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface FileAttachVOMapper extends BaseVOMapper<FileAttachDTO, FileAttachVO> {

    default FileAttachVOMapper me() {
        return SpringUtil.getBean(FileAttachVOMapper.class);
    }

    FileAttachAddReqDTO convertToFileAttachAddReqDTO(FileAttachAddReqVO fileAttachAddReqVO);

    FileAttachShowResVO convertToFileAttachShowResVO(FileAttachShowResDTO fileAttachShowResDTO);

    FileAttachModifyReqDTO convertToFileAttachModifyReqDTO(FileAttachModifyReqVO fileAttachModifyReqVO);

    FileAttachRemoveReqDTO convertToFileAttachRemoveReqDTO(FileAttachRemoveReqVO fileAttachRemoveReqVO);

    FileAttachListReqDTO convertToFileAttachListReqDTO(FileAttachListReqVO fileAttachReqVO);

    FileAttachPageReqDTO convertToFileAttachPageReqDTO(FileAttachPageReqVO fileAttachPageReqVO);

    FileAttachListResVO convertToFileAttachListResVO(FileAttachListResDTO fileAttachListResDTO);

    List<FileAttachListResVO> convertToFileAttachListResVOList(List<FileAttachListResDTO> fileAttachListResDTOList);

    Page<FileAttachPageResVO> convertToFileAttachPageResVOPage(Page<FileAttachPageResDTO> fileAttachPageResDTOPage);

    List<FileAttachAddReqDTO> convertToFileAttachAddReqDTOList(List<FileAttachAddReqVO> fileAttachAddReqVOList);

    List<FileAttachShowResVO> convertToFileAttachShowResVOList(List<FileAttachShowResDTO> fileAttachShowResDTOList);
}
