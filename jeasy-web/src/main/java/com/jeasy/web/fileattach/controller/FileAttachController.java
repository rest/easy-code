package com.jeasy.web.fileattach.controller;

import com.jeasy.fileattach.dto.FileAttachAddReqDTO;
import com.jeasy.fileattach.dto.FileAttachListReqDTO;
import com.jeasy.fileattach.dto.FileAttachListResDTO;
import com.jeasy.fileattach.dto.FileAttachModifyReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageResDTO;
import com.jeasy.fileattach.dto.FileAttachRemoveReqDTO;
import com.jeasy.fileattach.dto.FileAttachShowResDTO;
import com.jeasy.fileattach.service.FileAttachService;
import com.jeasy.web.fileattach.mapper.FileAttachVOMapper;
import com.jeasy.web.fileattach.vo.FileAttachAddReqVO;
import com.jeasy.web.fileattach.vo.FileAttachListReqVO;
import com.jeasy.web.fileattach.vo.FileAttachListResVO;
import com.jeasy.web.fileattach.vo.FileAttachModifyReqVO;
import com.jeasy.web.fileattach.vo.FileAttachPageReqVO;
import com.jeasy.web.fileattach.vo.FileAttachPageResVO;
import com.jeasy.web.fileattach.vo.FileAttachRemoveReqVO;
import com.jeasy.web.fileattach.vo.FileAttachShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 文件附件 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@RestController
public class FileAttachController extends BaseController<FileAttachService, FileAttachVOMapper> {

    @MethodDoc(items = FileAttachListResVO.class, desc = {"PC端", "文件附件-列表查询", "列表查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/list", method = {RequestMethod.GET})
    public void list(final @FromJson FileAttachListReqVO fileAttachListReqVO) {
        FileAttachListReqDTO fileAttachListReqDTO = mapper.convertToFileAttachListReqDTO(fileAttachListReqVO);

        List<FileAttachListResDTO> fileAttachListResDTOList = service.list(fileAttachListReqDTO);
        List<FileAttachListResVO> items = mapper.convertToFileAttachListResVOList(fileAttachListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = FileAttachListResVO.class, desc = {"PC端", "文件附件-列表查询", "列表查询1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson FileAttachListReqVO fileAttachListReqVO) {
        FileAttachListReqDTO fileAttachListReqDTO = mapper.convertToFileAttachListReqDTO(fileAttachListReqVO);

        List<FileAttachListResDTO> fileAttachListResDTOList = service.listByVersion1(fileAttachListReqDTO);
        List<FileAttachListResVO> items = mapper.convertToFileAttachListResVOList(fileAttachListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = FileAttachListResVO.class, desc = {"PC端", "文件附件-列表查询", "列表查询1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson FileAttachListReqVO fileAttachListReqVO) {
        FileAttachListReqDTO fileAttachListReqDTO = mapper.convertToFileAttachListReqDTO(fileAttachListReqVO);

        List<FileAttachListResDTO> fileAttachListResDTOList = service.listByVersion2(fileAttachListReqDTO);
        List<FileAttachListResVO> items = mapper.convertToFileAttachListResVOList(fileAttachListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = FileAttachListResVO.class, desc = {"PC端", "文件附件-列表查询", "列表查询1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson FileAttachListReqVO fileAttachListReqVO) {
        FileAttachListReqDTO fileAttachListReqDTO = mapper.convertToFileAttachListReqDTO(fileAttachListReqVO);

        List<FileAttachListResDTO> fileAttachListResDTOList = service.listByVersion3(fileAttachListReqDTO);
        List<FileAttachListResVO> items = mapper.convertToFileAttachListResVOList(fileAttachListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = FileAttachListResVO.class, desc = {"PC端", "文件附件-列表查询", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson FileAttachListReqVO fileAttachListReqVO) {
        FileAttachListReqDTO fileAttachListReqDTO = mapper.convertToFileAttachListReqDTO(fileAttachListReqVO);

        FileAttachListResDTO fileAttachListResDTO = service.listOne(fileAttachListReqDTO);
        FileAttachListResVO item = mapper.convertToFileAttachListResVO(fileAttachListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(pages = FileAttachPageResVO.class, desc = {"PC端", "文件附件-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/page", method = {RequestMethod.GET})
    public void page(final @FromJson FileAttachPageReqVO fileAttachPageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        FileAttachPageReqDTO fileAttachPageReqDTO = mapper.convertToFileAttachPageReqDTO(fileAttachPageReqVO);

        Page<FileAttachPageResDTO> fileAttachPageResDTOPage = service.pagination(fileAttachPageReqDTO, current, size);
        Page<FileAttachPageResVO> fileAttachPageResVOPage = mapper.convertToFileAttachPageResVOPage(fileAttachPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, fileAttachPageResVOPage.getTotal(), fileAttachPageResVOPage.getRecords(), size, current);
    }

    @MethodDoc(desc = {"PC端", "文件附件-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/add", method = {RequestMethod.POST})
    public void add(final @FromJson FileAttachAddReqVO fileAttachAddReqVO) {
        FileAttachAddReqDTO fileAttachAddReqDTO = mapper.convertToFileAttachAddReqDTO(fileAttachAddReqVO);

        Boolean isSuccess = service.add(fileAttachAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson FileAttachAddReqVO fileAttachAddReqVO) {
        FileAttachAddReqDTO fileAttachAddReqDTO = mapper.convertToFileAttachAddReqDTO(fileAttachAddReqVO);

        Boolean isSuccess = service.addAllColumn(fileAttachAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<FileAttachAddReqVO> fileAttachAddReqVOList) {
        List<FileAttachAddReqDTO> fileAttachAddReqDTOList = mapper.convertToFileAttachAddReqDTOList(fileAttachAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(fileAttachAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(item = FileAttachShowResVO.class, desc = {"PC端", "文件附件-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        FileAttachShowResDTO fileAttachShowResDTO = service.show(id);

        FileAttachShowResVO item = mapper.convertToFileAttachShowResVO(fileAttachShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = FileAttachShowResVO.class, desc = {"PC端", "文件附件-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<FileAttachShowResDTO> fileAttachShowResDTOList = service.showByIds(ids);

        List<FileAttachShowResVO> items = mapper.convertToFileAttachShowResVOList(fileAttachShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "文件附件-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson FileAttachModifyReqVO fileAttachModifyReqVO) {
        FileAttachModifyReqDTO fileAttachModifyReqDTO = mapper.convertToFileAttachModifyReqDTO(fileAttachModifyReqVO);

        Boolean isSuccess = service.modify(fileAttachModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson FileAttachModifyReqVO fileAttachModifyReqVO) {
        FileAttachModifyReqDTO fileAttachModifyReqDTO = mapper.convertToFileAttachModifyReqDTO(fileAttachModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(fileAttachModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "文件附件-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "fileAttach/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson FileAttachRemoveReqVO fileAttachRemoveReqVO) {
        FileAttachRemoveReqDTO fileAttachRemoveReqDTO = mapper.convertToFileAttachRemoveReqDTO(fileAttachRemoveReqVO);

        Boolean isSuccess = service.removeByParams(fileAttachRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
