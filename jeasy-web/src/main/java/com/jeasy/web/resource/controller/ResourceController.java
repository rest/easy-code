package com.jeasy.web.resource.controller;

import com.jeasy.resource.dto.ResourceAddReqDTO;
import com.jeasy.resource.dto.ResourceListReqDTO;
import com.jeasy.resource.dto.ResourceListResDTO;
import com.jeasy.resource.dto.ResourceModifyReqDTO;
import com.jeasy.resource.dto.ResourceShowResDTO;
import com.jeasy.resource.dto.UserMenuOperationDTO;
import com.jeasy.resource.dto.UserMenuResourceDTO;
import com.jeasy.resource.service.ResourceService;
import com.jeasy.web.resource.mapper.ResourceVOMapper;
import com.jeasy.web.resource.vo.ResourceAddReqVO;
import com.jeasy.web.resource.vo.ResourceListReqVO;
import com.jeasy.web.resource.vo.ResourceListResVO;
import com.jeasy.web.resource.vo.ResourceModifyReqVO;
import com.jeasy.web.resource.vo.ResourceShowResVO;
import com.jeasy.web.resource.vo.UserMenuOperationVO;
import com.jeasy.web.resource.vo.UserMenuResourceVO;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 菜单 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class ResourceController extends BaseController<ResourceService, ResourceVOMapper> {

    @RequiresPermissions(value = {"YHGL:CDZY:CX"})
    @MethodDoc(items = ResourceListResVO.class, desc = {"PC端", "菜单-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "resource/list", method = {RequestMethod.GET})
    public void list(final @FromJson ResourceListReqVO resourceListReqVO) {
        ResourceListReqDTO resourceListReqDTO = mapper.convertToResourceListReqDTO(resourceListReqVO);

        List<ResourceListResDTO> resourceListResDTOList = service.list(resourceListReqDTO);
        List<ResourceListResVO> items = mapper.convertToResourceListResVOList(resourceListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"YHGL:CDZY:XZ"})
    @MethodDoc(desc = {"PC端", "菜单-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "resource/add", method = {RequestMethod.POST})
    public void add(final @FromJson ResourceAddReqVO resourceAddReqVO) {
        ResourceAddReqDTO resourceAddReqDTO = mapper.convertToResourceAddReqDTO(resourceAddReqVO);

        Boolean isSuccess = service.add(resourceAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:CDZY:CK"})
    @MethodDoc(item = ResourceShowResVO.class, desc = {"PC端", "菜单-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "resource/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        ResourceShowResDTO resourceShowResDTO = service.show(id);

        ResourceShowResVO item = mapper.convertToResourceShowResVO(resourceShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"YHGL:CDZY:BJ"})
    @MethodDoc(desc = {"PC端", "菜单-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "resource/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson ResourceModifyReqVO resourceModifyReqVO) {
        ResourceModifyReqDTO resourceModifyReqDTO = mapper.convertToResourceModifyReqDTO(resourceModifyReqVO);

        Boolean isSuccess = service.modify(resourceModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"YHGL:CDZY:SC"})
    @MethodDoc(desc = {"PC端", "菜单-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "resource/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(items = UserMenuResourceVO.class, desc = {"5.登录管理", "1.权限管理", "1.权限资源-获取菜单资源集合"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "resource/listUserMenu", method = {RequestMethod.GET})
    public void listUserMenu() {
        List<UserMenuResourceDTO> userMenuResourceDTOList = service.listUserMenu();

        List<UserMenuResourceVO> items = mapper.convertToUserMenuResourceVOList(userMenuResourceDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = UserMenuOperationVO.class, desc = {"5.登录管理", "1.权限管理", "2.权限资源-根据菜单ID,获取菜单页面操作集合"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/03/28 15:13")
    @RequestMapping(value = "resource/listUserMenuOperation", method = {RequestMethod.GET})
    public void listUserMenuOperation(final @InitField(name = "menuPath", value = "/XXXX", desc = "菜单路径") @FromJson @ValidateNotNull(message = "菜单路径不允许为空") String menuPath) {
        List<UserMenuOperationDTO> userMenuOperationDTOList = service.listUserMenuOperation(menuPath);

        List<UserMenuOperationVO> items = mapper.convertToUserMenuOperationVOList(userMenuOperationDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }
}
