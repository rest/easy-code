package com.jeasy.web.resource.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.resource.dto.ResourceAddReqDTO;
import com.jeasy.resource.dto.ResourceDTO;
import com.jeasy.resource.dto.ResourceListReqDTO;
import com.jeasy.resource.dto.ResourceListResDTO;
import com.jeasy.resource.dto.ResourceModifyReqDTO;
import com.jeasy.resource.dto.ResourcePageReqDTO;
import com.jeasy.resource.dto.ResourcePageResDTO;
import com.jeasy.resource.dto.ResourceRemoveReqDTO;
import com.jeasy.resource.dto.ResourceShowResDTO;
import com.jeasy.resource.dto.UserMenuOperationDTO;
import com.jeasy.resource.dto.UserMenuResourceDTO;
import com.jeasy.web.resource.vo.ResourceAddReqVO;
import com.jeasy.web.resource.vo.ResourceListReqVO;
import com.jeasy.web.resource.vo.ResourceListResVO;
import com.jeasy.web.resource.vo.ResourceModifyReqVO;
import com.jeasy.web.resource.vo.ResourcePageReqVO;
import com.jeasy.web.resource.vo.ResourcePageResVO;
import com.jeasy.web.resource.vo.ResourceRemoveReqVO;
import com.jeasy.web.resource.vo.ResourceShowResVO;
import com.jeasy.web.resource.vo.ResourceVO;
import com.jeasy.web.resource.vo.UserMenuOperationVO;
import com.jeasy.web.resource.vo.UserMenuResourceVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface ResourceVOMapper extends BaseVOMapper<ResourceDTO, ResourceVO> {

    default ResourceVOMapper me() {
        return SpringUtil.getBean(ResourceVOMapper.class);
    }

    ResourceAddReqDTO convertToResourceAddReqDTO(ResourceAddReqVO resourceAddReqVO);

    ResourceShowResVO convertToResourceShowResVO(ResourceShowResDTO resourceShowResDTO);

    ResourceModifyReqDTO convertToResourceModifyReqDTO(ResourceModifyReqVO resourceModifyReqVO);

    ResourceRemoveReqDTO convertToResourceRemoveReqDTO(ResourceRemoveReqVO resourceRemoveReqVO);

    ResourceListReqDTO convertToResourceListReqDTO(ResourceListReqVO resourceReqVO);

    ResourcePageReqDTO convertToResourcePageReqDTO(ResourcePageReqVO resourcePageReqVO);

    ResourceListResVO convertToResourceListResVO(ResourceListResDTO resourceListResDTO);

    List<ResourceListResVO> convertToResourceListResVOList(List<ResourceListResDTO> resourceListResDTOList);

    Page<ResourcePageResVO> convertToResourcePageResVOPage(Page<ResourcePageResDTO> resourcePageResDTOPage);

    List<ResourceAddReqDTO> convertToResourceAddReqDTOList(List<ResourceAddReqVO> resourceAddReqVOList);

    List<ResourceShowResVO> convertToResourceShowResVOList(List<ResourceShowResDTO> resourceShowResDTOList);

    List<UserMenuResourceVO> convertToUserMenuResourceVOList(List<UserMenuResourceDTO> userMenuResourceDTOList);

    List<UserMenuOperationVO> convertToUserMenuOperationVOList(List<UserMenuOperationDTO> userMenuOperationDTOList);
}
