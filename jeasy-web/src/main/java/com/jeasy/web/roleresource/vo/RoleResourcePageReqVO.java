package com.jeasy.web.roleresource.vo;

import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.validate.AnnotationValidable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色资源 分页 ReqVO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleResourcePageReqVO implements AnnotationValidable, Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 主键
     */
    @InitField(value = "", desc = "主键")
    private String id;

    /**
     * 角色ID
     */
    @InitField(value = "", desc = "角色ID")
    private String roleId;

    /**
     * 角色名称
     */
    @InitField(value = "", desc = "角色名称")
    private String roleName;

    /**
     * 角色编码
     */
    @InitField(value = "", desc = "角色编码")
    private String roleCode;

    /**
     * 资源ID
     */
    @InitField(value = "", desc = "资源ID")
    private String resourceId;

    /**
     * 资源名称
     */
    @InitField(value = "", desc = "资源名称")
    private String resourceName;

    /**
     * 资源编码
     */
    @InitField(value = "", desc = "资源编码")
    private String resourceCode;

    /**
     * 创建时间
     */
    @InitField(value = "", desc = "创建时间")
    private Long createAt;

    /**
     * 创建人ID
     */
    @InitField(value = "", desc = "创建人ID")
    private String createBy;

    /**
     * 创建人名称
     */
    @InitField(value = "", desc = "创建人名称")
    private String createName;

    /**
     * 更新时间
     */
    @InitField(value = "", desc = "更新时间")
    private Long updateAt;

    /**
     * 更新人ID
     */
    @InitField(value = "", desc = "更新人ID")
    private String updateBy;

    /**
     * 更新人名称
     */
    @InitField(value = "", desc = "更新人名称")
    private String updateName;

    /**
     * 是否删除
     */
    @InitField(value = "", desc = "是否删除")
    private Integer isDel;

    /**
     * 是否测试
     */
    @InitField(value = "", desc = "是否测试")
    private Integer isTest;

}
