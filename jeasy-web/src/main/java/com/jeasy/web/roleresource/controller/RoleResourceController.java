package com.jeasy.web.roleresource.controller;

import com.jeasy.roleresource.dto.RoleResourceAddReqDTO;
import com.jeasy.roleresource.dto.RoleResourceListReqDTO;
import com.jeasy.roleresource.dto.RoleResourceListResDTO;
import com.jeasy.roleresource.dto.RoleResourceModifyReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageResDTO;
import com.jeasy.roleresource.dto.RoleResourceRemoveReqDTO;
import com.jeasy.roleresource.dto.RoleResourceShowResDTO;
import com.jeasy.roleresource.service.RoleResourceService;
import com.jeasy.web.roleresource.mapper.RoleResourceVOMapper;
import com.jeasy.web.roleresource.vo.RoleResourceAddReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceListReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceListResVO;
import com.jeasy.web.roleresource.vo.RoleResourceModifyReqVO;
import com.jeasy.web.roleresource.vo.RoleResourcePageReqVO;
import com.jeasy.web.roleresource.vo.RoleResourcePageResVO;
import com.jeasy.web.roleresource.vo.RoleResourceRemoveReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色资源 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@RestController
public class RoleResourceController extends BaseController<RoleResourceService, RoleResourceVOMapper> {

    @MethodDoc(items = RoleResourceListResVO.class, desc = {"PC端", "角色资源-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/list", method = {RequestMethod.GET})
    public void list(final @FromJson RoleResourceListReqVO roleResourceListReqVO) {
        RoleResourceListReqDTO roleResourceListReqDTO = mapper.convertToRoleResourceListReqDTO(roleResourceListReqVO);

        List<RoleResourceListResDTO> roleResourceListResDTOList = service.list(roleResourceListReqDTO);
        List<RoleResourceListResVO> items = mapper.convertToRoleResourceListResVOList(roleResourceListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleResourceListResVO.class, desc = {"PC端", "角色资源-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson RoleResourceListReqVO roleResourceListReqVO) {
        RoleResourceListReqDTO roleResourceListReqDTO = mapper.convertToRoleResourceListReqDTO(roleResourceListReqVO);

        List<RoleResourceListResDTO> roleResourceListResDTOList = service.listByVersion1(roleResourceListReqDTO);
        List<RoleResourceListResVO> items = mapper.convertToRoleResourceListResVOList(roleResourceListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleResourceListResVO.class, desc = {"PC端", "角色资源-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson RoleResourceListReqVO roleResourceListReqVO) {
        RoleResourceListReqDTO roleResourceListReqDTO = mapper.convertToRoleResourceListReqDTO(roleResourceListReqVO);

        List<RoleResourceListResDTO> roleResourceListResDTOList = service.listByVersion2(roleResourceListReqDTO);
        List<RoleResourceListResVO> items = mapper.convertToRoleResourceListResVOList(roleResourceListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = RoleResourceListResVO.class, desc = {"PC端", "角色资源-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson RoleResourceListReqVO roleResourceListReqVO) {
        RoleResourceListReqDTO roleResourceListReqDTO = mapper.convertToRoleResourceListReqDTO(roleResourceListReqVO);

        List<RoleResourceListResDTO> roleResourceListResDTOList = service.listByVersion3(roleResourceListReqDTO);
        List<RoleResourceListResVO> items = mapper.convertToRoleResourceListResVOList(roleResourceListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = RoleResourceListResVO.class, desc = {"PC端", "角色资源-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson RoleResourceListReqVO roleResourceListReqVO) {
        RoleResourceListReqDTO roleResourceListReqDTO = mapper.convertToRoleResourceListReqDTO(roleResourceListReqVO);

        RoleResourceListResDTO roleResourceListResDTO = service.listOne(roleResourceListReqDTO);
        RoleResourceListResVO item = mapper.convertToRoleResourceListResVO(roleResourceListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(pages = RoleResourcePageResVO.class, desc = {"PC端", "角色资源-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/page", method = {RequestMethod.GET})
    public void page(final @FromJson RoleResourcePageReqVO roleResourcePageReqVO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        RoleResourcePageReqDTO roleResourcePageReqDTO = mapper.convertToRoleResourcePageReqDTO(roleResourcePageReqVO);

        Page<RoleResourcePageResDTO> roleResourcePageResDTOPage = service.pagination(roleResourcePageReqDTO, current, size);
        Page<RoleResourcePageResVO> roleResourcePageResVOPage = mapper.convertToRoleResourcePageResVOPage(roleResourcePageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, roleResourcePageResVOPage.getTotal(), roleResourcePageResVOPage.getRecords(), size, current);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/add", method = {RequestMethod.POST})
    public void add(final @FromJson RoleResourceAddReqVO roleResourceAddReqVO) {
        RoleResourceAddReqDTO roleResourceAddReqDTO = mapper.convertToRoleResourceAddReqDTO(roleResourceAddReqVO);

        Boolean isSuccess = service.add(roleResourceAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson RoleResourceAddReqVO roleResourceAddReqVO) {
        RoleResourceAddReqDTO roleResourceAddReqDTO = mapper.convertToRoleResourceAddReqDTO(roleResourceAddReqVO);

        Boolean isSuccess = service.addAllColumn(roleResourceAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<RoleResourceAddReqVO> roleResourceAddReqVOList) {
        List<RoleResourceAddReqDTO> roleResourceAddReqDTOList = mapper.convertToRoleResourceAddReqDTOList(roleResourceAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(roleResourceAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(item = RoleResourceShowResVO.class, desc = {"PC端", "角色资源-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        RoleResourceShowResDTO roleResourceShowResDTO = service.show(id);

        RoleResourceShowResVO item = mapper.convertToRoleResourceShowResVO(roleResourceShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = RoleResourceShowResVO.class, desc = {"PC端", "角色资源-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<RoleResourceShowResDTO> roleResourceShowResDTOList = service.showByIds(ids);

        List<RoleResourceShowResVO> items = mapper.convertToRoleResourceShowResVOList(roleResourceShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "角色资源-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson RoleResourceModifyReqVO roleResourceModifyReqVO) {
        RoleResourceModifyReqDTO roleResourceModifyReqDTO = mapper.convertToRoleResourceModifyReqDTO(roleResourceModifyReqVO);

        Boolean isSuccess = service.modify(roleResourceModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson RoleResourceModifyReqVO roleResourceModifyReqVO) {
        RoleResourceModifyReqDTO roleResourceModifyReqDTO = mapper.convertToRoleResourceModifyReqDTO(roleResourceModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(roleResourceModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleResource/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson RoleResourceRemoveReqVO roleResourceRemoveReqVO) {
        RoleResourceRemoveReqDTO roleResourceRemoveReqDTO = mapper.convertToRoleResourceRemoveReqDTO(roleResourceRemoveReqVO);

        Boolean isSuccess = service.removeByParams(roleResourceRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
