package com.jeasy.web.roleresource.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.roleresource.dto.RoleResourceAddReqDTO;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.jeasy.roleresource.dto.RoleResourceListReqDTO;
import com.jeasy.roleresource.dto.RoleResourceListResDTO;
import com.jeasy.roleresource.dto.RoleResourceModifyReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageResDTO;
import com.jeasy.roleresource.dto.RoleResourceRemoveReqDTO;
import com.jeasy.roleresource.dto.RoleResourceShowResDTO;
import com.jeasy.web.roleresource.vo.RoleResourceAddReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceListReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceListResVO;
import com.jeasy.web.roleresource.vo.RoleResourceModifyReqVO;
import com.jeasy.web.roleresource.vo.RoleResourcePageReqVO;
import com.jeasy.web.roleresource.vo.RoleResourcePageResVO;
import com.jeasy.web.roleresource.vo.RoleResourceRemoveReqVO;
import com.jeasy.web.roleresource.vo.RoleResourceShowResVO;
import com.jeasy.web.roleresource.vo.RoleResourceVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface RoleResourceVOMapper extends BaseVOMapper<RoleResourceDTO, RoleResourceVO> {

    default RoleResourceVOMapper me() {
        return SpringUtil.getBean(RoleResourceVOMapper.class);
    }

    RoleResourceAddReqDTO convertToRoleResourceAddReqDTO(RoleResourceAddReqVO roleResourceAddReqVO);

    RoleResourceShowResVO convertToRoleResourceShowResVO(RoleResourceShowResDTO roleResourceShowResDTO);

    RoleResourceModifyReqDTO convertToRoleResourceModifyReqDTO(RoleResourceModifyReqVO roleResourceModifyReqVO);

    RoleResourceRemoveReqDTO convertToRoleResourceRemoveReqDTO(RoleResourceRemoveReqVO roleResourceRemoveReqVO);

    RoleResourceListReqDTO convertToRoleResourceListReqDTO(RoleResourceListReqVO roleResourceReqVO);

    RoleResourcePageReqDTO convertToRoleResourcePageReqDTO(RoleResourcePageReqVO roleResourcePageReqVO);

    RoleResourceListResVO convertToRoleResourceListResVO(RoleResourceListResDTO roleResourceListResDTO);

    List<RoleResourceListResVO> convertToRoleResourceListResVOList(List<RoleResourceListResDTO> roleResourceListResDTOList);

    Page<RoleResourcePageResVO> convertToRoleResourcePageResVOPage(Page<RoleResourcePageResDTO> roleResourcePageResDTOPage);

    List<RoleResourceAddReqDTO> convertToRoleResourceAddReqDTOList(List<RoleResourceAddReqVO> roleResourceAddReqVOList);

    List<RoleResourceShowResVO> convertToRoleResourceShowResVOList(List<RoleResourceShowResDTO> roleResourceShowResDTOList);
}
