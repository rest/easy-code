package com.jeasy.web.log.controller;

import com.jeasy.log.dto.LogAddReqDTO;
import com.jeasy.log.dto.LogListReqDTO;
import com.jeasy.log.dto.LogListResDTO;
import com.jeasy.log.dto.LogModifyReqDTO;
import com.jeasy.log.dto.LogPageReqDTO;
import com.jeasy.log.dto.LogPageResDTO;
import com.jeasy.log.dto.LogRemoveReqDTO;
import com.jeasy.log.dto.LogShowResDTO;
import com.jeasy.log.service.LogService;
import com.jeasy.web.log.mapper.LogVOMapper;
import com.jeasy.web.log.vo.LogAddReqVO;
import com.jeasy.web.log.vo.LogListReqVO;
import com.jeasy.web.log.vo.LogListResVO;
import com.jeasy.web.log.vo.LogModifyReqVO;
import com.jeasy.web.log.vo.LogPageReqVO;
import com.jeasy.web.log.vo.LogPageResVO;
import com.jeasy.web.log.vo.LogRemoveReqVO;
import com.jeasy.web.log.vo.LogShowResVO;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.InitField;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import com.javabooter.core.validate.handler.ValidateNotNull;
import com.javabooter.core.web.controller.BaseController;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 日志 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@RestController
public class LogController extends BaseController<LogService, LogVOMapper> {

    @MethodDoc(items = LogListResVO.class, desc = {"PC端", "日志-列表查询", "列表查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/list", method = {RequestMethod.GET})
    public void list(final @FromJson LogListReqVO logListReqVO) {
        LogListReqDTO logListReqDTO = mapper.convertToLogListReqDTO(logListReqVO);

        List<LogListResDTO> logListResDTOList = service.list(logListReqDTO);
        List<LogListResVO> items = mapper.convertToLogListResVOList(logListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = LogListResVO.class, desc = {"PC端", "日志-列表查询", "列表查询1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    public void listByVersion1(final @FromJson LogListReqVO logListReqVO) {
        LogListReqDTO logListReqDTO = mapper.convertToLogListReqDTO(logListReqVO);

        List<LogListResDTO> logListResDTOList = service.listByVersion1(logListReqDTO);
        List<LogListResVO> items = mapper.convertToLogListResVOList(logListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = LogListResVO.class, desc = {"PC端", "日志-列表查询", "列表查询1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    public void listByVersion2(final @FromJson LogListReqVO logListReqVO) {
        LogListReqDTO logListReqDTO = mapper.convertToLogListReqDTO(logListReqVO);

        List<LogListResDTO> logListResDTOList = service.listByVersion2(logListReqDTO);
        List<LogListResVO> items = mapper.convertToLogListResVOList(logListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(items = LogListResVO.class, desc = {"PC端", "日志-列表查询", "列表查询1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    public void listByVersion3(final @FromJson LogListReqVO logListReqVO) {
        LogListReqDTO logListReqDTO = mapper.convertToLogListReqDTO(logListReqVO);

        List<LogListResDTO> logListResDTOList = service.listByVersion3(logListReqDTO);
        List<LogListResVO> items = mapper.convertToLogListResVOList(logListResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @MethodDoc(item = LogListResVO.class, desc = {"PC端", "日志-列表查询", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/listOne", method = {RequestMethod.GET})
    public void listOne(final @FromJson LogListReqVO logListReqVO) {
        LogListReqDTO logListReqDTO = mapper.convertToLogListReqDTO(logListReqVO);

        LogListResDTO logListResDTO = service.listOne(logListReqDTO);
        LogListResVO item = mapper.convertToLogListResVO(logListResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @RequiresPermissions(value = {"RZJK:CZRZ:CX"})
    @MethodDoc(pages = LogPageResVO.class, desc = {"PC端", "日志-分页查询", "分页查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/page", method = {RequestMethod.GET})
    public void page(final @FromJson LogPageReqVO logPageReqVO,
        final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
        final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int current = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int size = Func.isNullOrZero(pageSize) ? 10 : pageSize;
        LogPageReqDTO logPageReqDTO = mapper.convertToLogPageReqDTO(logPageReqVO);

        Page<LogPageResDTO> logPageResDTOPage = service.pagination(logPageReqDTO, current, size);
        Page<LogPageResVO> logPageResVOPage = mapper.convertToLogPageResVOPage(logPageResDTOPage);

        responsePage(CommonInfoEnum.SUCCESS, logPageResVOPage.getTotal(), logPageResVOPage.getRecords(), size, current);
    }

    @RequiresPermissions(value = {"RZJK:CZRZ:XZ"})
    @MethodDoc(desc = {"PC端", "日志-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/add", method = {RequestMethod.POST})
    public void add(final @FromJson LogAddReqVO logAddReqVO) {
        LogAddReqDTO logAddReqDTO = mapper.convertToLogAddReqDTO(logAddReqVO);

        Boolean isSuccess = service.add(logAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "日志-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/addAllColumn", method = {RequestMethod.POST})
    public void addAllColumn(final @FromJson LogAddReqVO logAddReqVO) {
        LogAddReqDTO logAddReqDTO = mapper.convertToLogAddReqDTO(logAddReqVO);

        Boolean isSuccess = service.addAllColumn(logAddReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "日志-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/addBatchAllColumn", method = {RequestMethod.POST})
    public void addBatchAllColumn(final @FromJson List<LogAddReqVO> logAddReqVOList) {
        List<LogAddReqDTO> logAddReqDTOList = mapper.convertToLogAddReqDTOList(logAddReqVOList);

        Boolean isSuccess = service.addBatchAllColumn(logAddReqDTOList);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"RZJK:CZRZ:CK"})
    @MethodDoc(item = LogShowResVO.class, desc = {"PC端", "日志-查看", "查看"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/show", method = {RequestMethod.GET})
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        LogShowResDTO logShowResDTO = service.show(id);

        LogShowResVO item = mapper.convertToLogShowResVO(logShowResDTO);

        responseItem(CommonInfoEnum.SUCCESS, item);
    }

    @MethodDoc(item = LogShowResVO.class, desc = {"PC端", "日志-查看", "查看(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/showByIds", method = {RequestMethod.GET})
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        List<LogShowResDTO> logShowResDTOList = service.showByIds(ids);

        List<LogShowResVO> items = mapper.convertToLogShowResVOList(logShowResDTOList);

        responseItems(CommonInfoEnum.SUCCESS, items);
    }

    @RequiresPermissions(value = {"RZJK:CZRZ:BJ"})
    @MethodDoc(desc = {"PC端", "日志-编辑", "编辑"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/modify", method = {RequestMethod.POST})
    public void modify(final @FromJson LogModifyReqVO logModifyReqVO) {
        LogModifyReqDTO logModifyReqDTO = mapper.convertToLogModifyReqDTO(logModifyReqVO);

        Boolean isSuccess = service.modify(logModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "日志-编辑", "编辑(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/modifySelective", method = {RequestMethod.POST})
    public void modifyAllColumn(final @FromJson LogModifyReqVO logModifyReqVO) {
        LogModifyReqDTO logModifyReqDTO = mapper.convertToLogModifyReqDTO(logModifyReqVO);

        Boolean isSuccess = service.modifyAllColumn(logModifyReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @RequiresPermissions(value = {"XXXX:XXXX:SC"})
    @MethodDoc(desc = {"PC端", "日志-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/remove", method = {RequestMethod.POST})
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") String id) {
        Boolean isSuccess = service.remove(id);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "日志-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/removeBatch", method = {RequestMethod.POST})
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<String> ids) {
        Boolean isSuccess = service.removeBatch(ids);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }

    @MethodDoc(desc = {"PC端", "日志-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "log/removeByParams", method = {RequestMethod.POST})
    public void removeByParams(final @FromJson LogRemoveReqVO logRemoveReqVO) {
        LogRemoveReqDTO logRemoveReqDTO = mapper.convertToLogRemoveReqDTO(logRemoveReqVO);

        Boolean isSuccess = service.removeByParams(logRemoveReqDTO);

        responseMessage(isSuccess ? CommonInfoEnum.SUCCESS : CommonInfoEnum.INTERNAL_ERROR);
    }
}
