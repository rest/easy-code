package com.jeasy.web.log.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.log.dto.LogAddReqDTO;
import com.jeasy.log.dto.LogDTO;
import com.jeasy.log.dto.LogListReqDTO;
import com.jeasy.log.dto.LogListResDTO;
import com.jeasy.log.dto.LogModifyReqDTO;
import com.jeasy.log.dto.LogPageReqDTO;
import com.jeasy.log.dto.LogPageResDTO;
import com.jeasy.log.dto.LogRemoveReqDTO;
import com.jeasy.log.dto.LogShowResDTO;
import com.jeasy.web.log.vo.LogAddReqVO;
import com.jeasy.web.log.vo.LogListReqVO;
import com.jeasy.web.log.vo.LogListResVO;
import com.jeasy.web.log.vo.LogModifyReqVO;
import com.jeasy.web.log.vo.LogPageReqVO;
import com.jeasy.web.log.vo.LogPageResVO;
import com.jeasy.web.log.vo.LogRemoveReqVO;
import com.jeasy.web.log.vo.LogShowResVO;
import com.jeasy.web.log.vo.LogVO;
import com.javabooter.core.mapper.BaseVOMapper;
import com.javabooter.core.mapper.BaseVOMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseVOMapperConfig.class)
public interface LogVOMapper extends BaseVOMapper<LogDTO, LogVO> {

    default LogVOMapper me() {
        return SpringUtil.getBean(LogVOMapper.class);
    }

    LogAddReqDTO convertToLogAddReqDTO(LogAddReqVO logAddReqVO);

    LogShowResVO convertToLogShowResVO(LogShowResDTO logShowResDTO);

    LogModifyReqDTO convertToLogModifyReqDTO(LogModifyReqVO logModifyReqVO);

    LogRemoveReqDTO convertToLogRemoveReqDTO(LogRemoveReqVO logRemoveReqVO);

    LogListReqDTO convertToLogListReqDTO(LogListReqVO logReqVO);

    LogPageReqDTO convertToLogPageReqDTO(LogPageReqVO logPageReqVO);

    LogListResVO convertToLogListResVO(LogListResDTO logListResDTO);

    List<LogListResVO> convertToLogListResVOList(List<LogListResDTO> logListResDTOList);

    Page<LogPageResVO> convertToLogPageResVOPage(Page<LogPageResDTO> logPageResDTOPage);

    List<LogAddReqDTO> convertToLogAddReqDTOList(List<LogAddReqVO> logAddReqVOList);

    List<LogShowResVO> convertToLogShowResVOList(List<LogShowResDTO> logShowResDTOList);
}
