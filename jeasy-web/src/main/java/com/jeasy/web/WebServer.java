package com.jeasy.web;

import com.jeasy.dao.configuration.DaoConfiguration;
import com.jeasy.scheduler.configuration.SchedulerConfiguration;
import com.jeasy.service.configuration.ServiceConfiguration;
import com.jeasy.shiro.configuration.ShiroConfiguration;
import com.jeasy.web.configuration.WebConfiguration;
import com.javabooter.common.configuration.CommonConfiguration;
import com.javabooter.core.configuration.CoreConfiguration;
import com.javabooter.common.configuration.RedisConfiguration;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import java.util.Map;

/**
 * <pre>
 * 启动类
 * </pre>
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/2/7 上午9:43
 */
@SpringBootApplication
@Import({
    WebConfiguration.class, ShiroConfiguration.class, ServiceConfiguration.class, SchedulerConfiguration.class,
    DaoConfiguration.class, CommonConfiguration.class, RedisConfiguration.class, CoreConfiguration.class})
public class WebServer {

    private static final String SERVER_PORT_KEY = "server.port";

    public static void start(String[] args) {
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder(WebServer.class);

        // springApplicationBuilder.parent(CoreConfiguration.class, RedisConfiguration.class);

        Map<String, Object> properties = new HashedMap<>();
        properties.put(SERVER_PORT_KEY, 8080);

        springApplicationBuilder.properties(properties).run(args);
    }
}
