package com.jeasy.server;

import com.jeasy.web.WebServer;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * 启动类
 * </pre>
 *
 * @author TaoBangren
 * @version 1.0.0
 * @since 2020/2/7 上午9:43
 */
@Slf4j
public class BootStrap {
    public static void main(String[] args) {
        WebServer.start(args);
    }
}
