package com.jeasy.service.configuration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.javabooter.common.datasource.DataSourceRoutingAspectProcessor;
import com.javabooter.common.domain.DomainCostLogAspect;
import com.javabooter.common.manager.ManagerCostLogAspect;
import com.javabooter.common.service.ServiceCostLogAspect;
import com.javabooter.core.exception.MessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource;
import org.springframework.transaction.interceptor.NoRollbackRuleAttribute;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.RuleBasedTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@ComponentScan(basePackages = {"com.jeasy.*.service", "com.jeasy.*.manager", "com.jeasy.*.mapper", "com.jeasy.*.domain"})
public class ServiceConfiguration {

    private TransactionAttributeSource transactionAttributeSource() {
        NameMatchTransactionAttributeSource nameMatchTransactionAttributeSource =
            new NameMatchTransactionAttributeSource();
        // 只读事务，不做更新操作
        RuleBasedTransactionAttribute readOnlyTx = new RuleBasedTransactionAttribute();
        readOnlyTx.setReadOnly(true);
        readOnlyTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_SUPPORTS);

        // 当前存在事务就使用当前事务，当前不存在事务就创建一个新的事务
        List<RollbackRuleAttribute> rollbackRuleAttributes = Lists.newArrayList();
        rollbackRuleAttributes.add(new RollbackRuleAttribute(Exception.class));
        rollbackRuleAttributes.add(new NoRollbackRuleAttribute(MessageException.class));

        RuleBasedTransactionAttribute requiredTx =
            new RuleBasedTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRED, rollbackRuleAttributes);
        requiredTx.setTimeout(5);

        Map<String, TransactionAttribute> transactionAttributeMap = Maps.newHashMap();
        transactionAttributeMap.put("*remove*", requiredTx);
        transactionAttributeMap.put("*save*", requiredTx);
        transactionAttributeMap.put("*modify*", requiredTx);
        transactionAttributeMap.put("*create*", requiredTx);
        transactionAttributeMap.put("*delete*", requiredTx);
        transactionAttributeMap.put("*update*", requiredTx);
        transactionAttributeMap.put("*add*", requiredTx);

        transactionAttributeMap.put("*find*", readOnlyTx);
        transactionAttributeMap.put("*get*", readOnlyTx);
        transactionAttributeMap.put("*page*", readOnlyTx);
        transactionAttributeMap.put("*count*", readOnlyTx);
        transactionAttributeMap.put("*select*", readOnlyTx);
        transactionAttributeMap.put("*query*", readOnlyTx);
        transactionAttributeMap.put("*list*", readOnlyTx);
        transactionAttributeMap.put("*show*", readOnlyTx);

        nameMatchTransactionAttributeSource.setNameMap(transactionAttributeMap);
        return nameMatchTransactionAttributeSource;
    }

    /**
     * 事务切面
     *
     * @param dataSource
     * @return
     */
    @Bean("useTxAdvice")
    public TransactionInterceptor getTransactionInterceptor(DataSource dataSource) {
        return new TransactionInterceptor(new DataSourceTransactionManager(dataSource), transactionAttributeSource());
    }

    /**
     * 切面拦截规则 参数会自动从容器中注入
     *
     * @param useTxAdvice
     * @return
     */
    @Bean
    public AspectJExpressionPointcutAdvisor aspectJExpressionPointcutAdvisor(TransactionInterceptor useTxAdvice) {
        AspectJExpressionPointcutAdvisor pointcutAdvisor = new AspectJExpressionPointcutAdvisor();
        pointcutAdvisor.setAdvice(useTxAdvice);
        pointcutAdvisor.setExpression("execution(public * com..manager.*.*(..))");
        return pointcutAdvisor;
    }

    /**
     * 切面配置: Manager层多数据源切换支持,在事务之前执行
     *
     * @return
     */
    @Bean
    public DataSourceRoutingAspectProcessor dataSourceRoutingAspectProcessor() {
        return new DataSourceRoutingAspectProcessor();
    }

    /**
     * 切面配置：Service层方法执行日志
     *
     * @return
     */
    @Bean
    public ServiceCostLogAspect serviceCostLogAspect() {
        return new ServiceCostLogAspect();
    }

    /**
     * 切面配置：Manager层方法执行日志
     *
     * @return
     */
    @Bean
    public ManagerCostLogAspect managerCostLogAspect() {
        return new ManagerCostLogAspect();
    }

    /**
     * 切面配置：Domain层方法执行日志
     *
     * @return
     */
    @Bean
    public DomainCostLogAspect domainCostLogAspect() {
        return new DomainCostLogAspect();
    }
}
