package com.jeasy.userorg.domain;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userorg.dto.UserOrgAddReqDTO;
import com.jeasy.userorg.dto.UserOrgDTO;
import com.jeasy.userorg.dto.UserOrgListReqDTO;
import com.jeasy.userorg.dto.UserOrgListResDTO;
import com.jeasy.userorg.dto.UserOrgModifyReqDTO;
import com.jeasy.userorg.dto.UserOrgPageReqDTO;
import com.jeasy.userorg.dto.UserOrgPageResDTO;
import com.jeasy.userorg.dto.UserOrgRemoveReqDTO;
import com.jeasy.userorg.dto.UserOrgShowResDTO;
import com.jeasy.userorg.mapper.UserOrgMapper;
import com.jeasy.userorg.po.UserOrgPO;
import com.javabooter.core.Func;
import com.javabooter.core.domain.BaseDO;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户机构 DO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class UserOrgDO extends BaseDO<UserOrgDTO, UserOrgPO, UserOrgMapper> {

    public static UserOrgDO me() {
        return SpringUtil.getBean(UserOrgDO.class);
    }

    public void checkUserOrgAddReqDTO(final UserOrgAddReqDTO userOrgAddReqDTO) {
        if (Func.isEmpty(userOrgAddReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkUserOrgAddReqDTOList(final List<UserOrgAddReqDTO> userOrgAddReqDTOList) {
        if (Func.isEmpty(userOrgAddReqDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkIds(final List<String> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "集合不能为空且大小大于0");
        }
    }

    public void checkUserOrgModifyReqDTO(final UserOrgModifyReqDTO userOrgModifyReqDTO) {
        if (Func.isEmpty(userOrgModifyReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkUserOrgRemoveReqDTO(final UserOrgRemoveReqDTO userOrgRemoveReqDTO) {
        if (Func.isEmpty(userOrgRemoveReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public UserOrgDTO buildListParamsDTO(final UserOrgListReqDTO userOrgListReqDTO) {
        return mapper.convertToUserOrgDTO(userOrgListReqDTO);
    }

    public UserOrgDTO buildPageParamsDTO(final UserOrgPageReqDTO userOrgPageReqDTO) {
        return mapper.convertToUserOrgDTO(userOrgPageReqDTO);
    }

    public UserOrgDTO buildAddUserOrgDTO(final UserOrgAddReqDTO userOrgAddReqDTO) {
        return mapper.convertToUserOrgDTO(userOrgAddReqDTO);
    }

    public List<UserOrgDTO> buildAddBatchUserOrgDTOList(final List<UserOrgAddReqDTO> userOrgAddReqDTOList) {
        return mapper.convertToUserOrgDTOList(userOrgAddReqDTOList);
    }

    public UserOrgDTO buildModifyUserOrgDTO(final UserOrgModifyReqDTO userOrgModifyReqDTO) {
        return mapper.convertToUserOrgDTO(userOrgModifyReqDTO);
    }

    public UserOrgDTO buildRemoveUserOrgDTO(final UserOrgRemoveReqDTO userOrgRemoveReqDTO) {
        return mapper.convertToUserOrgDTO(userOrgRemoveReqDTO);
    }

    public List<UserOrgListResDTO> transferUserOrgListResDTOList(final List<UserOrgDTO> userOrgDTOList) {
        if (Func.isEmpty(userOrgDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserOrgListResDTOList(userOrgDTOList);
    }

    public UserOrgListResDTO transferUserOrgListResDTO(final UserOrgDTO userOrgDTO) {
        if (Func.isEmpty(userOrgDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserOrgListResDTO(userOrgDTO);
    }

    public Page<UserOrgPageResDTO> transferUserOrgPageResDTOPage(final Page<UserOrgDTO> userOrgDTOPage) {
        if (Func.isEmpty(userOrgDTOPage) || Func.isEmpty(userOrgDTOPage.getRecords())) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserOrgPageResDTOPage(userOrgDTOPage);
    }

    public UserOrgShowResDTO transferUserOrgShowResDTO(final UserOrgDTO userOrgDTO) {
        if (Func.isEmpty(userOrgDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserOrgShowResDTO(userOrgDTO);
    }

    public List<UserOrgShowResDTO> transferUserOrgShowResDTOList(final List<UserOrgDTO> userOrgDTOList) {
        if (Func.isEmpty(userOrgDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserOrgShowResDTOList(userOrgDTOList);
    }
}
