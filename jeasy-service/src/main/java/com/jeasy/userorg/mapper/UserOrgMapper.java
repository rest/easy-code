package com.jeasy.userorg.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userorg.dto.UserOrgAddReqDTO;
import com.jeasy.userorg.dto.UserOrgDTO;
import com.jeasy.userorg.dto.UserOrgListReqDTO;
import com.jeasy.userorg.dto.UserOrgListResDTO;
import com.jeasy.userorg.dto.UserOrgModifyReqDTO;
import com.jeasy.userorg.dto.UserOrgPageReqDTO;
import com.jeasy.userorg.dto.UserOrgPageResDTO;
import com.jeasy.userorg.dto.UserOrgRemoveReqDTO;
import com.jeasy.userorg.dto.UserOrgShowResDTO;
import com.jeasy.userorg.po.UserOrgPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface UserOrgMapper extends BaseMapper<UserOrgDTO, UserOrgPO> {

    default UserOrgMapper me() {
        return SpringUtil.getBean(UserOrgMapper.class);
    }

    UserOrgDTO convertToUserOrgDTO(UserOrgAddReqDTO userOrgAddReqDTO);

    UserOrgDTO convertToUserOrgDTO(UserOrgModifyReqDTO userOrgModifyReqDTO);

    UserOrgDTO convertToUserOrgDTO(UserOrgRemoveReqDTO userOrgRemoveReqDTO);

    UserOrgDTO convertToUserOrgDTO(UserOrgListReqDTO userOrgListReqDTO);

    UserOrgDTO convertToUserOrgDTO(UserOrgPageReqDTO userOrgPageReqDTO);

    UserOrgShowResDTO convertToUserOrgShowResDTO(UserOrgDTO userOrgDTO);

    List<UserOrgShowResDTO> convertToUserOrgShowResDTOList(List<UserOrgDTO> userOrgDTOList);

    UserOrgListResDTO convertToUserOrgListResDTO(UserOrgDTO userOrgDTO);

    List<UserOrgListResDTO> convertToUserOrgListResDTOList(List<UserOrgDTO> userOrgDTOList);

    List<UserOrgDTO> convertToUserOrgDTOList(List<UserOrgAddReqDTO> userOrgAddReqDTOList);

    UserOrgPageResDTO convertToUserOrgPageResDTO(UserOrgDTO userOrgDTO);

    List<UserOrgPageResDTO> convertToUserOrgPageResDTOList(List<UserOrgDTO> userOrgDTOList);

    Page<UserOrgPageResDTO> convertToUserOrgPageResDTOPage(Page<UserOrgDTO> userOrgDTOPage);
}
