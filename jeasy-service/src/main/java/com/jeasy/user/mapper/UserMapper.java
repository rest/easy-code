package com.jeasy.user.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.role.dto.RoleDTO;
import com.jeasy.user.dto.UserAddReqDTO;
import com.jeasy.user.dto.UserDTO;
import com.jeasy.user.dto.UserListReqDTO;
import com.jeasy.user.dto.UserListResDTO;
import com.jeasy.user.dto.UserModifyReqDTO;
import com.jeasy.user.dto.UserPageReqDTO;
import com.jeasy.user.dto.UserPageResDTO;
import com.jeasy.user.dto.UserPageRoleResDTO;
import com.jeasy.user.dto.UserRemoveReqDTO;
import com.jeasy.user.dto.UserShowResDTO;
import com.jeasy.user.po.UserPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface UserMapper extends BaseMapper<UserDTO, UserPO> {

    default UserMapper me() {
        return SpringUtil.getBean(UserMapper.class);
    }

    UserDTO convertToUserDTO(UserAddReqDTO userAddReqDTO);

    UserDTO convertToUserDTO(UserModifyReqDTO userModifyReqDTO);

    UserDTO convertToUserDTO(UserRemoveReqDTO userRemoveReqDTO);

    UserDTO convertToUserDTO(UserListReqDTO userListReqDTO);

    UserDTO convertToUserDTO(UserPageReqDTO userPageReqDTO);

    UserShowResDTO convertToUserShowResDTO(UserDTO userDTO);

    List<UserShowResDTO> convertToUserShowResDTOList(List<UserDTO> userDTOList);

    UserListResDTO convertToUserListResDTO(UserDTO userDTO);

    List<UserListResDTO> convertToUserListResDTOList(List<UserDTO> userDTOList);

    List<UserDTO> convertToUserDTOList(List<UserAddReqDTO> userAddReqDTOList);

    UserPageResDTO convertToUserPageResDTO(UserDTO userDTO);

    List<UserPageResDTO> convertToUserPageResDTOList(List<UserDTO> userDTOList);

    UserPageRoleResDTO convertToUserPageRoleResDTO(RoleDTO roleDTO);

    Page<UserPageRoleResDTO> convertToUserPageRoleResDTOPage(Page<RoleDTO> roleDTOPage);
}
