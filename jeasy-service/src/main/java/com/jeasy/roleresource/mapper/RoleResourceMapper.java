package com.jeasy.roleresource.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.roleresource.dto.RoleResourceAddReqDTO;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.jeasy.roleresource.dto.RoleResourceListReqDTO;
import com.jeasy.roleresource.dto.RoleResourceListResDTO;
import com.jeasy.roleresource.dto.RoleResourceModifyReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageResDTO;
import com.jeasy.roleresource.dto.RoleResourceRemoveReqDTO;
import com.jeasy.roleresource.dto.RoleResourceShowResDTO;
import com.jeasy.roleresource.po.RoleResourcePO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface RoleResourceMapper extends BaseMapper<RoleResourceDTO, RoleResourcePO> {

    default RoleResourceMapper me() {
        return SpringUtil.getBean(RoleResourceMapper.class);
    }

    RoleResourceDTO convertToRoleResourceDTO(RoleResourceAddReqDTO roleResourceAddReqDTO);

    RoleResourceDTO convertToRoleResourceDTO(RoleResourceModifyReqDTO roleResourceModifyReqDTO);

    RoleResourceDTO convertToRoleResourceDTO(RoleResourceRemoveReqDTO roleResourceRemoveReqDTO);

    RoleResourceDTO convertToRoleResourceDTO(RoleResourceListReqDTO roleResourceListReqDTO);

    RoleResourceDTO convertToRoleResourceDTO(RoleResourcePageReqDTO roleResourcePageReqDTO);

    RoleResourceShowResDTO convertToRoleResourceShowResDTO(RoleResourceDTO roleResourceDTO);

    List<RoleResourceShowResDTO> convertToRoleResourceShowResDTOList(List<RoleResourceDTO> roleResourceDTOList);

    RoleResourceListResDTO convertToRoleResourceListResDTO(RoleResourceDTO roleResourceDTO);

    List<RoleResourceListResDTO> convertToRoleResourceListResDTOList(List<RoleResourceDTO> roleResourceDTOList);

    List<RoleResourceDTO> convertToRoleResourceDTOList(List<RoleResourceAddReqDTO> roleResourceAddReqDTOList);

    RoleResourcePageResDTO convertToRoleResourcePageResDTO(RoleResourceDTO roleResourceDTO);

    List<RoleResourcePageResDTO> convertToRoleResourcePageResDTOList(List<RoleResourceDTO> roleResourceDTOList);

    Page<RoleResourcePageResDTO> convertToRoleResourcePageResDTOPage(Page<RoleResourceDTO> roleResourceDTOPage);
}
