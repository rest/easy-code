package com.jeasy.roleresource.domain;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.roleresource.dto.RoleResourceAddReqDTO;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.jeasy.roleresource.dto.RoleResourceListReqDTO;
import com.jeasy.roleresource.dto.RoleResourceListResDTO;
import com.jeasy.roleresource.dto.RoleResourceModifyReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageReqDTO;
import com.jeasy.roleresource.dto.RoleResourcePageResDTO;
import com.jeasy.roleresource.dto.RoleResourceRemoveReqDTO;
import com.jeasy.roleresource.dto.RoleResourceShowResDTO;
import com.jeasy.roleresource.mapper.RoleResourceMapper;
import com.jeasy.roleresource.po.RoleResourcePO;
import com.javabooter.core.Func;
import com.javabooter.core.domain.BaseDO;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色资源 DO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class RoleResourceDO extends BaseDO<RoleResourceDTO, RoleResourcePO, RoleResourceMapper> {

    public static RoleResourceDO me() {
        return SpringUtil.getBean(RoleResourceDO.class);
    }

    public void checkRoleResourceAddReqDTO(final RoleResourceAddReqDTO roleResourceAddReqDTO) {
        if (Func.isEmpty(roleResourceAddReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkRoleResourceAddReqDTOList(final List<RoleResourceAddReqDTO> roleResourceAddReqDTOList) {
        if (Func.isEmpty(roleResourceAddReqDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkIds(List<String> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "集合不能为空且大小大于0");
        }
    }

    public void checkRoleResourceModifyReqDTO(final RoleResourceModifyReqDTO roleResourceModifyReqDTO) {
        if (Func.isEmpty(roleResourceModifyReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkRoleResourceRemoveReqDTO(final RoleResourceRemoveReqDTO roleResourceRemoveReqDTO) {
        if (Func.isEmpty(roleResourceRemoveReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkRoleIdList(final List<String> roleIdList) {
        if (Func.isEmpty(roleIdList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public RoleResourceDTO buildListParamsDTO(final RoleResourceListReqDTO roleResourceListReqDTO) {
        return mapper.convertToRoleResourceDTO(roleResourceListReqDTO);
    }

    public RoleResourceDTO buildPageParamsDTO(final RoleResourcePageReqDTO roleResourcePageReqDTO) {
        return mapper.convertToRoleResourceDTO(roleResourcePageReqDTO);
    }

    public RoleResourceDTO buildAddRoleResourceDTO(final RoleResourceAddReqDTO roleResourceAddReqDTO) {
        return mapper.convertToRoleResourceDTO(roleResourceAddReqDTO);
    }

    public List<RoleResourceDTO> buildAddBatchRoleResourceDTO(final List<RoleResourceAddReqDTO> roleResourceAddReqDTOList) {
        return mapper.convertToRoleResourceDTOList(roleResourceAddReqDTOList);
    }

    public RoleResourceDTO buildModifyRoleResourceDTO(final RoleResourceModifyReqDTO roleResourceModifyReqDTO) {
        return mapper.convertToRoleResourceDTO(roleResourceModifyReqDTO);
    }

    public RoleResourceDTO buildRemoveRoleResourceDTO(final RoleResourceRemoveReqDTO roleResourceRemoveReqDTO) {
        return mapper.convertToRoleResourceDTO(roleResourceRemoveReqDTO);
    }

    public List<RoleResourceListResDTO> transferRoleResourceListResDTOList(final List<RoleResourceDTO> roleResourceDTOList) {
        if (Func.isEmpty(roleResourceDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToRoleResourceListResDTOList(roleResourceDTOList);
    }

    public RoleResourceListResDTO transferRoleResourceListResDTO(final RoleResourceDTO roleResourceDTO) {
        if (Func.isEmpty(roleResourceDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToRoleResourceListResDTO(roleResourceDTO);
    }

    public Page<RoleResourcePageResDTO> transferRoleResourcePageResDTOPage(final Page<RoleResourceDTO> roleResourceDTOPage) {
        if (Func.isEmpty(roleResourceDTOPage) || Func.isEmpty(roleResourceDTOPage.getRecords())) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToRoleResourcePageResDTOPage(roleResourceDTOPage);
    }

    public RoleResourceShowResDTO transferRoleResourceShowResDTO(final RoleResourceDTO roleResourceDTO) {
        if (Func.isEmpty(roleResourceDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToRoleResourceShowResDTO(roleResourceDTO);
    }

    public List<RoleResourceShowResDTO> transferRoleResourceShowResDTOList(final List<RoleResourceDTO> roleResourceDTOList) {
        if (Func.isEmpty(roleResourceDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToRoleResourceShowResDTOList(roleResourceDTOList);
    }
}
