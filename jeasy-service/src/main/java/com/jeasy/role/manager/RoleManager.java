package com.jeasy.role.manager;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeasy.resource.dto.ResourceDTO;
import com.jeasy.resource.manager.ResourceManager;
import com.jeasy.role.dao.RoleDAO;
import com.jeasy.role.domain.RoleDO;
import com.jeasy.role.dto.RoleAddReqDTO;
import com.jeasy.role.dto.RoleDTO;
import com.jeasy.role.dto.RoleListPermissionReqDTO;
import com.jeasy.role.dto.RoleListPermissionResDTO;
import com.jeasy.role.dto.RoleListReqDTO;
import com.jeasy.role.dto.RoleListResDTO;
import com.jeasy.role.dto.RoleModifyPermissionReqDTO;
import com.jeasy.role.dto.RoleModifyReqDTO;
import com.jeasy.role.dto.RolePageReqDTO;
import com.jeasy.role.dto.RolePageResDTO;
import com.jeasy.role.dto.RoleRemoveReqDTO;
import com.jeasy.role.dto.RoleShowResDTO;
import com.jeasy.role.mapper.RoleMapper;
import com.jeasy.role.po.RolePO;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.jeasy.roleresource.manager.RoleResourceManager;
import com.javabooter.core.Func;
import com.javabooter.core.manager.impl.BaseManagerImpl;
import com.javabooter.core.object.MapUtils;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 角色 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class RoleManager extends BaseManagerImpl<RoleDAO, RolePO, RoleDTO, RoleMapper> {

    public static RoleManager me() {
        return SpringUtil.getBean(RoleManager.class);
    }

    public List<RoleListResDTO> list(final RoleListReqDTO roleListReqDTO) {
        RoleDTO paramsDTO = RoleDO.me().buildListParamsDTO(roleListReqDTO);

        List<RoleDTO> roleDTOList = super.findList(paramsDTO);

        return RoleDO.me().transferRoleListResDTOList(roleDTOList);
    }

    public List<RoleListResDTO> listByVersion1(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public List<RoleListResDTO> listByVersion2(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public List<RoleListResDTO> listByVersion3(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public RoleListResDTO listOne(final RoleListReqDTO roleListReqDTO) {
        RoleDTO paramsDTO = RoleDO.me().buildListParamsDTO(roleListReqDTO);

        RoleDTO roleDTO = super.findOne(paramsDTO);

        return RoleDO.me().transferRoleListResDTO(roleDTO);
    }

    public Page<RolePageResDTO> pagination(final RolePageReqDTO rolePageReqDTO, final Integer current, final Integer size) {
        QueryWrapper<RolePO> queryWrapper = RoleDO.me().buildRoleQueryWrapper(rolePageReqDTO);

        Page<RoleDTO> roleDTOPage = super.findPage(queryWrapper, current, size);

        return RoleDO.me().transferRolePageResDTOPage(roleDTOPage);
    }

    public Boolean add(final RoleAddReqDTO roleAddReqDTO) {
        RoleDO.me().checkRoleAddReqDTO(roleAddReqDTO);

        QueryWrapper<RolePO> queryWrapper = RoleDO.me().buildRoleQueryWrapper(roleAddReqDTO);
        Integer count = RoleManager.me().findCount(queryWrapper);

        RoleDO.me().checkResultCount(count);

        RoleDTO addRoleDTO = RoleDO.me().buildAddRoleDTO(roleAddReqDTO);

        return super.saveDTO(addRoleDTO);
    }

    public Boolean addAllColumn(final RoleAddReqDTO roleAddReqDTO) {
        RoleDO.me().checkRoleAddReqDTO(roleAddReqDTO);

        RoleDTO addRoleDTO = RoleDO.me().buildAddRoleDTO(roleAddReqDTO);

        return super.saveAllColumn(addRoleDTO);
    }

    public Boolean addBatchAllColumn(final List<RoleAddReqDTO> roleAddReqDTOList) {
        RoleDO.me().checkRoleAddReqDTOList(roleAddReqDTOList);

        List<RoleDTO> addBatchRoleDTOList = RoleDO.me().buildAddBatchRoleDTOList(roleAddReqDTOList);

        return super.saveBatchAllColumn(addBatchRoleDTOList);
    }

    public RoleShowResDTO show(final String id) {
        RoleDTO roleDTO = super.findById(id);

        return RoleDO.me().transferRoleShowResDTO(roleDTO);
    }

    public List<RoleShowResDTO> showByIds(final List<String> ids) {
        RoleDO.me().checkIds(ids);

        List<RoleDTO> roleDTOList = super.findBatchIds(ids);

        return RoleDO.me().transferRoleShowResDTOList(roleDTOList);
    }

    public Boolean modify(final RoleModifyReqDTO roleModifyReqDTO) {
        RoleDO.me().checkRoleModifyReqDTO(roleModifyReqDTO);

        QueryWrapper<RolePO> queryWrapper = RoleDO.me().buildRoleQueryWrapper(roleModifyReqDTO);
        Integer count = RoleManager.me().findCount(queryWrapper);

        RoleDO.me().checkResultCount(count);

        RoleDTO modifyRoleDTO = RoleDO.me().buildModifyRoleDTO(roleModifyReqDTO);

        return super.modifyById(modifyRoleDTO);
    }

    public Boolean modifyAllColumn(final RoleModifyReqDTO roleModifyReqDTO) {
        RoleDO.me().checkRoleModifyReqDTO(roleModifyReqDTO);

        RoleDTO modifyRoleDTO = RoleDO.me().buildModifyRoleDTO(roleModifyReqDTO);

        return super.modifyAllColumnById(modifyRoleDTO);
    }

    public Boolean removeByParams(final RoleRemoveReqDTO roleRemoveReqDTO) {
        RoleDO.me().checkRoleRemoveReqDTO(roleRemoveReqDTO);

        RoleDTO removeRoleDTO = RoleDO.me().buildRemoveRoleDTO(roleRemoveReqDTO);

        return super.remove(removeRoleDTO);
    }

    public List<RoleListPermissionResDTO> listPermission(final RoleListPermissionReqDTO roleListPermissionReqDTO) {
        RoleResourceDTO roleResourceDTO = RoleDO.me().buildRoleResourceDTO(roleListPermissionReqDTO);

        List<RoleResourceDTO> roleResourceDTOList = RoleResourceManager.me().findList(roleResourceDTO);
        List<ResourceDTO> resourceDTOList = ResourceManager.me().findList(new ResourceDTO());

        return RoleDO.me().transferRoleListPermissionResDTO(roleResourceDTOList, resourceDTOList);
    }

    public Boolean modifyPermission(final RoleModifyPermissionReqDTO roleModifyPermissionReqDTO) {
        RoleDTO roleDTO = RoleManager.me().findById(roleModifyPermissionReqDTO.getRoleId());

        RoleDO.me().checkRoleDTO(roleDTO);

        RoleResourceDTO roleResourceDTO = RoleDO.me().buildRoleResourceDTO(roleModifyPermissionReqDTO);
        RoleResourceManager.me().remove(roleResourceDTO);

        if (Func.isNotEmpty(roleModifyPermissionReqDTO.getPermissionIds())) {
            List<ResourceDTO> resourceDTOList = ResourceManager.me().findBatchIds(roleModifyPermissionReqDTO.getPermissionIds());
            List<RoleResourceDTO> roleResourceDTOList = RoleDO.me().buildRoleResourceDTOList(roleDTO, resourceDTOList);
            return RoleResourceManager.me().saveBatchAllColumn(roleResourceDTOList);
        }

        return true;
    }

    @Override
    protected RolePO mapToPO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RolePO();
        }

        return (RolePO) MapUtils.toBean(map, RolePO.class);
    }

    @Override
    protected RoleDTO mapToDTO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RoleDTO();
        }

        return (RoleDTO) MapUtils.toBean(map, RoleDTO.class);
    }
}
