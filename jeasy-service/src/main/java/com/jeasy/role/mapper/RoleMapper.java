package com.jeasy.role.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.role.dto.RoleAddReqDTO;
import com.jeasy.role.dto.RoleDTO;
import com.jeasy.role.dto.RoleListPermissionReqDTO;
import com.jeasy.role.dto.RoleListReqDTO;
import com.jeasy.role.dto.RoleListResDTO;
import com.jeasy.role.dto.RoleModifyPermissionReqDTO;
import com.jeasy.role.dto.RoleModifyReqDTO;
import com.jeasy.role.dto.RolePageReqDTO;
import com.jeasy.role.dto.RolePageResDTO;
import com.jeasy.role.dto.RoleRemoveReqDTO;
import com.jeasy.role.dto.RoleShowResDTO;
import com.jeasy.role.po.RolePO;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface RoleMapper extends BaseMapper<RoleDTO, RolePO> {

    default RoleMapper me() {
        return SpringUtil.getBean(RoleMapper.class);
    }

    RoleDTO convertToRoleDTO(RoleAddReqDTO roleAddReqDTO);

    RoleDTO convertToRoleDTO(RoleModifyReqDTO roleModifyReqDTO);

    RoleDTO convertToRoleDTO(RoleRemoveReqDTO roleRemoveReqDTO);

    RoleDTO convertToRoleDTO(RoleListReqDTO roleListReqDTO);

    RoleDTO convertToRoleDTO(RolePageReqDTO rolePageReqDTO);

    RoleShowResDTO convertToRoleShowResDTO(RoleDTO roleDTO);

    List<RoleShowResDTO> convertToRoleShowResDTOList(List<RoleDTO> roleDTOList);

    RoleListResDTO convertToRoleListResDTO(RoleDTO roleDTO);

    List<RoleListResDTO> convertToRoleListResDTOList(List<RoleDTO> roleDTOList);

    List<RoleDTO> convertToRoleDTOList(List<RoleAddReqDTO> roleAddReqDTOList);

    RolePageResDTO convertToRolePageResDTO(RoleDTO roleDTO);

    List<RolePageResDTO> convertToRolePageResDTOList(List<RoleDTO> roleDTOList);

    RoleResourceDTO convertToRoleResourceDTO(RoleListPermissionReqDTO roleListPermissionReqDTO);

    RoleResourceDTO convertToRoleResourceDTO(RoleModifyPermissionReqDTO roleModifyPermissionReqDTO);

    Page<RolePageResDTO> convertToRolePageResDTOPage(Page<RoleDTO> roleDTOPage);
}
