package com.jeasy.resource.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.resource.dto.ResourceAddReqDTO;
import com.jeasy.resource.dto.ResourceDTO;
import com.jeasy.resource.dto.ResourceListReqDTO;
import com.jeasy.resource.dto.ResourceListResDTO;
import com.jeasy.resource.dto.ResourceModifyReqDTO;
import com.jeasy.resource.dto.ResourcePageReqDTO;
import com.jeasy.resource.dto.ResourcePageResDTO;
import com.jeasy.resource.dto.ResourceRemoveReqDTO;
import com.jeasy.resource.dto.ResourceShowResDTO;
import com.jeasy.resource.dto.UserMenuOperationDTO;
import com.jeasy.resource.dto.UserMenuResourceDTO;
import com.jeasy.resource.po.ResourcePO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface ResourceMapper extends BaseMapper<ResourceDTO, ResourcePO> {

    default ResourceMapper me() {
        return SpringUtil.getBean(ResourceMapper.class);
    }

    ResourceDTO convertToResourceDTO(ResourceAddReqDTO resourceAddReqDTO);

    ResourceDTO convertToResourceDTO(ResourceModifyReqDTO resourceModifyReqDTO);

    ResourceDTO convertToResourceDTO(ResourceRemoveReqDTO resourceRemoveReqDTO);

    ResourceDTO convertToResourceDTO(ResourceListReqDTO resourceListReqDTO);

    ResourceDTO convertToResourceDTO(ResourcePageReqDTO resourcePageReqDTO);

    ResourceShowResDTO convertToResourceShowResDTO(ResourceDTO resourceDTO);

    List<ResourceShowResDTO> convertToResourceShowResDTOList(List<ResourceDTO> resourceDTOList);

    ResourceListResDTO convertToResourceListResDTO(ResourceDTO resourceDTO);

    List<ResourceListResDTO> convertToResourceListResDTOList(List<ResourceDTO> resourceDTOList);

    List<ResourceDTO> convertToResourceDTOList(List<ResourceAddReqDTO> resourceAddReqDTOList);

    ResourcePageResDTO convertToResourcePageResDTO(ResourceDTO resourceDTO);

    List<ResourcePageResDTO> convertToResourcePageResDTOList(List<ResourceDTO> resourceDTOList);

    UserMenuOperationDTO convertToUserMenuOperationDTO(ResourceDTO resourceDTO);

    UserMenuResourceDTO convertToUserMenuResourceDTO(ResourceDTO resourceDTO);

    Page<ResourcePageResDTO> convertToResourcePageResDTOPage(Page<ResourceDTO> resourceDTOPage);
}
