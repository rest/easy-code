package com.jeasy.userrole.manager;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userrole.dao.UserRoleDAO;
import com.jeasy.userrole.domain.UserRoleDO;
import com.jeasy.userrole.dto.UserRoleAddReqDTO;
import com.jeasy.userrole.dto.UserRoleDTO;
import com.jeasy.userrole.dto.UserRoleListReqDTO;
import com.jeasy.userrole.dto.UserRoleListResDTO;
import com.jeasy.userrole.dto.UserRoleModifyReqDTO;
import com.jeasy.userrole.dto.UserRolePageReqDTO;
import com.jeasy.userrole.dto.UserRolePageResDTO;
import com.jeasy.userrole.dto.UserRoleRemoveReqDTO;
import com.jeasy.userrole.dto.UserRoleShowResDTO;
import com.jeasy.userrole.mapper.UserRoleMapper;
import com.jeasy.userrole.po.UserRolePO;
import com.javabooter.core.Func;
import com.javabooter.core.manager.impl.BaseManagerImpl;
import com.javabooter.core.object.MapUtils;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 用户角色 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class UserRoleManager extends BaseManagerImpl<UserRoleDAO, UserRolePO, UserRoleDTO, UserRoleMapper> {

    public static UserRoleManager me() {
        return SpringUtil.getBean(UserRoleManager.class);
    }

    public List<UserRoleListResDTO> list(final UserRoleListReqDTO userRoleListReqDTO) {
        UserRoleDTO paramsDTO = UserRoleDO.me().buildListParamsDTO(userRoleListReqDTO);

        List<UserRoleDTO> userRoleDTOList = super.findList(paramsDTO);

        return UserRoleDO.me().transferUserRoleListResDTOList(userRoleDTOList);
    }

    public List<UserRoleListResDTO> listByVersion1(final UserRoleListReqDTO userRoleListReqDTO) {
        return list(userRoleListReqDTO);
    }

    public List<UserRoleListResDTO> listByVersion2(final UserRoleListReqDTO userRoleListReqDTO) {
        return list(userRoleListReqDTO);
    }

    public List<UserRoleListResDTO> listByVersion3(final UserRoleListReqDTO userRoleListReqDTO) {
        return list(userRoleListReqDTO);
    }

    public UserRoleListResDTO listOne(final UserRoleListReqDTO userRoleListReqDTO) {
        UserRoleDTO paramsDTO = UserRoleDO.me().buildListParamsDTO(userRoleListReqDTO);

        UserRoleDTO userRoleDTO = super.findOne(paramsDTO);

        return UserRoleDO.me().transferUserRoleListResDTO(userRoleDTO);
    }

    public Page<UserRolePageResDTO> pagination(final UserRolePageReqDTO userRolePageReqDTO, final Integer current, final Integer size) {
        UserRoleDTO paramsDTO = UserRoleDO.me().buildPageParamsDTO(userRolePageReqDTO);

        Page<UserRoleDTO> userRoleDTOPage = super.findPage(paramsDTO, current, size);

        return UserRoleDO.me().transferUserRolePageResDTOPage(userRoleDTOPage);
    }

    public Boolean add(final UserRoleAddReqDTO userRoleAddReqDTO) {
        UserRoleDO.me().checkUserRoleAddReqDTO(userRoleAddReqDTO);

        UserRoleDTO addUserRoleDTO = UserRoleDO.me().buildAddUserRoleDTO(userRoleAddReqDTO);

        return super.saveDTO(addUserRoleDTO);
    }

    public Boolean addAllColumn(final UserRoleAddReqDTO userRoleAddReqDTO) {
        UserRoleDO.me().checkUserRoleAddReqDTO(userRoleAddReqDTO);

        UserRoleDTO addUserRoleDTO = UserRoleDO.me().buildAddUserRoleDTO(userRoleAddReqDTO);

        return super.saveAllColumn(addUserRoleDTO);
    }

    public Boolean addBatchAllColumn(final List<UserRoleAddReqDTO> userRoleAddReqDTOList) {
        UserRoleDO.me().checkUserRoleAddReqDTOList(userRoleAddReqDTOList);

        List<UserRoleDTO> addBatchUserRoleDTOList = UserRoleDO.me().buildAddBatchUserRoleDTOList(userRoleAddReqDTOList);

        return super.saveBatchAllColumn(addBatchUserRoleDTOList);
    }

    public UserRoleShowResDTO show(final String id) {
        UserRoleDTO userRoleDTO = super.findById(id);

        return UserRoleDO.me().transferUserRoleShowResDTO(userRoleDTO);
    }

    public List<UserRoleShowResDTO> showByIds(final List<String> ids) {
        UserRoleDO.me().checkIds(ids);

        List<UserRoleDTO> userRoleDTOList = super.findBatchIds(ids);

        return UserRoleDO.me().transferUserRoleShowResDTOList(userRoleDTOList);
    }

    public Boolean modify(final UserRoleModifyReqDTO userRoleModifyReqDTO) {
        UserRoleDO.me().checkUserRoleModifyReqDTO(userRoleModifyReqDTO);

        UserRoleDTO modifyUserRoleDTO = UserRoleDO.me().buildModifyUserRoleDTO(userRoleModifyReqDTO);

        return super.modifyById(modifyUserRoleDTO);
    }

    public Boolean modifyAllColumn(final UserRoleModifyReqDTO userRoleModifyReqDTO) {
        UserRoleDO.me().checkUserRoleModifyReqDTO(userRoleModifyReqDTO);

        UserRoleDTO modifyUserRoleDTO = UserRoleDO.me().buildModifyUserRoleDTO(userRoleModifyReqDTO);

        return super.modifyAllColumnById(modifyUserRoleDTO);
    }

    public Boolean removeByParams(final UserRoleRemoveReqDTO userRoleRemoveReqDTO) {
        UserRoleDO.me().checkUserRoleRemoveReqDTO(userRoleRemoveReqDTO);

        UserRoleDTO removeUserRoleDTO = UserRoleDO.me().buildRemoveUserRoleDTO(userRoleRemoveReqDTO);

        return super.remove(removeUserRoleDTO);
    }

    @Override
    protected UserRolePO mapToPO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserRolePO();
        }

        return (UserRolePO) MapUtils.toBean(map, UserRolePO.class);
    }

    @Override
    protected UserRoleDTO mapToDTO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserRoleDTO();
        }

        return (UserRoleDTO) MapUtils.toBean(map, UserRoleDTO.class);
    }
}
