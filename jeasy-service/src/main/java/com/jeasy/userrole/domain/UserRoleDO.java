package com.jeasy.userrole.domain;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userrole.dto.UserRoleAddReqDTO;
import com.jeasy.userrole.dto.UserRoleDTO;
import com.jeasy.userrole.dto.UserRoleListReqDTO;
import com.jeasy.userrole.dto.UserRoleListResDTO;
import com.jeasy.userrole.dto.UserRoleModifyReqDTO;
import com.jeasy.userrole.dto.UserRolePageReqDTO;
import com.jeasy.userrole.dto.UserRolePageResDTO;
import com.jeasy.userrole.dto.UserRoleRemoveReqDTO;
import com.jeasy.userrole.dto.UserRoleShowResDTO;
import com.jeasy.userrole.mapper.UserRoleMapper;
import com.jeasy.userrole.po.UserRolePO;
import com.javabooter.core.Func;
import com.javabooter.core.domain.BaseDO;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户角色 DO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class UserRoleDO extends BaseDO<UserRoleDTO, UserRolePO, UserRoleMapper> {

    public static UserRoleDO me() {
        return SpringUtil.getBean(UserRoleDO.class);
    }

    public void checkUserRoleAddReqDTO(final UserRoleAddReqDTO userRoleAddReqDTO) {
        if (Func.isEmpty(userRoleAddReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkUserRoleAddReqDTOList(final List<UserRoleAddReqDTO> userRoleAddReqDTOList) {
        if (Func.isEmpty(userRoleAddReqDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkIds(final List<String> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "集合不能为空且大小大于0");
        }
    }

    public void checkUserRoleModifyReqDTO(final UserRoleModifyReqDTO userRoleModifyReqDTO) {
        if (Func.isEmpty(userRoleModifyReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkUserRoleRemoveReqDTO(final UserRoleRemoveReqDTO userRoleRemoveReqDTO) {
        if (Func.isEmpty(userRoleRemoveReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public UserRoleDTO buildListParamsDTO(final UserRoleListReqDTO userRoleListReqDTO) {
        return mapper.convertToUserRoleDTO(userRoleListReqDTO);
    }

    public UserRoleDTO buildPageParamsDTO(final UserRolePageReqDTO userRolePageReqDTO) {
        return mapper.convertToUserRoleDTO(userRolePageReqDTO);
    }

    public UserRoleDTO buildAddUserRoleDTO(final UserRoleAddReqDTO userRoleAddReqDTO) {
        return mapper.convertToUserRoleDTO(userRoleAddReqDTO);
    }

    public List<UserRoleDTO> buildAddBatchUserRoleDTOList(final List<UserRoleAddReqDTO> userRoleAddReqDTOList) {
        return mapper.convertToUserRoleDTOList(userRoleAddReqDTOList);
    }

    public UserRoleDTO buildModifyUserRoleDTO(final UserRoleModifyReqDTO userRoleModifyReqDTO) {
        return mapper.convertToUserRoleDTO(userRoleModifyReqDTO);
    }

    public UserRoleDTO buildRemoveUserRoleDTO(final UserRoleRemoveReqDTO userRoleRemoveReqDTO) {
        return mapper.convertToUserRoleDTO(userRoleRemoveReqDTO);
    }

    public List<UserRoleListResDTO> transferUserRoleListResDTOList(final List<UserRoleDTO> userRoleDTOList) {
        if (Func.isEmpty(userRoleDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserRoleListResDTOList(userRoleDTOList);
    }

    public UserRoleListResDTO transferUserRoleListResDTO(final UserRoleDTO userRoleDTO) {
        if (Func.isEmpty(userRoleDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserRoleListResDTO(userRoleDTO);
    }

    public Page<UserRolePageResDTO> transferUserRolePageResDTOPage(final Page<UserRoleDTO> userRoleDTOPage) {
        if (Func.isEmpty(userRoleDTOPage) || Func.isEmpty(userRoleDTOPage.getRecords())) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserRolePageResDTOPage(userRoleDTOPage);
    }

    public UserRoleShowResDTO transferUserRoleShowResDTO(final UserRoleDTO userRoleDTO) {
        if (Func.isEmpty(userRoleDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserRoleShowResDTO(userRoleDTO);
    }

    public List<UserRoleShowResDTO> transferUserRoleShowResDTOList(final List<UserRoleDTO> userRoleDTOList) {
        if (Func.isEmpty(userRoleDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToUserRoleShowResDTOList(userRoleDTOList);
    }
}
