package com.jeasy.userrole.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.userrole.dto.UserRoleAddReqDTO;
import com.jeasy.userrole.dto.UserRoleDTO;
import com.jeasy.userrole.dto.UserRoleListReqDTO;
import com.jeasy.userrole.dto.UserRoleListResDTO;
import com.jeasy.userrole.dto.UserRoleModifyReqDTO;
import com.jeasy.userrole.dto.UserRolePageReqDTO;
import com.jeasy.userrole.dto.UserRolePageResDTO;
import com.jeasy.userrole.dto.UserRoleRemoveReqDTO;
import com.jeasy.userrole.dto.UserRoleShowResDTO;
import com.jeasy.userrole.po.UserRolePO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface UserRoleMapper extends BaseMapper<UserRoleDTO, UserRolePO> {

    default UserRoleMapper me() {
        return SpringUtil.getBean(UserRoleMapper.class);
    }

    UserRoleDTO convertToUserRoleDTO(UserRoleAddReqDTO userRoleAddReqDTO);

    UserRoleDTO convertToUserRoleDTO(UserRoleModifyReqDTO userRoleModifyReqDTO);

    UserRoleDTO convertToUserRoleDTO(UserRoleRemoveReqDTO userRoleRemoveReqDTO);

    UserRoleDTO convertToUserRoleDTO(UserRoleListReqDTO userRoleListReqDTO);

    UserRoleDTO convertToUserRoleDTO(UserRolePageReqDTO userRolePageReqDTO);

    UserRoleShowResDTO convertToUserRoleShowResDTO(UserRoleDTO userRoleDTO);

    List<UserRoleShowResDTO> convertToUserRoleShowResDTOList(List<UserRoleDTO> userRoleDTOList);

    UserRoleListResDTO convertToUserRoleListResDTO(UserRoleDTO userRoleDTO);

    List<UserRoleListResDTO> convertToUserRoleListResDTOList(List<UserRoleDTO> userRoleDTOList);

    List<UserRoleDTO> convertToUserRoleDTOList(List<UserRoleAddReqDTO> userRoleAddReqDTOList);

    UserRolePageResDTO convertToUserRolePageResDTO(UserRoleDTO userRoleDTO);

    List<UserRolePageResDTO> convertToUserRolePageResDTOList(List<UserRoleDTO> userRoleDTOList);

    Page<UserRolePageResDTO> convertToUserRolePageResDTOPage(Page<UserRoleDTO> userRoleDTOPage);
}
