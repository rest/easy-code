package com.jeasy.dictionary.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.dictionary.dto.DictionaryAddReqDTO;
import com.jeasy.dictionary.dto.DictionaryDTO;
import com.jeasy.dictionary.dto.DictionaryListReqDTO;
import com.jeasy.dictionary.dto.DictionaryListResDTO;
import com.jeasy.dictionary.dto.DictionaryModifyReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageReqDTO;
import com.jeasy.dictionary.dto.DictionaryPageResDTO;
import com.jeasy.dictionary.dto.DictionaryRemoveReqDTO;
import com.jeasy.dictionary.dto.DictionaryShowResDTO;
import com.jeasy.dictionary.po.DictionaryPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface DictionaryMapper extends BaseMapper<DictionaryDTO, DictionaryPO> {

    default DictionaryMapper me() {
        return SpringUtil.getBean(DictionaryMapper.class);
    }

    DictionaryDTO convertToDictionaryDTO(DictionaryAddReqDTO dictionaryAddReqDTO);

    DictionaryDTO convertToDictionaryDTO(DictionaryModifyReqDTO dictionaryModifyReqDTO);

    DictionaryDTO convertToDictionaryDTO(DictionaryRemoveReqDTO dictionaryRemoveReqDTO);

    DictionaryDTO convertToDictionaryDTO(DictionaryListReqDTO dictionaryListReqDTO);

    DictionaryDTO convertToDictionaryDTO(DictionaryPageReqDTO dictionaryPageReqDTO);

    DictionaryShowResDTO convertToDictionaryShowResDTO(DictionaryDTO dictionaryDTO);

    List<DictionaryShowResDTO> convertToDictionaryShowResDTOList(List<DictionaryDTO> dictionaryDTOList);

    DictionaryListResDTO convertToDictionaryListResDTO(DictionaryDTO dictionaryDTO);

    List<DictionaryListResDTO> convertToDictionaryListResDTOList(List<DictionaryDTO> dictionaryDTOList);

    List<DictionaryDTO> convertToDictionaryDTOList(List<DictionaryAddReqDTO> dictionaryAddReqDTOList);

    DictionaryPageResDTO convertToDictionaryPageResDTO(DictionaryDTO dictionaryDTO);

    List<DictionaryPageResDTO> convertToDictionaryPageResDTOList(List<DictionaryDTO> dictionaryDTOList);

    Page<DictionaryPageResDTO> convertToDictionaryPageResDTOPage(Page<DictionaryDTO> dictionaryDTOPage);
}
