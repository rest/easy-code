package com.jeasy.log.manager;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.log.dao.LogDAO;
import com.jeasy.log.domain.LogDO;
import com.jeasy.log.dto.LogAddReqDTO;
import com.jeasy.log.dto.LogDTO;
import com.jeasy.log.dto.LogListReqDTO;
import com.jeasy.log.dto.LogListResDTO;
import com.jeasy.log.dto.LogModifyReqDTO;
import com.jeasy.log.dto.LogPageReqDTO;
import com.jeasy.log.dto.LogPageResDTO;
import com.jeasy.log.dto.LogRemoveReqDTO;
import com.jeasy.log.dto.LogShowResDTO;
import com.jeasy.log.mapper.LogMapper;
import com.jeasy.log.po.LogPO;
import com.javabooter.core.Func;
import com.javabooter.core.manager.impl.BaseManagerImpl;
import com.javabooter.core.object.MapUtils;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 日志 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@Component
public class LogManager extends BaseManagerImpl<LogDAO, LogPO, LogDTO, LogMapper> {

    public static LogManager me() {
        return SpringUtil.getBean(LogManager.class);
    }

    public List<LogListResDTO> list(final LogListReqDTO logListReqDTO) {
        LogDTO paramsDTO = LogDO.me().buildListParamsDTO(logListReqDTO);

        List<LogDTO> logDTOList = super.findList(paramsDTO);

        return LogDO.me().transferLogListResDTOList(logDTOList);
    }

    public List<LogListResDTO> listByVersion1(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public List<LogListResDTO> listByVersion2(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public List<LogListResDTO> listByVersion3(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public LogListResDTO listOne(final LogListReqDTO logListReqDTO) {
        LogDTO paramsDTO = LogDO.me().buildListParamsDTO(logListReqDTO);

        LogDTO logDTO = super.findOne(paramsDTO);

        return LogDO.me().transferLogListResDTO(logDTO);
    }

    public Page<LogPageResDTO> pagination(final LogPageReqDTO logPageReqDTO, final Integer current, final Integer size) {
        LogDTO paramsDTO = LogDO.me().buildPageParamsDTO(logPageReqDTO);

        Page<LogDTO> logDTOPage = super.findPage(paramsDTO, current, size);

        return LogDO.me().transferLogPageResDTOPage(logDTOPage);
    }

    public Boolean add(final LogAddReqDTO logAddReqDTO) {
        LogDO.me().checkLogAddReqDTO(logAddReqDTO);

        LogDTO addLogDTO = LogDO.me().buildAddLogDTO(logAddReqDTO);

        return super.saveDTO(addLogDTO);
    }

    public Boolean addAllColumn(final LogAddReqDTO logAddReqDTO) {
        LogDO.me().checkLogAddReqDTO(logAddReqDTO);

        LogDTO addLogDTO = LogDO.me().buildAddLogDTO(logAddReqDTO);

        return super.saveAllColumn(addLogDTO);
    }

    public Boolean addBatchAllColumn(final List<LogAddReqDTO> logAddReqDTOList) {
        LogDO.me().checkLogAddReqDTOList(logAddReqDTOList);

        List<LogDTO> addBatchLogDTOList = LogDO.me().buildAddBatchLogDTOList(logAddReqDTOList);

        return super.saveBatchAllColumn(addBatchLogDTOList);
    }

    public LogShowResDTO show(final String id) {
        LogDTO logDTO = super.findById(id);

        return LogDO.me().transferLogShowResDTO(logDTO);
    }

    public List<LogShowResDTO> showByIds(final List<String> ids) {
        List<LogDTO> logDTOList = super.findBatchIds(ids);

        return LogDO.me().transferLogShowResDTOList(logDTOList);
    }

    public Boolean modify(final LogModifyReqDTO logModifyReqDTO) {
        LogDO.me().checkLogModifyReqDTO(logModifyReqDTO);

        LogDTO modifyLogDTO = LogDO.me().buildModifyLogDTO(logModifyReqDTO);

        return super.modifyById(modifyLogDTO);
    }

    public Boolean modifyAllColumn(final LogModifyReqDTO logModifyReqDTO) {
        LogDO.me().checkLogModifyReqDTO(logModifyReqDTO);

        LogDTO modifyLogDTO = LogDO.me().buildModifyLogDTO(logModifyReqDTO);

        return super.modifyAllColumnById(modifyLogDTO);
    }

    public Boolean removeByParams(final LogRemoveReqDTO logRemoveReqDTO) {
        LogDO.me().checkLogRemoveReqDTO(logRemoveReqDTO);

        LogDTO removeLogDTO = LogDO.me().buildRemoveLogDTO(logRemoveReqDTO);

        return super.remove(removeLogDTO);
    }

    @Override
    protected LogPO mapToPO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new LogPO();
        }

        return (LogPO) MapUtils.toBean(map, LogPO.class);
    }

    @Override
    protected LogDTO mapToDTO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new LogDTO();
        }

        return (LogDTO) MapUtils.toBean(map, LogDTO.class);
    }
}
