package com.jeasy.log.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.log.dto.LogAddReqDTO;
import com.jeasy.log.dto.LogDTO;
import com.jeasy.log.dto.LogListReqDTO;
import com.jeasy.log.dto.LogListResDTO;
import com.jeasy.log.dto.LogModifyReqDTO;
import com.jeasy.log.dto.LogPageReqDTO;
import com.jeasy.log.dto.LogPageResDTO;
import com.jeasy.log.dto.LogRemoveReqDTO;
import com.jeasy.log.dto.LogShowResDTO;
import com.jeasy.log.po.LogPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface LogMapper extends BaseMapper<LogDTO, LogPO> {

    default LogMapper me() {
        return SpringUtil.getBean(LogMapper.class);
    }

    LogDTO convertToLogDTO(LogAddReqDTO logAddReqDTO);

    LogDTO convertToLogDTO(LogModifyReqDTO logModifyReqDTO);

    LogDTO convertToLogDTO(LogRemoveReqDTO logRemoveReqDTO);

    LogDTO convertToLogDTO(LogListReqDTO logListReqDTO);

    LogDTO convertToLogDTO(LogPageReqDTO logPageReqDTO);

    LogShowResDTO convertToLogShowResDTO(LogDTO logDTO);

    List<LogShowResDTO> convertToLogShowResDTOList(List<LogDTO> logDTOList);

    LogListResDTO convertToLogListResDTO(LogDTO logDTO);

    List<LogListResDTO> convertToLogListResDTOList(List<LogDTO> logDTOList);

    List<LogDTO> convertToLogDTOList(List<LogAddReqDTO> logAddReqDTOList);

    LogPageResDTO convertToLogPageResDTO(LogDTO logDTO);

    List<LogPageResDTO> convertToLogPageResDTOList(List<LogDTO> logDTOList);

    Page<LogPageResDTO> convertToLogPageResDTOPage(Page<LogDTO> logDTOPage);
}
