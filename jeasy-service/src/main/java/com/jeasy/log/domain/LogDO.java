package com.jeasy.log.domain;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.log.dto.LogAddReqDTO;
import com.jeasy.log.dto.LogDTO;
import com.jeasy.log.dto.LogListReqDTO;
import com.jeasy.log.dto.LogListResDTO;
import com.jeasy.log.dto.LogModifyReqDTO;
import com.jeasy.log.dto.LogPageReqDTO;
import com.jeasy.log.dto.LogPageResDTO;
import com.jeasy.log.dto.LogRemoveReqDTO;
import com.jeasy.log.dto.LogShowResDTO;
import com.jeasy.log.mapper.LogMapper;
import com.jeasy.log.po.LogPO;
import com.javabooter.core.Func;
import com.javabooter.core.domain.BaseDO;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 日志 DO
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@Component
public class LogDO extends BaseDO<LogDTO, LogPO, LogMapper> {

    public static LogDO me() {
        return SpringUtil.getBean(LogDO.class);
    }

    public void checkLogAddReqDTO(final LogAddReqDTO logAddReqDTO) {
        if (Func.isEmpty(logAddReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkLogAddReqDTOList(final List<LogAddReqDTO> logAddReqDTOList) {
        if (Func.isEmpty(logAddReqDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkLogModifyReqDTO(final LogModifyReqDTO logModifyReqDTO) {
        if (Func.isEmpty(logModifyReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkLogRemoveReqDTO(final LogRemoveReqDTO logRemoveReqDTO) {
        if (Func.isEmpty(logRemoveReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public LogDTO buildListParamsDTO(final LogListReqDTO logListReqDTO) {
        return mapper.convertToLogDTO(logListReqDTO);
    }

    public LogDTO buildPageParamsDTO(final LogPageReqDTO logPageReqDTO) {
        return mapper.convertToLogDTO(logPageReqDTO);
    }

    public LogDTO buildAddLogDTO(final LogAddReqDTO logAddReqDTO) {
        return mapper.convertToLogDTO(logAddReqDTO);
    }

    public List<LogDTO> buildAddBatchLogDTOList(final List<LogAddReqDTO> logAddReqDTOList) {
        return mapper.convertToLogDTOList(logAddReqDTOList);
    }

    public LogDTO buildModifyLogDTO(final LogModifyReqDTO logModifyReqDTO) {
        return mapper.convertToLogDTO(logModifyReqDTO);
    }

    public LogDTO buildRemoveLogDTO(final LogRemoveReqDTO logRemoveReqDTO) {
        return mapper.convertToLogDTO(logRemoveReqDTO);
    }

    public List<LogListResDTO> transferLogListResDTOList(final List<LogDTO> logDTOList) {
        if (Func.isEmpty(logDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToLogListResDTOList(logDTOList);
    }

    public LogListResDTO transferLogListResDTO(final LogDTO logDTO) {
        if (Func.isEmpty(logDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToLogListResDTO(logDTO);
    }

    public Page<LogPageResDTO> transferLogPageResDTOPage(final Page<LogDTO> logDTOPage) {
        if (Func.isEmpty(logDTOPage) || Func.isEmpty(logDTOPage.getRecords())) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToLogPageResDTOPage(logDTOPage);
    }

    public LogShowResDTO transferLogShowResDTO(final LogDTO logDTO) {
        if (Func.isEmpty(logDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToLogShowResDTO(logDTO);
    }

    public List<LogShowResDTO> transferLogShowResDTOList(final List<LogDTO> logDTOList) {
        if (Func.isEmpty(logDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToLogShowResDTOList(logDTOList);
    }
}
