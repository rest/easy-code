package com.jeasy.organization.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.organization.dto.OrganizationAddReqDTO;
import com.jeasy.organization.dto.OrganizationDTO;
import com.jeasy.organization.dto.OrganizationListReqDTO;
import com.jeasy.organization.dto.OrganizationListResDTO;
import com.jeasy.organization.dto.OrganizationModifyReqDTO;
import com.jeasy.organization.dto.OrganizationPageReqDTO;
import com.jeasy.organization.dto.OrganizationPageResDTO;
import com.jeasy.organization.dto.OrganizationRemoveReqDTO;
import com.jeasy.organization.dto.OrganizationShowResDTO;
import com.jeasy.organization.po.OrganizationPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface OrganizationMapper extends BaseMapper<OrganizationDTO, OrganizationPO> {

    default OrganizationMapper me() {
        return SpringUtil.getBean(OrganizationMapper.class);
    }

    OrganizationDTO convertToOrganizationDTO(OrganizationAddReqDTO organizationAddReqDTO);

    OrganizationDTO convertToOrganizationDTO(OrganizationModifyReqDTO organizationModifyReqDTO);

    OrganizationDTO convertToOrganizationDTO(OrganizationRemoveReqDTO organizationRemoveReqDTO);

    OrganizationDTO convertToOrganizationDTO(OrganizationListReqDTO organizationListReqDTO);

    OrganizationDTO convertToOrganizationDTO(OrganizationPageReqDTO organizationPageReqDTO);

    OrganizationShowResDTO convertToOrganizationShowResDTO(OrganizationDTO organizationDTO);

    List<OrganizationShowResDTO> convertToOrganizationShowResDTOList(List<OrganizationDTO> organizationDTOList);

    OrganizationListResDTO convertToOrganizationListResDTO(OrganizationDTO organizationDTO);

    List<OrganizationListResDTO> convertToOrganizationListResDTOList(List<OrganizationDTO> organizationDTOList);

    List<OrganizationDTO> convertToOrganizationDTOList(List<OrganizationAddReqDTO> organizationAddReqDTOList);

    OrganizationPageResDTO convertToOrganizationPageResDTO(OrganizationDTO organizationDTO);

    List<OrganizationPageResDTO> convertToOrganizationPageResDTOList(List<OrganizationDTO> organizationDTOList);

    Page<OrganizationPageResDTO> convertToOrganizationPageResDTOPage(Page<OrganizationDTO> organizationDTOPage);
}
