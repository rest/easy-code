package com.jeasy.organization.manager;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeasy.organization.dao.OrganizationDAO;
import com.jeasy.organization.domain.OrganizationDO;
import com.jeasy.organization.dto.OrganizationAddReqDTO;
import com.jeasy.organization.dto.OrganizationDTO;
import com.jeasy.organization.dto.OrganizationListReqDTO;
import com.jeasy.organization.dto.OrganizationListResDTO;
import com.jeasy.organization.dto.OrganizationModifyReqDTO;
import com.jeasy.organization.dto.OrganizationPageReqDTO;
import com.jeasy.organization.dto.OrganizationPageResDTO;
import com.jeasy.organization.dto.OrganizationRemoveReqDTO;
import com.jeasy.organization.dto.OrganizationShowResDTO;
import com.jeasy.organization.mapper.OrganizationMapper;
import com.jeasy.organization.po.OrganizationPO;
import com.javabooter.core.Func;
import com.javabooter.core.manager.impl.BaseManagerImpl;
import com.javabooter.core.object.MapUtils;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 机构 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class OrganizationManager extends BaseManagerImpl<OrganizationDAO, OrganizationPO, OrganizationDTO, OrganizationMapper> {

    public static OrganizationManager me() {
        return SpringUtil.getBean(OrganizationManager.class);
    }

    public List<OrganizationListResDTO> list(final OrganizationListReqDTO organizationListReqDTO) {
        OrganizationDTO paramsDTO = OrganizationDO.me().buildListParamsDTO(organizationListReqDTO);

        List<OrganizationDTO> organizationDTOList = super.findList(paramsDTO);

        return OrganizationDO.me().transferOrganizationListResDTOList(organizationDTOList);
    }

    public OrganizationListResDTO listOne(final OrganizationListReqDTO organizationListReqDTO) {
        OrganizationDTO paramsDTO = OrganizationDO.me().buildListParamsDTO(organizationListReqDTO);

        OrganizationDTO organizationDTO = super.findOne(paramsDTO);

        return OrganizationDO.me().transferOrganizationListResDTO(organizationDTO);
    }

    public Page<OrganizationPageResDTO> pagination(final OrganizationPageReqDTO organizationPageReqDTO, final Integer current, final Integer size) {
        OrganizationDTO paramsDTO = OrganizationDO.me().buildPageParamsDTO(organizationPageReqDTO);

        Page<OrganizationDTO> organizationDTOPage = super.findPage(paramsDTO, current, size);

        return OrganizationDO.me().transferOrganizationPageResDTOPage(organizationDTOPage);
    }

    public Boolean add(final OrganizationAddReqDTO organizationAddReqDTO) {
        OrganizationDO.me().checkOrganizationAddReqDTO(organizationAddReqDTO);

        QueryWrapper<OrganizationPO> queryWrapper = OrganizationDO.me().buildOrganizationQueryWrapper(organizationAddReqDTO);
        Integer count = OrganizationManager.me().findCount(queryWrapper);

        OrganizationDO.me().checkResultCount(count);

        OrganizationDTO addOrganizationDTO = OrganizationDO.me().buildAddOrganizationDTO(organizationAddReqDTO);

        return super.saveDTO(addOrganizationDTO);
    }

    public Boolean addAllColumn(final OrganizationAddReqDTO organizationAddReqDTO) {
        OrganizationDO.me().checkOrganizationAddReqDTO(organizationAddReqDTO);

        OrganizationDTO addOrganizationDTO = OrganizationDO.me().buildAddOrganizationDTO(organizationAddReqDTO);

        return super.saveAllColumn(addOrganizationDTO);
    }

    public Boolean addBatchAllColumn(final List<OrganizationAddReqDTO> organizationAddReqDTOList) {
        OrganizationDO.me().checkOrganizationAddReqDTOList(organizationAddReqDTOList);

        List<OrganizationDTO> addBatchOrganizationDTOList = OrganizationDO.me().buildAddBatchOrganizationDTOList(organizationAddReqDTOList);

        return super.saveBatchAllColumn(addBatchOrganizationDTOList);
    }

    public OrganizationShowResDTO show(final String id) {
        OrganizationDTO organizationDTO = super.findById(id);

        OrganizationDO.me().checkOrganizationDTO(organizationDTO);

        OrganizationDTO parentOrganizationDTO = null;
        if (Func.isNotEmpty(organizationDTO.getPid())) {
            parentOrganizationDTO = super.findById(organizationDTO.getPid());
        }

        return OrganizationDO.me().transferOrganizationShowResDTO(organizationDTO, parentOrganizationDTO);
    }

    public List<OrganizationShowResDTO> showByIds(final List<String> ids) {
        OrganizationDO.me().checkIds(ids);

        List<OrganizationDTO> organizationDTOList = super.findBatchIds(ids);

        return OrganizationDO.me().transferOrganizationShowResDTOList(organizationDTOList);
    }

    public Boolean modify(final OrganizationModifyReqDTO organizationModifyReqDTO) {
        OrganizationDO.me().checkOrganizationModifyReqDTO(organizationModifyReqDTO);

        QueryWrapper<OrganizationPO> queryWrapper = OrganizationDO.me().buildOrganizationQueryWrapper(organizationModifyReqDTO);
        Integer count = OrganizationManager.me().findCount(queryWrapper);

        OrganizationDO.me().checkResultCount(count);

        OrganizationDTO modifyOrganizationDTO = OrganizationDO.me().buildModifyOrganizationDTO(organizationModifyReqDTO);

        return super.modifyById(modifyOrganizationDTO);
    }

    public Boolean modifyAllColumn(final OrganizationModifyReqDTO organizationModifyReqDTO) {
        OrganizationDO.me().checkOrganizationModifyReqDTO(organizationModifyReqDTO);

        OrganizationDTO modifyOrganizationDTO = OrganizationDO.me().buildModifyOrganizationDTO(organizationModifyReqDTO);

        return super.modifyAllColumnById(modifyOrganizationDTO);
    }

    public Boolean removeByParams(final OrganizationRemoveReqDTO organizationRemoveReqDTO) {
        OrganizationDO.me().checkOrganizationRemoveReqDTO(organizationRemoveReqDTO);

        OrganizationDTO removeOrganizationDTO = OrganizationDO.me().buildRemoveOrganizationDTO(organizationRemoveReqDTO);

        return super.remove(removeOrganizationDTO);
    }

    @Override
    public Boolean removeById(final String id) {
        QueryWrapper<OrganizationPO> parentOrganizationWrapper = OrganizationDO.me().buildParentQueryWrapper(id);

        if (Func.isNotEmpty(parentOrganizationWrapper)) {
            List<OrganizationDTO> subOrganizationDTOList = super.findList(parentOrganizationWrapper);
            OrganizationDO.me().checkSubOrganizationDTOList(subOrganizationDTOList);
        }

        return super.removeById(id);
    }

    @Override
    protected OrganizationPO mapToPO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new OrganizationPO();
        }

        return (OrganizationPO) MapUtils.toBean(map, OrganizationPO.class);
    }

    @Override
    protected OrganizationDTO mapToDTO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new OrganizationDTO();
        }

        return (OrganizationDTO) MapUtils.toBean(map, OrganizationDTO.class);
    }
}
