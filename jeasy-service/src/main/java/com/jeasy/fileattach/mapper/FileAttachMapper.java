package com.jeasy.fileattach.mapper;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.fileattach.dto.FileAttachAddReqDTO;
import com.jeasy.fileattach.dto.FileAttachDTO;
import com.jeasy.fileattach.dto.FileAttachListReqDTO;
import com.jeasy.fileattach.dto.FileAttachListResDTO;
import com.jeasy.fileattach.dto.FileAttachModifyReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageResDTO;
import com.jeasy.fileattach.dto.FileAttachRemoveReqDTO;
import com.jeasy.fileattach.dto.FileAttachShowResDTO;
import com.jeasy.fileattach.po.FileAttachPO;
import com.javabooter.core.mapper.BaseMapper;
import com.javabooter.core.mapper.BaseMapperConfig;
import com.javabooter.core.page.Page;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0.0
 * @since 2021/06/02 17:10
 */
@Mapper(config = BaseMapperConfig.class)
public interface FileAttachMapper extends BaseMapper<FileAttachDTO, FileAttachPO> {

    default FileAttachMapper me() {
        return SpringUtil.getBean(FileAttachMapper.class);
    }

    FileAttachDTO convertToFileAttachDTO(FileAttachAddReqDTO fileAttachAddReqDTO);

    FileAttachDTO convertToFileAttachDTO(FileAttachModifyReqDTO fileAttachModifyReqDTO);

    FileAttachDTO convertToFileAttachDTO(FileAttachRemoveReqDTO fileAttachRemoveReqDTO);

    FileAttachDTO convertToFileAttachDTO(FileAttachListReqDTO fileAttachListReqDTO);

    FileAttachDTO convertToFileAttachDTO(FileAttachPageReqDTO fileAttachPageReqDTO);

    FileAttachShowResDTO convertToFileAttachShowResDTO(FileAttachDTO fileAttachDTO);

    List<FileAttachShowResDTO> convertToFileAttachShowResDTOList(List<FileAttachDTO> fileAttachDTOList);

    FileAttachListResDTO convertToFileAttachListResDTO(FileAttachDTO fileAttachDTO);

    List<FileAttachListResDTO> convertToFileAttachListResDTOList(List<FileAttachDTO> fileAttachDTOList);

    List<FileAttachDTO> convertToFileAttachDTOList(List<FileAttachAddReqDTO> fileAttachAddReqDTOList);

    FileAttachPageResDTO convertToFileAttachPageResDTO(FileAttachDTO fileAttachDTO);

    List<FileAttachPageResDTO> convertToFileAttachPageResDTOList(List<FileAttachDTO> fileAttachDTOList);

    Page<FileAttachPageResDTO> convertToFileAttachPageResDTOPage(Page<FileAttachDTO> fileAttachDTOPage);
}
