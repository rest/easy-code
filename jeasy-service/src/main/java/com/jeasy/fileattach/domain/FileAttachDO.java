package com.jeasy.fileattach.domain;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.fileattach.dto.FileAttachAddReqDTO;
import com.jeasy.fileattach.dto.FileAttachDTO;
import com.jeasy.fileattach.dto.FileAttachListReqDTO;
import com.jeasy.fileattach.dto.FileAttachListResDTO;
import com.jeasy.fileattach.dto.FileAttachModifyReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageResDTO;
import com.jeasy.fileattach.dto.FileAttachRemoveReqDTO;
import com.jeasy.fileattach.dto.FileAttachShowResDTO;
import com.jeasy.fileattach.mapper.FileAttachMapper;
import com.jeasy.fileattach.po.FileAttachPO;
import com.javabooter.core.Func;
import com.javabooter.core.domain.BaseDO;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文件附件 DO
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@Component
public class FileAttachDO extends BaseDO<FileAttachDTO, FileAttachPO, FileAttachMapper> {

    public static FileAttachDO me() {
        return SpringUtil.getBean(FileAttachDO.class);
    }

    public void checkFileAttachAddReqDTO(final FileAttachAddReqDTO fileAttachAddReqDTO) {
        if (Func.isEmpty(fileAttachAddReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkFileAttachAddReqDTOList(final List<FileAttachAddReqDTO> fileAttachAddReqDTOList) {
        if (Func.isEmpty(fileAttachAddReqDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkFileAttachModifyReqDTO(final FileAttachModifyReqDTO fileAttachModifyReqDTO) {
        if (Func.isEmpty(fileAttachModifyReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public void checkFileAttachRemoveReqDTO(final FileAttachRemoveReqDTO fileAttachRemoveReqDTO) {
        if (Func.isEmpty(fileAttachRemoveReqDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "入参不能为空");
        }
    }

    public FileAttachDTO buildListParamsDTO(final FileAttachListReqDTO fileAttachListReqDTO) {
        return mapper.convertToFileAttachDTO(fileAttachListReqDTO);
    }

    public FileAttachDTO buildPageParamsDTO(final FileAttachPageReqDTO fileAttachPageReqDTO) {
        return mapper.convertToFileAttachDTO(fileAttachPageReqDTO);
    }

    public FileAttachDTO buildAddFileAttachDTO(final FileAttachAddReqDTO fileAttachAddReqDTO) {
        return mapper.convertToFileAttachDTO(fileAttachAddReqDTO);
    }

    public List<FileAttachDTO> buildAddBatchFileAttachDTOList(final List<FileAttachAddReqDTO> fileAttachAddReqDTOList) {
        return mapper.convertToFileAttachDTOList(fileAttachAddReqDTOList);
    }

    public FileAttachDTO buildModifyFileAttachDTO(final FileAttachModifyReqDTO fileAttachModifyReqDTO) {
        return mapper.convertToFileAttachDTO(fileAttachModifyReqDTO);
    }

    public FileAttachDTO buildRemoveFileAttachDTO(final FileAttachRemoveReqDTO fileAttachRemoveReqDTO) {
        return mapper.convertToFileAttachDTO(fileAttachRemoveReqDTO);
    }

    public FileAttachListResDTO transferFileAttachListResDTO(final FileAttachDTO fileAttachDTO) {
        if (Func.isEmpty(fileAttachDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToFileAttachListResDTO(fileAttachDTO);
    }

    public List<FileAttachListResDTO> transferFileAttachListResDTOList(final List<FileAttachDTO> fileAttachDTOList) {
        if (Func.isEmpty(fileAttachDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToFileAttachListResDTOList(fileAttachDTOList);
    }

    public Page<FileAttachPageResDTO> transferFileAttachPageResDTOPage(final Page<FileAttachDTO> fileAttachDTOPage) {
        if (Func.isEmpty(fileAttachDTOPage) || Func.isEmpty(fileAttachDTOPage.getRecords())) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToFileAttachPageResDTOPage(fileAttachDTOPage);
    }

    public FileAttachShowResDTO transferFileAttachShowResDTO(final FileAttachDTO fileAttachDTO) {
        if (Func.isEmpty(fileAttachDTO)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToFileAttachShowResDTO(fileAttachDTO);
    }

    public List<FileAttachShowResDTO> transferFileAttachShowResDTOList(final List<FileAttachDTO> fileAttachDTOList) {
        if (Func.isEmpty(fileAttachDTOList)) {
            throw new MessageException(CommonInfoEnum.SUCCESS, "未查找到记录");
        }

        return mapper.convertToFileAttachShowResDTOList(fileAttachDTOList);
    }
}
