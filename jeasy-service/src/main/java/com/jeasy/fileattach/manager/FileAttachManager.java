package com.jeasy.fileattach.manager;

import cn.hutool.extra.spring.SpringUtil;
import com.jeasy.fileattach.dao.FileAttachDAO;
import com.jeasy.fileattach.domain.FileAttachDO;
import com.jeasy.fileattach.dto.FileAttachAddReqDTO;
import com.jeasy.fileattach.dto.FileAttachDTO;
import com.jeasy.fileattach.dto.FileAttachListReqDTO;
import com.jeasy.fileattach.dto.FileAttachListResDTO;
import com.jeasy.fileattach.dto.FileAttachModifyReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageReqDTO;
import com.jeasy.fileattach.dto.FileAttachPageResDTO;
import com.jeasy.fileattach.dto.FileAttachRemoveReqDTO;
import com.jeasy.fileattach.dto.FileAttachShowResDTO;
import com.jeasy.fileattach.mapper.FileAttachMapper;
import com.jeasy.fileattach.po.FileAttachPO;
import com.javabooter.core.Func;
import com.javabooter.core.manager.impl.BaseManagerImpl;
import com.javabooter.core.object.MapUtils;
import com.javabooter.core.page.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 文件附件 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2019/06/20 17:27
 */
@Slf4j
@Component
public class FileAttachManager extends BaseManagerImpl<FileAttachDAO, FileAttachPO, FileAttachDTO, FileAttachMapper> {

    public static FileAttachManager me() {
        return SpringUtil.getBean(FileAttachManager.class);
    }

    public List<FileAttachListResDTO> list(final FileAttachListReqDTO fileAttachListReqDTO) {
        FileAttachDTO paramsDTO = FileAttachDO.me().buildListParamsDTO(fileAttachListReqDTO);

        List<FileAttachDTO> fileAttachDTOList = super.findList(paramsDTO);

        return FileAttachDO.me().transferFileAttachListResDTOList(fileAttachDTOList);
    }

    public List<FileAttachListResDTO> listByVersion1(final FileAttachListReqDTO fileAttachListReqDTO) {
        return list(fileAttachListReqDTO);
    }

    public List<FileAttachListResDTO> listByVersion2(final FileAttachListReqDTO fileAttachListReqDTO) {
        return list(fileAttachListReqDTO);
    }

    public List<FileAttachListResDTO> listByVersion3(final FileAttachListReqDTO fileAttachListReqDTO) {
        return list(fileAttachListReqDTO);
    }

    public FileAttachListResDTO listOne(final FileAttachListReqDTO fileAttachListReqDTO) {
        FileAttachDTO paramsDTO = FileAttachDO.me().buildListParamsDTO(fileAttachListReqDTO);

        FileAttachDTO fileAttachDTO = super.findOne(paramsDTO);

        return FileAttachDO.me().transferFileAttachListResDTO(fileAttachDTO);
    }

    public Page<FileAttachPageResDTO> pagination(final FileAttachPageReqDTO fileAttachPageReqDTO, final Integer current, final Integer size) {
        FileAttachDTO paramsDTO = FileAttachDO.me().buildPageParamsDTO(fileAttachPageReqDTO);

        Page<FileAttachDTO> fileAttachDTOPage = super.findPage(paramsDTO, current, size);

        return FileAttachDO.me().transferFileAttachPageResDTOPage(fileAttachDTOPage);
    }

    public Boolean add(final FileAttachAddReqDTO fileAttachAddReqDTO) {
        FileAttachDO.me().checkFileAttachAddReqDTO(fileAttachAddReqDTO);

        FileAttachDTO addFileAttachDTO = FileAttachDO.me().buildAddFileAttachDTO(fileAttachAddReqDTO);

        return super.saveDTO(addFileAttachDTO);
    }

    public Boolean addAllColumn(final FileAttachAddReqDTO fileAttachAddReqDTO) {
        FileAttachDO.me().checkFileAttachAddReqDTO(fileAttachAddReqDTO);

        FileAttachDTO addFileAttachDTO = FileAttachDO.me().buildAddFileAttachDTO(fileAttachAddReqDTO);

        return super.saveAllColumn(addFileAttachDTO);
    }

    public Boolean addBatchAllColumn(final List<FileAttachAddReqDTO> fileAttachAddReqDTOList) {
        FileAttachDO.me().checkFileAttachAddReqDTOList(fileAttachAddReqDTOList);

        List<FileAttachDTO> addBatchFileAttachDTOList = FileAttachDO.me().buildAddBatchFileAttachDTOList(fileAttachAddReqDTOList);

        return super.saveBatchAllColumn(addBatchFileAttachDTOList);
    }

    public FileAttachShowResDTO show(final String id) {
        FileAttachDTO fileAttachDTO = super.findById(id);

        return FileAttachDO.me().transferFileAttachShowResDTO(fileAttachDTO);
    }

    public List<FileAttachShowResDTO> showByIds(final List<String> ids) {
        List<FileAttachDTO> fileAttachDTOList = super.findBatchIds(ids);

        return FileAttachDO.me().transferFileAttachShowResDTOList(fileAttachDTOList);
    }

    public Boolean modify(final FileAttachModifyReqDTO fileAttachModifyReqDTO) {
        FileAttachDO.me().checkFileAttachModifyReqDTO(fileAttachModifyReqDTO);

        FileAttachDTO modifyFileAttachDTO = FileAttachDO.me().buildModifyFileAttachDTO(fileAttachModifyReqDTO);

        return super.modifyById(modifyFileAttachDTO);
    }

    public Boolean modifyAllColumn(final FileAttachModifyReqDTO fileAttachModifyReqDTO) {
        FileAttachDO.me().checkFileAttachModifyReqDTO(fileAttachModifyReqDTO);

        FileAttachDTO modifyFileAttachDTO = FileAttachDO.me().buildModifyFileAttachDTO(fileAttachModifyReqDTO);

        return super.modifyAllColumnById(modifyFileAttachDTO);
    }

    public Boolean removeByParams(final FileAttachRemoveReqDTO fileAttachRemoveReqDTO) {
        FileAttachDO.me().checkFileAttachRemoveReqDTO(fileAttachRemoveReqDTO);

        FileAttachDTO removeFileAttachDTO = FileAttachDO.me().buildRemoveFileAttachDTO(fileAttachRemoveReqDTO);

        return super.remove(removeFileAttachDTO);
    }

    @Override
    protected FileAttachPO mapToPO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new FileAttachPO();
        }

        return (FileAttachPO) MapUtils.toBean(map, FileAttachPO.class);
    }

    @Override
    protected FileAttachDTO mapToDTO(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new FileAttachDTO();
        }

        return (FileAttachDTO) MapUtils.toBean(map, FileAttachDTO.class);
    }
}
