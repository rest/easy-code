package com.jeasy.service.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Slf4j
@Configuration
@Import(cn.hutool.extra.spring.SpringUtil.class)
@ComponentScan(basePackages = {"com.jeasy.*.mapper", "com.jeasy.*.domain"})
public class ServiceTestConfiguration {
}
