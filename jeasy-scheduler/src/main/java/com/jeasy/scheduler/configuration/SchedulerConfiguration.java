package com.jeasy.scheduler.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@Configuration
@EnableScheduling
@ComponentScan(basePackages = {"com.jeasy.scheduler"})
public class SchedulerConfiguration {

}
