package com.jeasy.db;

import cn.hutool.core.util.StrUtil;
import com.jeasy.util.TypeUtils;
import com.javabooter.core.str.StrConstants;
import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/19 12:17
 */
@Data
public class ColumnInfo {

    private static final String ID = "id";

    public ColumnInfo(String name, String type, String comment) {
        this.name = name;
        this.type = type;
        this.comment = comment;
    }

    private String name;

    private String type;

    private String comment;

    public String getName() {
        return name;
    }

    public String getCamelName() {
        return StrUtil.toCamelCase(name);
    }

    public String getUnderLineUpperName() {
        return StrUtil.toUnderlineCase(name).toUpperCase();
    }

    public String getClassName() {
        return StrUtil.upperFirst(getCamelName());
    }

    public String getJavaType() {
        if (ID.equalsIgnoreCase(name)) {
            return "Long";
        }

        if (TypeUtils.JAVA_TYPE_MAP.containsKey(type)) {
            return TypeUtils.JAVA_TYPE_MAP.get(type);
        }

        return type;
    }

    public String getMyBatisType() {
        if (TypeUtils.MYBATIS_TYPE_MAP.containsKey(type)) {
            return TypeUtils.MYBATIS_TYPE_MAP.get(type);
        }

        return type;
    }

    public String getClassImport() {
        if (TypeUtils.CLASS_IMPORT_MAP.containsKey(type)) {
            return TypeUtils.CLASS_IMPORT_MAP.get(type);
        }
        return StrConstants.S_EMPTY;
    }
}
