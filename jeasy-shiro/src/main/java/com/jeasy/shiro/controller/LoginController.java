package com.jeasy.shiro.controller;

import com.javabooter.common.captcha.CaptchaCacheManager;
import com.javabooter.core.Func;
import com.javabooter.core.doc.annotation.MethodDoc;
import com.javabooter.core.doc.annotation.StatusEnum;
import com.javabooter.core.exception.MessageException;
import com.javabooter.core.info.CommonInfoEnum;
import com.javabooter.core.security.PasswordHash;
import com.javabooter.core.web.controller.ControllerSupport;
import com.javabooter.core.web.csrf.CsrfToken;
import com.javabooter.core.web.dto.CurrentUser;
import com.javabooter.core.web.resolver.FromJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录退出
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Slf4j
@RestController
public class LoginController extends ControllerSupport {

    private static final String REDIRECT_LOGIN = "redirect:/#/login";

    private static final String REDIRECT_HOME = "redirect:/#/home";

    /**
     * S_GET 登录 shiro 写法
     */
    @CsrfToken(create = true)
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated() || subject.isRemembered()) {
            return REDIRECT_HOME;
        }
        return REDIRECT_LOGIN;
    }

    /**
     * POST 登录 shiro 写法
     */
    @MethodDoc(item = LoginResDTO.class, desc = {"5.登录管理", "2.用户登录", "1.用户登录接口"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2015-07-31")
    @CsrfToken(remove = true)
    @RequestMapping(value = "/login", method = {RequestMethod.POST})
    public void login(@FromJson LoginReqDTO loginDTO) {

        boolean isCheckSuccess = CaptchaCacheManager.me().validate(request, response, loginDTO.getCaptcha());
        if (!isCheckSuccess) {
            responseMessage(CommonInfoEnum.INTERNAL_ERROR, "验证码错误!");
            return;
        }

        Subject user = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(loginDTO.getUsername(), loginDTO.getPassword());

        // 设置记住密码
        boolean isRememberMe = true;
        if (Func.isNotEmpty(loginDTO.getRememberMe())) {
            isRememberMe = 1 == loginDTO.getRememberMe();
        }
        token.setRememberMe(isRememberMe);

        try {
            user.login(token);
        } catch (UnknownAccountException e) {
            log.error("unknown account.", e);
            responseMessage(CommonInfoEnum.SUCCESS, "账号不存在!");
            return;
        } catch (DisabledAccountException e) {
            log.error("disabled account.", e);
            responseMessage(CommonInfoEnum.SUCCESS, "账号未启用!");
            return;
        } catch (IncorrectCredentialsException e) {
            log.error("incorrect credentials.", e);
            responseMessage(CommonInfoEnum.SUCCESS, "密码错误!");
            return;
        } catch (AuthenticationException e) {
            log.error("authentication fail.", e);
            if (MessageException.class.isInstance(e.getCause())) {
                MessageException me = (MessageException) e.getCause();
                responseMessage(me.getCode(), me.getMessage());
            } else {
                responseMessage(CommonInfoEnum.INTERNAL_ERROR, e.getMessage());
            }
            return;
        } catch (Throwable e) {
            log.error("other error.", e);
            responseMessage(CommonInfoEnum.INTERNAL_ERROR, e.getMessage());
            return;
        }

        // 返回当前登录用户信息
        CurrentUser currentUser = (CurrentUser) SecurityUtils.getSubject().getPrincipal();
        LoginResDTO loginResDTO = new LoginResDTO();
        loginResDTO.setAccess(currentUser.getName());
        loginResDTO.setAvatar("https://avatars0.githubusercontent.com/u/20942571?s=460&v=4");
        loginResDTO.setToken(currentUser.getName());
        loginResDTO.setName(currentUser.getName());
        responseItem(CommonInfoEnum.SUCCESS, loginResDTO);
    }


    /**
     * POST 登录 shiro 写法
     */
    @MethodDoc(item = LoginResDTO.class, desc = {"5.登录管理", "2.用户登录", "2.获取当前登录用户信息接口"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2015-07-31")
    @RequestMapping(value = "/userInfo", method = {RequestMethod.POST})
    public void userInfo() {
        // 返回当前登录用户信息
        CurrentUser currentUser = (CurrentUser) SecurityUtils.getSubject().getPrincipal();
        LoginResDTO loginResDTO = new LoginResDTO();
        loginResDTO.setAccess(currentUser.getName());
        loginResDTO.setAvatar("https://avatars0.githubusercontent.com/u/20942571?s=460&v=4");
        loginResDTO.setToken(currentUser.getName());
        loginResDTO.setName(currentUser.getName());
        responseItem(CommonInfoEnum.SUCCESS, loginResDTO);
    }

    /**
     * 未授权
     */
    @RequestMapping(value = "/unauth", method = {RequestMethod.GET})
    public String unauth() {
        if (!SecurityUtils.getSubject().isAuthenticated()) {
            return REDIRECT_LOGIN;
        }
        return "unauth";
    }

    @MethodDoc(desc = {"5.登录管理", "2.用户登录", "3.验证码接口"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2015-07-31")
    @CsrfToken(create = true)
    @RequestMapping("/captcha.jpg")
    public void captcha() {
        CaptchaCacheManager.me().generate(request, response);
    }

    /**
     * 退出
     */
    @MethodDoc(desc = {"5.登录管理", "3.用户登出", "1.用户登出接口"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2015-07-31")
    @RequestMapping(value = "/logout", method = {RequestMethod.POST})
    public void logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        responseMessage(CommonInfoEnum.SUCCESS);
    }

    public static void main(String[] args) {
        PasswordHash passwordHash = new PasswordHash();
        passwordHash.setAlgorithmName("md5");
        passwordHash.setHashIterations(1);

        System.out.println(passwordHash.toHex("admin", "abc"));
        System.out.println(System.getProperty("java.io.tmpdir"));
    }
}
