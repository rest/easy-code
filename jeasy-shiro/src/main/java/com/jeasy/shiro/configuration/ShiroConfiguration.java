package com.jeasy.shiro.configuration;

import static org.apache.shiro.session.mgt.AbstractSessionManager.DEFAULT_GLOBAL_SESSION_TIMEOUT;

import com.google.common.collect.Maps;
import com.jeasy.shiro.authc.RetryLimitCredentialsMatcher;
import com.jeasy.shiro.filter.KickoutSessionControlFilter;
import com.jeasy.shiro.filter.ShiroAjaxSessionFilter;
import com.jeasy.shiro.realm.ShiroDbRealm;
import com.javabooter.common.cache.CommonCacheManager;
import com.javabooter.core.security.PasswordHash;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.filter.DelegatingFilterProxy;

import java.util.EnumSet;
import java.util.Map;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;

@Slf4j
@Configuration
@ComponentScan(basePackages = {"com.jeasy.shiro"})
public class ShiroConfiguration {

    /**
     * ShiroFilter 权限控制
     *
     * @return
     */
    @Bean("filterShiroFilterRegistrationBean")
    public FilterRegistrationBean<DelegatingFilterProxy> delegatingFilterProxy() {
        FilterRegistrationBean<DelegatingFilterProxy> filterRegistrationBean = new FilterRegistrationBean<>();

        DelegatingFilterProxy proxy = new DelegatingFilterProxy();
        proxy.setTargetFilterLifecycle(true);
        proxy.setTargetBeanName("shiroFilter");
        filterRegistrationBean.setFilter(proxy);

        filterRegistrationBean.setEnabled(true);
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setDispatcherTypes(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD));

        Map<String, String> initParameters = Maps.newHashMap();
        initParameters.put("staticSecurityManagerEnabled", "true");
        filterRegistrationBean.setInitParameters(initParameters);
        return filterRegistrationBean;
    }

    /**
     * 項目自定义的Realm
     *
     * @param commonCacheManager
     * @param retryLimitCredentialsMatcher
     * @return
     */
    @Bean
    public ShiroDbRealm shiroDbRealm(CommonCacheManager commonCacheManager,
        RetryLimitCredentialsMatcher retryLimitCredentialsMatcher) {
        ShiroDbRealm shiroDbRealm = new ShiroDbRealm(commonCacheManager, retryLimitCredentialsMatcher);
        shiroDbRealm.setAuthorizationCachingEnabled(true);
        shiroDbRealm.setAuthorizationCacheName("authorizationCache");

        shiroDbRealm.setAuthenticationCachingEnabled(true);
        shiroDbRealm.setAuthenticationCacheName("authenticationCache");

        shiroDbRealm.setKickoutCacheManager(commonCacheManager);
        shiroDbRealm.setKickoutCacheName("shiro-kickout-session");
        return shiroDbRealm;
    }

    @Bean
    public DefaultWebSecurityManager securityManager(CommonCacheManager commonCacheManager, ShiroDbRealm shiroDbRealm,
        DefaultWebSessionManager defaultWebSessionManager) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(shiroDbRealm);
        defaultWebSecurityManager.setCacheManager(commonCacheManager);
        defaultWebSecurityManager.setRememberMeManager(rememberMeManager());
        defaultWebSecurityManager.setSessionManager(defaultWebSessionManager);
        return defaultWebSecurityManager;
    }

    private CookieRememberMeManager rememberMeManager() {
        SimpleCookie rememberMeCookie = new SimpleCookie("rememberMe");
        rememberMeCookie.setHttpOnly(true);
        rememberMeCookie.setMaxAge(7 * 24 * 60 * 60);

        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie);
        cookieRememberMeManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager defaultWebSecurityManager,
        KickoutSessionControlFilter kickoutSessionControlFilter) {
        ShiroFilterFactoryBean filterFactoryBean = new ShiroFilterFactoryBean();
        filterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        filterFactoryBean.setFilterChainDefinitionMap(shiroFilterChainDefinition().getFilterChainMap());
        filterFactoryBean.setLoginUrl("/#/login");
        filterFactoryBean.setSuccessUrl("/#/home");
        filterFactoryBean.setUnauthorizedUrl("/unauth");

        Map<String, Filter> filters = Maps.newHashMap();
        filters.put("user", new ShiroAjaxSessionFilter());
        filters.put("kickout", kickoutSessionControlFilter);
        filterFactoryBean.setFilters(filters);
        return filterFactoryBean;
    }

    @Bean
    public KickoutSessionControlFilter kickoutSessionControlFilter(CommonCacheManager commonCacheManager,
        DefaultWebSessionManager defaultWebSessionManager) {
        KickoutSessionControlFilter kickoutSessionControlFilter = new KickoutSessionControlFilter();
        kickoutSessionControlFilter.setCacheManager(commonCacheManager);
        kickoutSessionControlFilter.setKickoutCacheName("shiro-kickout-session");

        kickoutSessionControlFilter.setSessionManager(defaultWebSessionManager);
        kickoutSessionControlFilter.setKickoutAttribute("kickout");

        kickoutSessionControlFilter.setKickoutAfter(false);
        kickoutSessionControlFilter.setMaxSession(1);
        kickoutSessionControlFilter.setKickoutUrl("/#/login?kickout=1");
        return kickoutSessionControlFilter;
    }

    private ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition filterChainDefinition = new DefaultShiroFilterChainDefinition();
        filterChainDefinition.addPathDefinition("/", "anon");
        filterChainDefinition.addPathDefinition("/captcha*", "anon");
        filterChainDefinition.addPathDefinition("/commons/**", "anon");
        filterChainDefinition.addPathDefinition("/hook", "anon");
        filterChainDefinition.addPathDefinition("/login", "anon");
        filterChainDefinition.addPathDefinition("/doc/**", "user, kickout");
        filterChainDefinition.addPathDefinition("/monitor/**", "user, kickout");
        filterChainDefinition.addPathDefinition("/druid/**", "user, kickout");
        filterChainDefinition.addPathDefinition("/**", "user, kickout");
        return filterChainDefinition;
    }

    @Bean
    public EnterpriseCacheSessionDAO sessionDAO(CommonCacheManager commonCacheManager) {
        EnterpriseCacheSessionDAO sessionDAO = new EnterpriseCacheSessionDAO();
        sessionDAO.setCacheManager(commonCacheManager);
        sessionDAO.setActiveSessionsCacheName("activeSessionCache");
        return sessionDAO;
    }

    @Bean(destroyMethod = "destroy")
    public DefaultWebSessionManager sessionManager(EnterpriseCacheSessionDAO enterpriseCacheSessionDAO) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();

        sessionManager.setGlobalSessionTimeout(DEFAULT_GLOBAL_SESSION_TIMEOUT);
        sessionManager.setSessionValidationSchedulerEnabled(false);
        sessionManager.setDeleteInvalidSessions(false);

        SimpleCookie sessionIdCookie = new SimpleCookie("sid");
        sessionIdCookie.setHttpOnly(true);
        sessionIdCookie.setMaxAge(-1);

        sessionManager.setSessionDAO(enterpriseCacheSessionDAO);
        sessionManager.setSessionIdCookie(sessionIdCookie);
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }

    @Bean
    public RetryLimitCredentialsMatcher credentialsMatcher(CommonCacheManager commonCacheManager) {
        RetryLimitCredentialsMatcher retryLimitCredentialsMatcher =
            new RetryLimitCredentialsMatcher(commonCacheManager);
        retryLimitCredentialsMatcher.setRetryLimitCount(5);
        retryLimitCredentialsMatcher.setRetryLimitCacheName("halfHour");

        retryLimitCredentialsMatcher.setPasswordHash(PasswordHash.me());
        return retryLimitCredentialsMatcher;
    }

    /**
     * 保证实现了 Shiro 内部 lifecycle 函数的 Bean 执行
     *
     * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    /**
     * AOP 式方法级权限检查
     *
     * @return
     */
    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    /**
     * 在方法中 注入 securityManager,进行代理控制
     *
     * @return
     */
    @Bean
    public MethodInvokingFactoryBean methodInvokingFactoryBean(DefaultWebSecurityManager securityManager) {
        MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
        methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
        methodInvokingFactoryBean.setArguments(securityManager);
        return methodInvokingFactoryBean;
    }

    /**
     * 启用 shiro 控制器授权注解拦截方式
     *
     * @param securityManager
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(
        DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor =
            new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    class FilterOrders {
        public static final int SHIRO_FILTER = 1;
    }
}
